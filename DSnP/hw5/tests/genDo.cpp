#include<cstdlib>
#include<cstdio>
#include<ctime>
#include<iostream>
#include<fstream>
using namespace std;

#define RAND(x) ((int)(x * (double) rand() / ((double)(RAND_MAX) + 1)))
#define LENGTH 5

void usage(const char * name) {
   cout << name << " <iteration> <output>" << endl ;
   exit(-1) ;
}

string random(const size_t length) {
   static char buf[256] ;
   for(size_t i = 0 ; i < length ; i ++) {
      buf[i] = (char)('a' + RAND(26)) ;
   }
   buf[length] = '\0' ;
   return buf ;
}

void rDelete(const size_t number , ofstream& fout) {
   for(size_t i = 0 ; i < number ; i ++) {
      fout << "adtd -s " << random(LENGTH) << endl ;
   }
}

void change(ofstream & fout) {
   fout << "adtd -max " << (RAND(30) + 1) << endl ;
   fout << "adtd -min " << (RAND(30) + 1) << endl ;
   rDelete(RAND(100) + 1, fout) ;
   fout << "adtp\nadtp -r" << endl ;
}

int main(int argc , char* argv[])
{
   if(argc != 3) usage(argv[0]) ;

   srand((unsigned)time(NULL)) ;
   int n ;
   if (sscanf(argv[1] , "%d" , &n) != 1) usage(argv[0]) ;

   ofstream fout(argv[2] , ios::out) ;
   if(fout.is_open() == false) usage(argv[0]) ;

   for(int i = 0 ; i < n ; i ++) {
      fout << "adta -s " << random(LENGTH) << endl ;
      if(RAND(1000) < 5) {
         change(fout) ;
      }
   }
   change(fout) ;
   fout << "usage" << endl ;
   fout << "q -f" << endl ;
   fout.close() ;
   return 0;
}

