#!/usr/bin/env ruby
CHAR = ('a'..'z').to_a
TITLE = ["insert" , "it++" , "it--" , "erase" , "" , "pop front" ,"" , "pop back"]

class Hw5Benchmark
  def initialize(n)
    @n = n
  end

  def run(exes)
    exe_result = Array.new exes.size
    for i in @n
      threads = []
      test_for i
      for j in 0...exes.size
        threads << Thread.new(j) do |index|
          result = IO.popen(exes[index] + ' -f bm.tmp').readlines
          time = result.map {|s| s.scan /Total time used  : ([\d\.]*) seconds/ }.reject {|s| s == [] }.flatten
          (0...time.size).to_a.reverse.each {|i| (i != 0) ? time[i] = time[i].to_f - time[i-1].to_f : time[i] = time[i].to_f }
          Thread.current['exe'], Thread.current['time'] = index, time
        end
      end
      threads.each do |t|
        t.join
        exe_result[t['exe']] ||= []
        exe_result[t['exe']] << t['time']
      end
    end
    File.delete 'bm.tmp'
    @report = exe_result
  end

  def plot(exes)
    require 'rubygems'
    require 'gnuplot'

  for j in 0...TITLE.size
		  next if TITLE[j] == ""
		  Gnuplot.open do |gp|
			  Gnuplot::Plot.new(gp) do |plot|
				  plot.terminal "postscript eps enhanced color"
				  plot.output "#{TITLE[j].sub(/ / , '_')}.eps"
				  plot.title  TITLE[j]
				  plot.ylabel "time"
				  plot.xlabel "n"

				  for i in 0...exes.size
					  plot.data << Gnuplot::DataSet.new([@n, @report[i].map {|v| v[j]}]) do |ds|
						  ds.with = "linespoints"
						  ds.title = exes[i]
						  ds.linewidth = 1
					  end
				  end
			  end
		  end
	  end
  end

  def test_for(i)
    File.open('bm.tmp', 'w') do |f|
      cmds = ["adta -r #{i}", 'adtp', 'adtp -r', "adtd -r #{i}", "adta -r #{i}", "adtd -min #{i}", "adta -r #{i}", "adtd -max #{i}"]
      for cmd in cmds
        f.puts cmd
        f.puts 'usage'
      end
      f.puts 'q -f'
    end
  end
end

if $PROGRAM_NAME == __FILE__
  if ARGV[0] == '--plot'
    ARGV.shift
    plot = true
  end
  if ARGV[0] == '--log'
    n = [ARGV[1].to_i]
    while n[-1] * 2 < ARGV[2].to_i
      n << n[-1] * 2
    end
  else
    n = ((ARGV[0].to_i)..(ARGV[1].to_i)).step(ARGV[2].to_i).to_a
  end
  bm = Hw5Benchmark.new(n)
  report = bm.run(ARGV[3..-1])
  bm.plot(ARGV[3..-1]) if plot
  report.unshift(nil, nil, nil)

  puts "=====Test case====="
  p n
  for i in 3...ARGV.size
    puts "=====#{ARGV[i]}====="
    for k in 0...TITLE.size
      next if TITLE[k] == ""
      puts "#{ARGV[i]} #{TITLE[k]}"
      p report[i].map {|j| j[k]}
    end
  end
end

