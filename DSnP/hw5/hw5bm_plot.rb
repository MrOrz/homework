#!/usr/bin/env ruby
#
require 'rubygems'
require 'gnuplot'

class Hw5Benchmark
	def initialize(from, to, step = 1)
		@from = from
		@to = to
		@step = step
		@str = (0..to).map{ (0...10).map{ ('a'..'z').to_a[rand(26)] }.join }
	end

	def run(exes)
		exe_result = Array.new exes.size
		for k in n()
			test_for k
			p k
			for j in 0...exes.size
				IO.popen(exes[j] + ' -f bm.tmp') { |input|
					result = input.readlines
					time = result.map {|s| s.scan /Total time used  : ([\d\.]*) seconds/ }.reject {|s| s == [] }.flatten
					(0...time.size).to_a.reverse.each {|i| (i != 0) ? time[i] = (time[i].to_f - time[i-1].to_f)/k : time[i] = time[i].to_f / k }
					exe_result[j] ||= []
					exe_result[j] << (time)
				}
			end
		end
		#File.delete 'bm.tmp'
		exe_result
	end

	def n()
		i = @from
		result = []
		while i <= @to
			result << i.to_i
			i += @step
		end
		result
	end

	def test_for(i)
		File.open('bm.tmp', 'w') do |f|
			# insert
			(0...i).each { |j| f.puts "adta -s #{@str[j]}" }
			f.puts 'usage'
			# ++
			f.puts 'adtp'
			f.puts 'usage'
			# --
			f.puts 'adtp -r'
			f.puts 'usage'
			# erase
			(0...i).each { |j| f.puts "adtd -s #{@str[j]}" }
			f.puts 'usage'
			# pop_front
			(0...i).each { |j| f.puts "adta -s #{@str[j]}" }
			f.puts 'usage'
			f.puts "adtd -min #{i}"
			f.puts 'usage'
			# pop_back
			(0...i).each { |j| f.puts "adta -s #{@str[j]}" }
			f.puts 'usage'
			f.puts "adtd -max #{i}"
			f.puts 'usage'
			f.puts 'q -f'
		end
	end
end

class Hw5BenchmarkLog < Hw5Benchmark
	def initialize(from, to, density = 10.0)
		@from = from
		@to = to
		@density = density
		@str = (0..to).map{ (0...10).map{ ('a'..'z').to_a[rand(26)] }.join }
	end

	def n()
		i = @from
		result = []
		while i <= @to
			result << i.to_i
			i = (Math.exp(((1.0 / @density) * Math.log(10.0)) + Math.log(i)))
		end
		result
	end

	def run(exes)
		exe_result = Array.new exes.size
		delta = 0.00001
		for k in n()
			test_for k.to_i
			p k.to_i
			for j in 0...exes.size
				IO.popen(exes[j] + ' -f bm.tmp') { |input|
					result = input.readlines
					time = result.map {|s| s.scan /Total time used  : ([\d\.]*) seconds/ }.reject {|s| s == [] }.flatten
					(0...time.size).to_a.reverse.each {|i| ((i != 0) ? time[i] = time[i].to_f - time[i-1].to_f : time[i] = time[i].to_f) + delta }
					# + delta is used to prevent time[i] = 0
					exe_result[j] ||= []
					exe_result[j] << time
				}
			end
		end
		File.delete 'bm.tmp'
		exe_result
	end
end


if $PROGRAM_NAME == __FILE__
	if ARGV[0] == "--log"
		bm = Hw5BenchmarkLog.new(ARGV[1].to_i,ARGV[2].to_i,ARGV[3].to_f)
		exes = ARGV[4 .. -1]
		x = bm.n()
		report = bm.run(exes)
	else
		bm = Hw5Benchmark.new(ARGV[0].to_i,ARGV[1].to_i,ARGV[2].to_i)
		exes = ARGV[3 .. -1]
		x = bm.n()
		report = bm.run(exes)
	end

	titles = ["insert" , "it++" , "it--" , "erase" , "" , "pop front" ,"" , "pop back"]

	for j in 0 ... titles.size
		next if titles[j] == ""
		Gnuplot.open do |gp|
			Gnuplot::Plot.new( gp ) do |plot|
				plot.terminal "postscript eps"
				plot.output "#{titles[j].sub(/ / , '_')}.eps"
				#if ARGV[0] == "--log"
				#	plot.log "xy"
				#end
				plot.title  titles[j]
				plot.ylabel "time"
				plot.xlabel "n"

				for i in 0 ... exes.size
					plot.data << Gnuplot::DataSet.new([x , report[i].map {|v| v[j]}]) do |ds|
						ds.with = "linespoints"
						ds.title = exes[i]
						ds.linewidth=1
					end
				end
			end
		end
	end
	for i in 0 ... exes.size
		puts "=====#{exes[i]}====="
		puts "#{exes[i]} insert:"
		p report[i].map {|j| j[0]}
		puts "#{exes[i]} ++:"
		p report[i].map {|j| j[1]}
		puts "#{exes[i]} --:"
		p report[i].map {|j| j[2]}
		puts "#{exes[i]} erase:"
		p report[i].map {|j| j[3]}
		puts "#{exes[i]} pop front:"
		p report[i].map {|j| j[5]}
		puts "#{exes[i]} pop back:"
		p report[i].map {|j| j[7]}
	end
end

