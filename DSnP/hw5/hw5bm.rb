#!/usr/bin/env ruby
CHAR = ('a'..'z').to_a

class Hw5Benchmark
  def initialize(n)
    @n = n
    @str = (0..n[-1]).map{ (0...5).map{ CHAR[rand(26)] }.join }
  end

  def run(exes)
    exe_result = Array.new exes.size
    for i in @n
      test_for i
      for j in 0...exes.size
        result = IO.popen(exes[j] + ' -f bm.tmp').readlines
        time = result.map {|s| s.scan /Total time used  : ([\d\.]*) seconds/ }.reject {|s| s == [] }.flatten
        (0...time.size).to_a.reverse.each {|i| (i != 0) ? time[i] = time[i].to_f - time[i-1].to_f : time[i] = time[i].to_f }
        exe_result[j] ||= []
        exe_result[j] << time
      end
    end
    File.delete 'bm.tmp'
    exe_result
  end

  def test_for(i)
    File.open('bm.tmp', 'w') do |f|
      # insert
      (0...i).each { |j| f.puts "adta -s #{@str[j]}" }
      f.puts 'usage'
      # ++
      f.puts 'adtp'
      f.puts 'usage'
      # --
      f.puts 'adtp -r'
      f.puts 'usage'
      # erase
      (0...i).each { |j| f.puts "adtd -s #{@str[j]}" }
      f.puts 'usage'
      # pop_front
      (0...i).each { |j| f.puts "adta -s #{@str[j]}" }
      f.puts "adtd -min #{i}"
      f.puts 'usage'
      # pop_back
      (0...i).each { |j| f.puts "adta -s #{@str[j]}" }
      f.puts "adtd -max #{i}"
      f.puts 'usage'
      f.puts 'q -f'
    end
  end
end

if $PROGRAM_NAME == __FILE__
  if ARGV[0] == '--log'
    n = [ARGV[1].to_i]
    while n[-1] * 2 < ARGV[2].to_i
      n << n[-1] * 2
    end
  else
    n = ((ARGV[0].to_i)..(ARGV[1].to_i)).step(ARGV[2].to_i).to_a
  end
  report = Hw5Benchmark.new(n).run(ARGV[3..-1])
  report.unshift(nil, nil, nil)

  puts "=====Test case====="
  p n
  for i in 3...ARGV.size
    puts "=====#{ARGV[i]}====="
    puts "#{ARGV[i]} insert:"
    p report[i].map {|j| j[0]}
    puts "#{ARGV[i]} ++:"
    p report[i].map {|j| j[1]}
    puts "#{ARGV[i]} --:"
    p report[i].map {|j| j[2]}
    puts "#{ARGV[i]} erase:"
    p report[i].map {|j| j[3]}
    puts "#{ARGV[i]} pop front:"
    p report[i].map {|j| j[4]}
    puts "#{ARGV[i]} pop back:"
    p report[i].map {|j| j[5]}
  end
end

