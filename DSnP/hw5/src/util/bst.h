/****************************************************************************
  FileName     [ bst.h ]
  PackageName  [ util ]
  Synopsis     [ Define binary search tree package ]
  Author       [ Chung-Yang (Ric) Huang ]
  Copyright    [ Copyleft(c) 2005-2010 LaDs(III), GIEE, NTU, Taiwan ]
****************************************************************************/

#ifndef BST_H
#define BST_H

#define __DEBUG__

#include <cassert>

template <class T> class BSTree;

// BSTreeNode is supposed to be a private class. User don't need to see it.
// Only BSTree and BSTree::iterator can access it.
//
// DO NOT add any public data member or function to this class!!
//
template <class T>
class BSTreeNode
{
   friend class BSTree<T>;
   friend class BSTree<T>::iterator;
   // DONE: design your own class!!

   BSTreeNode(const T& k, BSTreeNode<T>* p = NULL):
   _key(k), _p(p), _left(NULL), _right(NULL) {}

   T              _key;    // the data used to compare
   BSTreeNode<T>* _p;      // parent. if = NULL, then the node is root or dummy
   BSTreeNode<T>* _left;
   BSTreeNode<T>* _right;

};


template <class T>
class BSTree
{
public:
   BSTree():_root(NULL), _dummy( new BSTreeNode<T>(T()) ), _size(0) {}
   ~BSTree() { clear(); }

   class iterator
   {
      friend class BSTree;

   public:
      iterator(BSTreeNode<T>* n= NULL,const BSTree<T> *t = NULL): _node(n), _tree(t) {}
      iterator(const iterator& i) : _node(i._node), _tree(i._tree) {}
      ~iterator() {} // Should NOT delete _node

      // overloaded operators
      const T& operator * () const { return _node->_key; }
      T& operator * () { return _node->_key; }
      iterator& operator ++ () {
         if(_node != _tree->_dummy){
            _node = _tree->_successor(_node);
            if(_node == NULL) _node = _tree->_dummy; // cannot find successor,
                                                     // set to dummy
         }
         return *this;
      }
      iterator operator ++ (int) {
         iterator tmp( *this );
         ++(*this);
         return tmp;
      }
      iterator& operator -- () {
         BSTreeNode<T>* original = _node;
         if(_node != _tree->_dummy){
            _node = _tree->_predecessor(_node);
         }
         else{ // now at dummy (pass the end of tree)
            // returns to the last node
            _node = _tree->_maximum(_tree->_root);
         }
         if(_node == NULL) _node = original;
            // cannot find predecessor, back to where what it was.
         return *this;
      }
      iterator operator -- (int) {
         iterator tmp( *this );
         --(*this);
         return tmp;
      }

      iterator& operator = (const iterator& i) {
         _node = i._node; _tree = i._tree;
         return *(this);
      }

      bool operator != (const iterator& i) const { return i._node != _node; }
      bool operator == (const iterator& i) const { return i._node == _node; }

   private:
      BSTreeNode<T>*       _node;
      const BSTree<T>*     _tree;
   };

   iterator begin() const {
      if( empty() ) return iterator( _dummy, (this) );
      return iterator(_minimun(_root), (this));
   }
   iterator end() const {
      return iterator(_dummy, (this));
   }
   bool empty() const { return _size == 0; }
   size_t size() const {  return _size; }

   void pop_front() {
      if(empty()) return;

      BSTreeNode<T>* z = _minimun(_root);
      if(z != NULL) _delete(z);
   }
   void pop_back() {
      if(empty()) return;

      BSTreeNode<T>* z = _maximum(_root);
      if(z != NULL) _delete(z);
   }

   // return false if nothing to erase
   bool erase(iterator pos) {
      BSTreeNode<T>* z = pos._node;
      if( z == _dummy || z == NULL ) return false; // empty tree

      _delete(z);
      return true;
   }
   bool erase(const T& k) {
      if( empty() ) return false;
      BSTreeNode<T>* z = _find(k);
      if( z == NULL ) return false; // fails to find k in tree

      _delete(z);
      return true;
   }

   void print(){}

   // return false if insertion fails
   bool insert(const T& n) {
      BSTreeNode<T>* z = new BSTreeNode<T>(n);
      BSTreeNode<T>* y = NULL, *x = _root;
         // x traces down the path, looking for NULL to insert z
         // y is trailing pointer, to the parent of x

      while(x != NULL){
         y = x;
         if(z->_key == x->_key) // x already exists
            return false;     // insertion fail

         if(z->_key < x->_key) // go left
            x = x->_left;
         else x = x->_right;    // go right
      }

      z->_p = y;

      if(y == NULL) // empty tree before insert
         _root = z;
      else if(z->_key < y->_key)
         y->_left = z;
      else
         y->_right = z;


      ++_size;
      return true;
   }

   void clear() { } // delete all nodes except for the dummy node

private:
   BSTreeNode<T>*  _root;  // == NULL if empty
   BSTreeNode<T>*  _dummy; // a node for end()
   size_t          _size;  // # of nodes

   // find minimun in the subtree rooted at x
   //
   BSTreeNode<T>* _minimun(BSTreeNode<T>* x) const{
      while (x->_left != NULL) x = x->_left;
      return x;
   }

   BSTreeNode<T>* _maximum(BSTreeNode<T>* x) const{
      while (x->_right != NULL) x = x->_right;
      return x;
   }

   // find x's predecessor
   //
   BSTreeNode<T>* _predecessor(BSTreeNode<T>* x) const{
      if(x->_left != NULL) // has non-empty left subtree
         return _maximum(x->_left);

      BSTreeNode<T>* y = x->_p;
      while(y != NULL && x == y->_left){
         x = y;
         y = y->_p;
      }
      return y;
   }

   // find x's successor
   //
   BSTreeNode<T>* _successor(BSTreeNode<T>* x) const{
      if(x->_right != NULL)  // has non-empty right subtree
         return _minimun(x->_right);

      BSTreeNode<T>* y = x->_p;
      while(y != NULL && x == y->_right){
         x = y;
         y = y->_p;
      }
      return y;
   }

   // replace the subtree rooted at u by the subtree rooted at v.
   //
   void _transplant(BSTreeNode<T>* &u, BSTreeNode<T>* &v){
      if(u->_p == NULL) _root = v;
      else if( u == u->_p->_left) u->_p->_left = v; // up->down relationship
      else u->_p->_right = v;                       // up->down relationship

      if(v != NULL)  // down->up relationship
         v->_p = u->_p;
   }

   // find x in tree
   // if not found, NULL is returned
   //
   BSTreeNode<T>* _find(const T& k){
      BSTreeNode<T>* x = _root;
      while(x != NULL){
         if(k == x->_key) return x; // found

         if(k < x->_key) x = x->_left;
         else x = x->_right;
      }
      return x;
   }

   // delete tree node z in tree
   // z must be a valid tree node inside this tree
   //
   void _delete(BSTreeNode<T>* z){
      if(z->_left == NULL)
         _transplant(z, z->_right);
      else if(z->_right == NULL )
         _transplant(z, z->_left);
      else{ // z has two children
         BSTreeNode<T>* y = _minimun(z->_right);
         if(y->_p != z){
            // y lies within z's right subtree
            _transplant(y, y->_right);
            y->_right = z->_right;
            y->_right->_p = y;
         }

         // replace z by y
         _transplant(z,y);
         y->_left = z->_left;
         y->_left->_p = y;
      }
      --_size;
      delete z;
   }
};

#endif // BST_H

