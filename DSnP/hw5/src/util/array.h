/****************************************************************************
  FileName     [ array.h ]
  PackageName  [ util ]
  Synopsis     [ Define dynamic array package ]
  Author       [ Chung-Yang (Ric) Huang ]
  Copyright    [ Copyleft(c) 2005-2010 LaDs(III), GIEE, NTU, Taiwan ]
****************************************************************************/

#ifndef ARRAY_H
#define ARRAY_H

#include <cassert>

// NO need to implement class ArrayNode
//
template <class T>
class Array
{
// #define __DEBUG__

public:
   Array() : _data(0), _size(0), _capacity(0) {}
   ~Array() { delete []_data; }

   // DO NOT add any more data member or function for class iterator
   class iterator
   {
      friend class Array;

   public:
      iterator(T* n= 0): _node(n) {}
      iterator(const iterator& i): _node(i._node) {}
      ~iterator() {} // Should NOT delete _node

      // DONE: implement these overloaded operators
      const T& operator * () const { return (*_node); }
      T& operator * () { return (*_node); }
      iterator& operator ++ () {
         ++_node;
         return (*this);
      }
      iterator operator ++ (int) {
         iterator tmp = (*this);
         ++_node;
         return tmp;
      }
      iterator& operator -- () {
         --_node;
         return (*this);
      }
      iterator operator -- (int) {
         iterator tmp = (*this);
         --_node;
         return tmp;
      }

      iterator operator + (int i) const { return iterator(_node + i); }
      iterator& operator += (int i) { _node += i; return (*this); }

      iterator& operator = (const iterator& i) {
         _node = i._node;
         return (*this);
      }

      bool operator != (const iterator& i) const { return _node != i._node; }
      bool operator == (const iterator& i) const { return _node == i._node; }

   private:
      T*    _node;
   };

   // DONE: implement these functions
   iterator begin() const { return iterator(_data); }
   iterator end() const { return iterator(_data+_size); }
   bool empty() const { return _size == 0; }
   size_t size() const { return _size; }

   T& operator [] (size_t i) { return _data[i]; }
   const T& operator [] (size_t i) const { return _data[i]; }

   void pop_front() {
      if( empty() ) return;

      for(size_t i = 1; i < _size; ++i ) _data[i-1] = _data[i];
      --_size;
   }

   void pop_back() {
      if( !empty() ) --_size;
   }

   bool erase(iterator pos) {
      if( empty() ) return false;

      for(iterator it = pos; it + 1 != end(); ++it ){
         *it = *(it+1);
      }
      --_size;
      return true;
   }

   bool erase(const T&x) {
      if( empty() )return false;
         // _size is unsigned and _size-1 is used in the following code,
         // thus it should ensure that _size > 0 before preceeding.

      size_t i = find(x); // binary search
      if(! (_data[i] == x) )return false; // x not found
      for(; i < _size-1; ++i )
         _data[i] = _data[i+1];

      --_size;
      return true;
   }


   bool insert(const T& x) {

      size_t pos = 0;

      if(!empty() ){
         pos = find(x);
         #ifdef __DEBUG__
         cerr << "insert pos: " << pos << endl;
         #endif
         if( _data[pos] == x )return false;
            // returns false if x already exists in the array
      }

      if( _capacity == _size ){ // should create new space for Array _data
         if(_capacity == 0)_capacity = 1; // _capacity is initially 0.
         _capacity *= 2;
         T* tmp = _data;

         // migrating existing elements to new T[]
         _data = new T[_capacity];
         for(size_t i = 0; i < _size; ++i)
            _data[i] = tmp[i];

         delete[] tmp;
      }
      // check if _data[pos] < x; that is, no existing elements in _data
      // is larger than x
      if(_data[pos] < x) ++pos; // if so, then x should be inserted at ++pos

      // shifting data after _data[pos].
      for(size_t i = _size; i > pos; --i)
         _data[i] = _data[i-1];

      // inserting the new element
      _data[pos] = x;
      ++_size;

      #ifdef __DEBUG__
      cerr << "capacity: " << _capacity << "  size:" << _size  << endl;
      #endif

      return true;
   }

   void clear() {
      _size = 0;
   }

   // Nice to have, but not required in this homework...
   // void reserve(size_t n) { ... }
   // void resize(size_t n) { ... }

private:
   T*           _data;
   size_t       _size;       // number of valid elements
   size_t       _capacity;   // max number of elements

   // [OPTIONAL DONE] Helper functions;

   // given x, use binary search to search for the position x should be.
   // returns the index of x inside the array if x already exists, or
   // the position of the smallest element > x in the array.
   // if no element in the array > x, than the biggest element is returned.
   //
   size_t find(const T& x) const{
      size_t s = 0, e = _size-1;
      while(e-s > 1){
         #ifdef __DEBUG__
            cerr << "data[" << s << "] = " << _data[s] << " <= "
                 << x << " < data[" << e << "] = " << _data[e] << endl;
         #endif

         size_t m = (s+e)/2;         // pivot: _data[m]
         if(x == _data[m]) return m;
         if(x < _data[m] ) e = m;
         else s = m;
      }
      if(x < _data[s]) return s;
      else return e;
   }
};

#endif // ARRAY_H

