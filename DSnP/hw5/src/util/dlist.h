/****************************************************************************
  FileName     [ dlist.h ]
  PackageName  [ util ]
  Synopsis     [ Define doubly linked list package ]
  Author       [ Chung-Yang (Ric) Huang ]
  Copyright    [ Copyleft(c) 2005-2010 LaDs(III), GIEE, NTU, Taiwan ]
****************************************************************************/

#ifndef DLIST_H
#define DLIST_H

//#define __DEBUG__
#include <cassert>

template <class T> class DList;

// DListNode is supposed to be a private class. User don't need to see it.
// Only DList and DList::iterator can access it.
//
// DO NOT add any public data member or function to this class!!
//
template <class T>
class DListNode
{

   friend class DList<T>;
   friend class DList<T>::iterator;

   DListNode(const T& d, DListNode<T>* p = 0, DListNode<T>* n = 0):
      _data(d), _prev(p), _next(n) {}

   T              _data;
   DListNode<T>*  _prev;
   DListNode<T>*  _next;
};


template <class T>
class DList
{
public:
   DList() {
      _head = new DListNode<T>(T());
      _head->_prev = _head->_next = _head; // _head is a dummy node
   }
   ~DList() { clear(); delete _head; }

   // DO NOT add any more data member or function for class iterator
   class iterator
   {
      friend class DList;

   public:
      iterator(DListNode<T>* n= 0): _node(n) {}
      iterator(const iterator& i) : _node(i._node) {}
      ~iterator() {} // Should NOT delete _node

      // DONE: implement these overloaded operators
      const T& operator * () const { return _node->_data; }
      T& operator * () { return _node->_data; }
      iterator& operator ++ () { _node = _node->_next; return *this; }
      iterator operator ++ (int) {
         iterator tmp( *this );
         _node = _node->_next;
         return tmp;
      }
      iterator& operator -- () { _node = _node->_prev; return *this; }
      iterator operator -- (int) {
         iterator tmp(*this);
         _node = _node->_prev;
         return tmp;
      }

      iterator& operator = (const iterator& i) {
         _node = i._node; return *(this);
      }

      bool operator != (const iterator& i) const { return i._node != _node; }
      bool operator == (const iterator& i) const { return i._node == _node; }

   private:
      DListNode<T>* _node;
   };

   // DONE: implement these functions
   iterator begin() const { return iterator(_head); }
   iterator end() const { return iterator( getDummy() ); } // pass-the-end
   bool empty() const { return _head == getDummy(); }
   size_t size() const {
      size_t s = 0;
      DListNode<T>* curr = _head;
      DListNode<T>* dummy = getDummy();

      while( curr != dummy ) {
         curr = curr->_next;
         ++s;
      }
      return s;
   }

   void pop_front() {
      if( empty() )return;
      DListNode<T> *del = _head; // node to delete
      DListNode<T> *dummy = getDummy();

      _head = _head->_next; //update _head

      // update _head->_prev and dummy->_next
      _head->_prev = dummy;
      dummy->_next = _head;

      delete del;
   }
   void pop_back() {
      if( empty() )return;
      DListNode<T> *del = getDummy()->_prev; // node to delete
      DListNode<T> *dummy = getDummy();

      if(del==_head) _head = dummy; // update _head if no elements left

      dummy->_prev = del->_prev;
      del->_prev->_next = dummy;

      delete del;
   }

   // return false if nothing to erase
   bool erase(iterator pos) {
      if( empty() ) return false;
      DListNode<T> *n = pos._node;

      n->_prev->_next = n->_next;
      n->_next->_prev = n->_prev;

      if(n == _head)_head = n->_next; // if pos._node is _head, update _head
      delete n;

      return true;
   }
   bool erase(const T& x) {
      DListNode<T> *n = find(x);
      if(! (x == n->_data) ) return false; // x does not exist

      n->_prev->_next = n->_next;
      n->_next->_prev = n->_prev;

      if(n == _head)_head = n->_next; // if n is _head, update _head
      delete n;

      return true;
   }
   // return false if insertion fails
   bool insert(const T& x) {

      DListNode<T>* next = find(x); // find the smallest node that is larger than x
      if(x == next->_data) return false; // already exists, insertion failed

      #ifdef __DEBUG__
      cerr << "head: " << (_head->_data) << ", next: " << next->_data << endl;
      #endif

      DListNode<T>* new_node = new DListNode<T>(x, next->_prev, next);
      next->_prev->_next = new_node;
      next->_prev = new_node;

      if(next == _head){
         // the new node is inserted at the beginning of DList,
         // thus _head should be updated.
         _head = new_node;

         // the relationship between _head and dummy node is already handled
         // before. No further actions needed.
      }

      return true;
   }

   // delete all nodes except for the dummy node
   //
   void clear() {
      DListNode<T> *dummy = getDummy(), *curr = _head, *next;
      while(curr != dummy){
         next = curr->_next;
         delete curr;
         curr = next;
      }
      #ifdef __DEBUG__
      assert(next==dummy); assert(dummy == curr);
      #endif
      _head = curr; // now next == dummy == curr
   }

private:
   DListNode<T>*  _head;  // = dummy node if list is empty

   // [OPTIONAL DONE] helper functions;
   inline DListNode<T>* getDummy() const{
      return _head->_prev;
   }


   // given x, use binary search to search for x.
   // returns the node inside the array if x already exists, or
   // the the smallest element > x in the array.
   // if no element in the array > x, then dummy node is returned.
   //
   DListNode<T>* find(const T& x){
      size_t s = 0, e = size(), m=0; // dummy node is the end initially
      DListNode<T>* curr = _head;

      while(e-s>1) {
         curr = _head;
         m = (s+e)/2;
         for(size_t i = 0; i<m; ++i) curr = curr->_next; // move curr to mth elem

         #ifdef __DEBUG__
//            cerr << "element[" << m << "]: " << curr->_data << " with s, e = "
//                 << s << ", " << e <<  endl;
         #endif

         if(x == curr->_data) return curr; // already found x
         if(x < curr->_data) e = m;
         else s = m;
      }

      curr = _head;
      for(size_t i = 0; i<s; ++i) curr = curr->_next; // move curr to sth elem

      if(x < curr->_data) return curr;
      else return curr->_next;

   }
};

#endif // DLIST_H

