/****************************************************************************
  FileName     [ cirGate.h ]
  PackageName  [ cir ]
  Synopsis     [ Define basic gate data structures ]
  Author       [ Chung-Yang (Ric) Huang ]
  Copyright    [ Copyleft(c) 2008-2010 LaDs(III), GIEE, NTU, Taiwan ]
****************************************************************************/

#ifndef CIR_GATE_H
#define CIR_GATE_H

#include <vector>
#include <string>

using namespace std;

enum GateType
{
   UNDEF_GATE = 0,
   PI_GATE    = 1,
   PO_GATE    = 2,
   AIG_GATE   = 3,
   CONST      = 4,

   TOT_GATE
};

static string gateTypeStr[TOT_GATE] = { "", "PI", "PO", "AIG", "CONST" };

//------------------------------------------------------------------------
//   Define classes
//------------------------------------------------------------------------
// TODO: You are free to define data members and member functions on your own
class CirAigGate
{
public:
   CirAigGate() {}
   CirAigGate(GateType t, size_t id, size_t lineNo=0)
      :_id(id), _lineNo(lineNo), _type(t){}
   //virtual ~CirAigGate() {}
   ~CirAigGate() {
      if(_id < _map.size()) _map[_id] = NULL;
      // if it is inside the map, remove the entry
   }

   // Basic access methods
   string getTypeStr(){ return gateTypeStr[_type]; }
   size_t getLineNo(){ return _lineNo; }
   string getSymName(){ return _sym_name; }
   void   setSymName(const string& s){ _sym_name = s; }

   // Methods about circuit construction
   static void resetMap(size_t s = 1){
      _map.resize(s);
      _map[0] = _const;
      for(size_t i = 1; i < s; ++i) _map[i] = NULL;
   }
   static CirAigGate* add(CirAigGate* g){ _map[g->_id] = g; return g; }
   static CirAigGate* find(size_t id){ return _map[id]; }
   void addFanIn(size_t literal_id){
      _fanin.push_back(FanIn(literal_id) );
   }

   // DFS methods
   static void resetDFS(){
      _dfs_num = 0;
      for(size_t i=1; i<_map.size(); ++i) if(_map[i])_map[i]->resetTraversed();
   }
   void resetTraversed(){_traversed = false;}
   void DFS(vector<CirAigGate*>& dfs_list);
   void reportNetList();

   // Methods about circuit simulation


   // Methods about fraig operation

   // Printing functions
   void reportGate() const;
   void reportFanin(unsigned level) const;
   void reportFanout(unsigned level) const;

private:
   static vector<CirAigGate*> _map;    // maps variable ID -> gate
                                       // the map is shared between all gates

   class FanIn{
   public:
      FanIn(size_t i):lid(i), id(i/2), inverted(i%2==1){};
      size_t      lid;         // literal id specified in the aag file
      size_t      id;          // variable id
      bool        inverted;    // inverted or not
      CirAigGate* gate(){
         return _map[id];
      }
   };
   vector<FanIn>     _fanin;        // fanin list, represented by FanIn Obj
   vector<size_t*>   _fanout;       // fanout list, represented by variable ID

   size_t               _id;           // variable ID
   size_t               _lineNo;       // defined line number
   GateType             _type;         // type
   string               _sym_name;     // symbolic name;

   static CirAigGate*   _const;   // dummy cell, representing constants
                                       // has variable ID '0'
protected:
   // gate ID, in0, in1, line number, type, value...?
   bool                    _traversed; // whether this gate is traversed or not
   static unsigned         _dfs_num;   // the printed number of DFS
};

#endif // CIR_GATE_H

