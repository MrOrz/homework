/****************************************************************************
  FileName     [ cirMgr.cpp ]
  PackageName  [ cir ]
  Synopsis     [ Define cir manager functions ]
  Author       [ Chung-Yang (Ric) Huang ]
  Copyright    [ Copyleft(c) 2008-2010 LaDs(III), GIEE, NTU, Taiwan ]
****************************************************************************/

#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cstdlib>
#include <ctype.h>
#include <cassert>
#include <cstring>
#include <exception>
#include "cirMgr.h"
#include "cirGate.h"

#define BUFSIZE 1024

using namespace std;

// TODO: You are free to define data members and member functions on your own
/*******************************/
/*   Global variable and enum  */
/*******************************/
CirMgr* cirMgr = 0;
bool CirMgr::_noopt = false;  // default: do optimization

//-----  For your reference only ------

enum CirParseError {
   EXTRA_SPACE,
   MISSING_SPACE,
   ILLEGAL_WSPACE,
   ILLEGAL_NUM,
   ILLEGAL_IDENTIFIER,
   ILLEGAL_SYMBOL_TYPE,
   ILLEGAL_SYMBOL_NAME,
   MISSING_NUM,
   MISSING_IDENTIFIER,
   MISSING_NEWLINE,
   MISSING_DEF,
   CANNOT_INVERTED,
   MAX_LIT_ID,
   REDEF_GATE,
   REDEF_SYMBOLIC_NAME,
   REDEF_CONST,
   NUM_TOO_SMALL,
   NUM_TOO_BIG,

   DUMMY_END
};


/**************************************/
/*   Static varaibles and functions   */
/**************************************/
//-----  For your reference only ------

static unsigned lineNo = 0;  // in printing, lineNo needs to ++
static unsigned colNo  = 0;  // in printing, colNo needs to ++
static char buf[BUFSIZE];
static string errMsg;
static int errInt;
static CirAigGate errGate;

static bool
parseError(CirParseError err)
{
   switch (err) {
      case EXTRA_SPACE:
         cerr << "[ERROR] Line " << lineNo+1 << ", Col " << colNo+1
              << ": Extra space character is detected!!" << endl;
         break;
      case MISSING_SPACE:
         cerr << "[ERROR] Line " << lineNo+1 << ", Col " << colNo+1
              << ": Missing space character!!" << endl;
         break;
      case ILLEGAL_WSPACE: // for non-space white space character
         cerr << "[ERROR] Line " << lineNo+1 << ", Col " << colNo+1
              << ": Illegal white space char(" << errInt
              << ") is detected!!" << endl;
         break;
      case ILLEGAL_NUM:
         cerr << "[ERROR] Line " << lineNo+1 << ": Illegal "
              << errMsg << "!!" << endl;
         break;
      case ILLEGAL_IDENTIFIER:
         cerr << "[ERROR] Line " << lineNo+1 << ": Illegal identifier \""
              << errMsg << "\"!!" << endl;
         break;
      case ILLEGAL_SYMBOL_TYPE:
         cerr << "[ERROR] Line " << lineNo+1 << ", Col " << colNo+1
              << ": Illegal symbol type (" << errMsg << ")!!" << endl;
         break;
      case ILLEGAL_SYMBOL_NAME:
         cerr << "[ERROR] Line " << lineNo+1 << ", Col " << colNo+1
              << ": Symbolic name contains un-printable char(" << errInt
              << ")!!" << endl;
         break;
      case MISSING_NUM:
         cerr << "[ERROR] Line " << lineNo+1 << ", Col " << colNo+1
              << ": Missing " << errMsg << "!!" << endl;
         break;
      case MISSING_IDENTIFIER:
         cerr << "[ERROR] Line " << lineNo+1 << ": Missing \""
              << errMsg << "\"!!" << endl;
         break;
      case MISSING_NEWLINE:
         cerr << "[ERROR] Line " << lineNo+1 << ", Col " << colNo+1
              << ": A new line is expected here!!" << endl;
         break;
      case MISSING_DEF:
         cerr << "[ERROR] Line " << lineNo+1 << ": Missing " << errMsg
              << " definition!!" << endl;
         break;
      case CANNOT_INVERTED:
         cerr << "[ERROR] Line " << lineNo+1 << ", Col " << colNo+1
              << ": " << errMsg << " " << errInt << "(" << errInt/2
              << ") cannot be inverted!!" << endl;
         break;
      case MAX_LIT_ID:
         cerr << "[ERROR] Line " << lineNo+1 << ", Col " << colNo+1
              << ": Literal \"" << errInt << "\" exceeds maximum valid ID!!"
              << endl;
         break;
      case REDEF_GATE:
         cerr << "[ERROR] Line " << lineNo+1 << ": Literal \"" << errInt
              << "\" is redefined, previously defined as "
              << errGate.getTypeStr() << " in line " << errGate.getLineNo()
              << "!!" << endl;
         break;
      case REDEF_SYMBOLIC_NAME:
         cerr << "[ERROR] Line " << lineNo+1 << ": Symbolic name for \""
              << errMsg << errInt << "\" is redefined!!" << endl;
         break;
      case REDEF_CONST:
         cerr << "[ERROR] Line " << lineNo+1 << ", Col " << colNo+1
              << ": Cannot redefine const (" << errInt << ")!!" << endl;
         break;
      case NUM_TOO_SMALL:
         cerr << "[ERROR] Line " << lineNo+1 << ": " << errMsg
              << " is too small (" << errInt << ")!!" << endl;
         break;
      case NUM_TOO_BIG:
         cerr << "[ERROR] Line " << lineNo+1 << ": " << errMsg
              << " is too big (" << errInt << ")!!" << endl;
         break;
      default: break;
   }
   return false;
}

// read single whtie space.
void get_whitespace(FILE* fin, bool allow_multiple_space = false)
   throw (CirParseError){

   // first should be whitespace
   errInt = getc(fin);
   if(errInt != ' ') throw MISSING_SPACE;
   ++colNo;

   if(allow_multiple_space) return;

   // second should be something other than whitespace
   errInt = getc(fin);
   if(errInt == ' ') throw EXTRA_SPACE;
   ungetc(errInt, fin);
}

// read '\n'
void get_newline(FILE* fin) throw (CirParseError){
   errInt = getc(fin);
   if(errInt != '\n') throw MISSING_NEWLINE;
   ++lineNo; colNo = 0;
}

// read a number
size_t get_number(FILE* fin, string illegal_message = "")
   throw (CirParseError)
{
   // first character should be a number, not a space or somethin else
   errMsg = illegal_message;
   errInt = getc(fin);
   if(errInt == ' ') throw EXTRA_SPACE;
   if(errInt == '\t') throw ILLEGAL_WSPACE;
   if(errInt > '9' || errInt < '0') throw MISSING_NUM;
   ungetc(errInt, fin);

   size_t num;
   // get the entire string
   fscanf(fin, "%s", buf);
   errMsg = illegal_message + "(" + buf + ")";
   char* end;
   num = strtol(buf, &end, 10);
   colNo += int(end-buf); // end - buf = 'length' of the number
   if(*end != '\0') throw ILLEGAL_NUM;
   return num;
}

/*************************************/
/*   class CirMgr member functions   */
/*************************************/
bool
CirMgr::readCircuit(const string& fileName)
{

   lineNo = colNo = 0;
   FILE* fin = fopen(fileName.c_str(), "r");
   if(!fin){ cerr << "Cannot open design \"" << fileName << "\"!!" ; return false; }
   size_t M,I,L,O,A; // params identified in the first line

   CirAigGate *tmp; // temp pointer to newly created gates

   try{
      // --- first line ---
      // identifier
      errInt = getc(fin);
      if(errInt==' ') throw EXTRA_SPACE;
      if(errInt=='\t') throw ILLEGAL_WSPACE;
      ungetc(errInt, fin);
      fscanf(fin, "%s", buf); errMsg = buf;
      if(strcmp(buf, "aag") != 0) throw ILLEGAL_IDENTIFIER;

      colNo += 3;

      get_whitespace(fin);

      // parameter M
      M = get_number(fin, "number of vars");
      get_whitespace(fin);
      CirAigGate::resetMap(M+1); // prepare a map of size m+1 for PIs and AIGs

      // parameter I
      I = get_number(fin, "number of PIs");
      get_whitespace(fin);
      _PI.reserve(I);

      // parameter L
      L = get_number(fin, "number of Latches");
      get_whitespace(fin);

      // parameter O
      O = get_number(fin, "number of POs");
      get_whitespace(fin);
      _PO.reserve(O);

      // parameter A
      A = get_number(fin, "number of AIGs");
      get_newline(fin);
      _AIG.reserve(A);

      // read list of PIs
      for(size_t i = 0; i < I; ++i){
         size_t old_col_number = colNo, new_col_number;
         errInt = get_number(fin, "PI");  // get literal id
         new_col_number = colNo;
         errMsg = "PI";

         // checking for error, recover the col number temporarily
         colNo = old_col_number;

         size_t id = errInt / 2; // variable id
         if(errInt%2 != 0) throw CANNOT_INVERTED;
         if(id > M) throw MAX_LIT_ID;

         tmp = CirAigGate::find(id); // check re-definition
         if( tmp != NULL ){ errGate = *( tmp ); throw REDEF_GATE; }

         tmp = CirAigGate::add( new CirAigGate(PI_GATE, id, lineNo) ); // create the gate
         _PI.push_back( tmp );  // update the lists

         // ready to preceed
         colNo = new_col_number;
         get_newline(fin);
      } // end of reading PIs

      // read list of POs
      for(size_t i = 0; i < O; ++i){
         size_t old_col_number = colNo, new_col_number;
         errInt = get_number(fin, "PO"); // get literal id
         new_col_number = colNo;
         errMsg = "PO";

         // checking for error, recover the col number temporarily
         colNo = old_col_number;

         size_t id = errInt / 2;  // variable id of fanin
         if(id > M) throw MAX_LIT_ID;

         tmp = new CirAigGate(PO_GATE, M+i+1, lineNo);
         _PO.push_back( tmp );
         tmp->addFanIn( errInt );

         // ready to preceed
         colNo = new_col_number;
         get_newline(fin);
      } // end of reading POs

      // read list of AIGs
      for(size_t i = 0; i < A; ++i){
         size_t old_col_number = colNo, new_col_number;

         // get literal ID
         errInt = get_number(fin, "AIG");
         new_col_number = colNo;
         errMsg = "AIG";

         size_t id = errInt / 2; // variable id
         // checking for error, recover the col number temporarily
         colNo = old_col_number;
         if(errInt % 2) throw CANNOT_INVERTED;
         if(id > M) throw MAX_LIT_ID;

         tmp = CirAigGate::find(id); // check re-definition
         if( tmp != NULL ){ errGate = *( tmp ); throw REDEF_GATE; }

         tmp = CirAigGate::add(new CirAigGate(AIG_GATE, id, lineNo));  // create the gate
         _AIG.push_back(tmp);   // update the lists


         // ready to preceed
         colNo = new_col_number;

         // get the two fanins
         for(size_t j = 0; j<2; ++j){
            get_whitespace(fin);

            old_col_number = colNo;

            // get literal ID
            errInt = get_number(fin, "AIG");
            new_col_number = colNo;
            errMsg = "AIG";

            if(size_t(errInt / 2) > M) throw MAX_LIT_ID;

            tmp->addFanIn(errInt);

            // ready to preceed
            colNo = new_col_number;
         } // end of reading fanins

         if(i<A-1) get_newline(fin);
      } // end of reading AIGs


      // if no symbolic name or comments,
      // the file should end without trailing '\n'
      // FIXME: always reads a '\n' even if the file is not ended with '\n'. WHY?
      get_newline(fin); // trigger feof = true

      // get symbolic names or comments

      while((errInt = getc(fin)) != EOF){
         if(errInt == 'c') break; // comments reached.
         else if(errInt == 'o' || errInt == 'i'){
            ++colNo; // proceed.

            CirAigGate* g;
            if(errInt == 'o'){   // output gates
               // get gate id
               errInt = get_number(fin, "symbol index");
               if(unsigned(errInt) > _PO.size()){
                  errMsg = "PO index"; throw NUM_TOO_BIG;
               }

               // get the gate which is referenced
               g = _PO[errInt];
               if(g->getSymName() != ""){
                  errMsg = 'o'; errMsg += errInt;
                  throw REDEF_SYMBOLIC_NAME;
               }
            }
            else{                // input gates
               // get gate id
               errInt = get_number(fin, "symbol index");
               if(unsigned(errInt) > _PI.size()){
                  errMsg = "PI index"; throw NUM_TOO_BIG;
               }

               // get the gate which is referenced
               g = _PI[errInt];
               if(!g->getSymName().empty()){
                  errMsg = 'i'; errMsg += errInt;
                  throw REDEF_SYMBOLIC_NAME;
               }
            }

            get_whitespace(fin, true); // allow multiple spaces here

            // read symbolic name
            fgets(buf, BUFSIZE, fin);
            if(feof(fin) || buf[0] == '\n'){
               errMsg = "symbolic name";
               throw MISSING_IDENTIFIER;
            }
            size_t i = 0;  // check if the sym name contains illegal characters
            for(;buf[i] != '\n' || buf[i] == '\0' ; ++i, ++colNo){
               errInt = buf[i];
               if(errInt < 32 || errInt > 127) throw ILLEGAL_SYMBOL_NAME;
            }
            if(buf[i] == '\n'){
               buf[i] = '\0';
               ungetc('\n', fin); // put '\n' back to stream, which is required
                                  // when re-entering while loop
            }

            // set symbolic name
            errMsg = buf;
            g->setSymName(errMsg);
         }
         else{ // illegal
            ungetc(errInt, fin); // put the error character back
            fscanf(fin, "%s", buf);   // and retrieve the whole word
            errMsg = buf;
            throw ILLEGAL_SYMBOL_TYPE;
         }

         get_newline(fin);
      } // end of reading symbolic names
   }
   catch(CirParseError err){
      fclose(fin);
      return parseError(err);
   }

   fclose(fin); // we're done with the file.

   _genDFSList(); // generate DFS lists.

   return true;
}

/*********************
Circuit Statistics
==================
  PI          20
  PO          12
  AIG        130
------------------
  Total      167
*********************/
void
CirMgr::printSummary() const
{
   printf("Circuit Statistics\n");
   printf("==================\n");
   printf("  PI    %8u\n", _PI.size() );
   printf("  PO    %8u\n", _PO.size() );
   printf("  AIG   %8u\n", _AIG.size() );
   printf("------------------\n");
   printf("  Total %8u\n", _PI.size() + _PO.size() + _AIG.size() );
}

void
CirMgr::printNetlist() const
{
   for(size_t i = 0; i < _dfs_list.size(); ++i){
      printf("[%d] ", i);
      _dfs_list[i]->reportNetList();
   }
}

void
CirMgr::printPIs() const
{
}

void
CirMgr::printPOs() const
{
}

void
CirMgr::printFloatGates() const
{
}

void
CirMgr::printFECPairs() const
{
}

void
CirMgr::_genDFSList(){
   CirAigGate::resetDFS(); // reset CirAigGate::_traversed

   // clear current _dfs_list
   _dfs_list.clear();
   _dfs_list.reserve(_PO.size() + _PI.size() + _AIG.size() );

   for(size_t i=0; i<_PO.size(); ++i){
      // POs are not in CirAigGate::_map, thus its 'traversed' flag is not
      // resetted by CirAigGate::resetDFS. We shall reset it here, before new DFS
      _PO[i]->resetTraversed();

      _PO[i]->DFS(_dfs_list);
   }
}

