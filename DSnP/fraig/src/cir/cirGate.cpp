/****************************************************************************
  FileName     [ cirGate.cpp ]
  PackageName  [ cir ]
  Synopsis     [ Define class CirAigGate member functions ]
  Author       [ Chung-Yang (Ric) Huang ]
  Copyright    [ Copyleft(c) 2008-2010 LaDs(III), GIEE, NTU, Taiwan ]
****************************************************************************/

#include <iostream>
#include <iomanip>
#include <stdarg.h>
#include "cirGate.h"
#include "util.h"

using namespace std;

// TODO: You are free to define data members and member functions on your own

// initializing static members
unsigned CirAigGate::_dfs_num = 0;
vector<CirAigGate*> CirAigGate::_map;
CirAigGate* CirAigGate::_const = new CirAigGate(CONST, 0);

void
CirAigGate::DFS(vector<CirAigGate*>& dfs_list){
   if(_traversed) return;
   _traversed = true;

   // post order traversal: visit the fanouts first
   for(vector<FanIn>::iterator it = _fanin.begin(); it != _fanin.end(); ++it){
      CirAigGate* g = _map[it->id];

      // skip undefined fanin floating) and const fanins
      if(g == NULL || g->_type == CONST) continue;
      else
         g->DFS(dfs_list);

   }

   // visiting itself
   dfs_list.push_back(this);
}

void
CirAigGate::reportNetList()
{
   // "<type> <gate-id>"
   printf("%-3s %d", gateTypeStr[_type].c_str(), _id);

   // print its fanin
   for(vector<FanIn>::iterator it = _fanin.begin(); it != _fanin.end(); ++it){
      CirAigGate* g = _map[it->id];

      if(g == NULL){ // fanout not defined (floating)
         if(it->inverted) printf(" *!%u", it->id);
         else printf(" *%u", it->id);
         continue;
      }

      if(g->_type == CONST){
         printf(" CONST%d\n", it->lid ); // it->lid must be 0 or 1
      }
      else{
          // print the fanin
         if(it->inverted) printf(" !%u", it->id);
         else printf(" %u", it->id);
      }
   }

   if(!_sym_name.empty()) printf(" (%s)", _sym_name.c_str() );
   printf("\n");
}

void
CirAigGate::reportGate() const
{

}

void
CirAigGate::reportFanin(unsigned level) const
{
}

void
CirAigGate::reportFanout(unsigned level) const
{
}

