/****************************************************************************
  FileName     [ cirMgr.h ]
  PackageName  [ cir ]
  Synopsis     [ Define circuit manager ]
  Author       [ Chung-Yang (Ric) Huang ]
  Copyright    [ Copyleft(c) 2008-2010 LaDs(III), GIEE, NTU, Taiwan ]
****************************************************************************/

#ifndef CIR_MGR_H
#define CIR_MGR_H

#include <vector>
#include <string>
#include <fstream>

#include "cirGate.h"

//class CirAigGate;

using namespace std;

// TODO: You are free to define data members and member functions on your own
class CirMgr
{
public:
   CirMgr() {}
   ~CirMgr() { deleteCircuit(); }

   // Access functions
   // return '0' if "gid" corresponds to an undefined gate.
   CirAigGate* getGate(unsigned gid) const { return CirAigGate::find(gid); }

   // Member functions about circuit construction
   bool readCircuit(const string&);
   void deleteCircuit() {
      // clear lists
      vector<CirAigGate*>::iterator it;
      for(it=_PO.begin(); it != _PO.end(); ++it) delete *it;
      for(it=_PI.begin(); it != _PI.end(); ++it) delete *it;
      for(it=_AIG.begin(); it != _AIG.end(); ++it) delete *it;

      _dfs_list.clear();
   }

   // Member functions about circuit optimization
   static void setNoOpt(bool noopt) { _noopt = noopt; }

   // Member functions about circuit reporting
   void printSummary() const;
   void printNetlist() const;
   void printPIs() const;
   void printPOs() const;
   void printFloatGates() const;
   void printFECPairs() const;

   // Member functions about fraig
   void strash();
   void setSimLog(ofstream *logFile) { _simLog = logFile; }
   void randomSim();
   void fileSim(ifstream&);
   void fraig();

   // Member functions about flags

private:
   // lists of pi, po, aig, total, dfs, floating, FEC...
   // Simulation, fraig related...
   ofstream    *_simLog;
   static bool  _noopt;

   // gates in the order of AAG file
   vector<CirAigGate*> _PO;
   vector<CirAigGate*> _PI;
   vector<CirAigGate*> _AIG;

   // dfs list
   vector<CirAigGate*> _dfs_list;
   void _genDFSList();

   // private member functions for circuit parsing
};

#endif // CIR_MGR_H

