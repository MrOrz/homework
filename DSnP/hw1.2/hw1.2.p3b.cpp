#include <iostream>
#include <vector>

using namespace std;

void printVec(vector<int> nums){
  for(unsigned i=0; i<nums.size(); ++i)
    cout << nums[i] << " ";  
  cout << endl; 
}

void mySwap(int &i, int &j){
  int tmp = i; i = j; j = tmp;
}

class Compare{
  public:
    virtual bool operator () (const int&,const int&) const = 0;
};
class Less:public Compare{
  public:
    bool operator() (const int& a, const int& b) const {
      return a<b;
    }
};
class Greater:public Compare{
  public:
    bool operator() (const int& a, const int& b) const {
      return a>b;
    }
};

int compare(const int& i, const int& j){
  return i<j;
}

void selectionSort(vector<int>& array, const Compare& compare)
{
  for (size_t i = 0, n = array.size(); i < n - 1; ++i) {
    size_t pivot = i;
    for (size_t j = i+1; j < n; ++j) {
      if (!compare(array[pivot], array[j]))
        pivot = j;
    }
    if (pivot != i)
      mySwap(array[pivot], array[i]);
  }
}

int main(){
  // input.
  int count;
  vector<int> nums;
  cout << "How many numbers? ";
  cin >> count;
  nums.resize(count);
  for(int i=0; i<count; ++i)
    cin >> nums[i];

  cout << "Before sort:" << endl;
  printVec(nums);

  // ascending sort.
  selectionSort(nums, Less());
  cout << "Ascedning sort:" << endl;
  printVec(nums);

  // descending sort.
  selectionSort(nums, Greater());
  cout << "Descedning sort:" << endl;
  printVec(nums);
  
  return 0;
}
