/****************************************************************************
  FileName     [ Matrix.h ]
  PackageName  [ HW1.2.P2 ]
  Synopsis     [ A matrix program for HW1.2 of DSnP class. The code is not
                 very efficient as there are many object copies. However,
                 this is to get the students familiar with C++ class usage
                 and understand the different between "return-by-object" and
                 "return-by-reference". ]
  Author       [ Chung-Yang (Ric) Huang ]
  Copyright    [ Copyleft(c) 2010 LaDs(III), GIEE, NTU, Taiwan ]
****************************************************************************/

#include <iostream>
#include <iomanip>
#include "matrix.h"

using namespace std;

Matrix inputMatrix()
{
   cout << "Please enter data carefully. \nThere\'s no checking between "
        << "the number of data entered and the matrix size" << endl;

   unsigned nRows, nCols;
   cout << "Enter num of rows: ";
   cin >> nRows;
   cout << "Enter num of cols: ";
   cin >> nCols;

   Matrix m(nRows, nCols);

   for (unsigned i = 0; i < nRows; ++i) {
      cout << "Enter data for row " << i << " : ";
      for (unsigned j = 0; j < nCols; ++j) {
         unsigned data;
         cin >> data;
         m[i][j] = data;
      }
   }

   return m;
}

ostream& operator << (ostream& os, const Matrix& m)
{
   unsigned d = m.maxElm();
   unsigned nDigits = 1;
   while (d >= 10) { d /= 10; ++nDigits; }
   cout << " /" << right << setw(m.columns() * (nDigits + 2) + 3) << "\\\n";
   for (unsigned i = 0, r = m.rows(); i < r; ++i) {
      cout << "|";
      for (unsigned j = 0, c = m.columns(); j < c; ++j)
         cout << right << setw(nDigits + 2) << m[i][j];
      cout << "   |" << endl;
   }
   cout << " \\" << right << setw(m.columns() * (nDigits + 2) + 3) << "/\n";
   return os;
}

int main()
{
   cout << "Enter 3 matrices m1, m2, m3. We will perform (m1 + m2) * m3.\n";
   cout << "==== m1 ====" << endl;
   Matrix m1 = inputMatrix();
   cout << "==== m2 ====" << endl;
   Matrix m2 = inputMatrix();
   cout << "==== m3 ====" << endl;
   Matrix m3 = inputMatrix();

   cout << endl;
   cout << "m1, m2, m3 are..." << endl;
   cout << m1 << m2 << m3;

   Matrix ans = m1 + m2;
   cout << endl;
   cout << "M1 + M2 = " << endl << ans << endl;

   ans *= m3;
   cout << endl;
   cout << "(M1 + M2) * M3 = " << endl << ans << endl;
}
