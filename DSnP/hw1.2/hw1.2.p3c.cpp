#include <iostream>
#include <vector>
#include <string>

using namespace std;

template <class T>
void printVec(vector<T> nums){
  for(unsigned i=0; i<nums.size(); ++i)
    cout << nums[i] << " ";  
  cout << endl; 
}

template <class T>
void mySwap(T &i, T &j){
  T tmp = i; i = j; j = tmp;
}

template <class T>
class Compare{
  public:
    virtual bool operator () (const T&,const T&) const = 0;
};

template <class T>
class Less:public Compare<T>{
  public:
    bool operator() (const T& a, const T& b) const {
      return a<b;
    }
};

template <class T>
class Greater:public Compare<T>{
  public:
    bool operator() (const T& a, const T& b) const {
      return a>b;
    }
};


template <class T>
void selectionSort(vector<T>& array, const Compare<T>& compare)
{
  for (size_t i = 0, n = array.size(); i < n - 1; ++i) {
    size_t pivot = i;
    for (size_t j = i+1; j < n; ++j) {
      if (!compare(array[pivot], array[j]))
        pivot = j;
    }
    if (pivot != i)
      mySwap<T>(array[pivot], array[i]);
  }
}

template <class T>
vector<T> getNums(){
  int count;
  vector<T> nums;

  cin >> count;
  nums.resize(count);
  for(int i=0; i<count; ++i)
    cin >> nums[i];

  cout << "Before sort:" << endl;
  printVec<T>(nums);
  return nums;
}

int main(){
  cout << "How many strings? ";
  vector<string> nums = getNums<string>();
  // ascending sort.
  selectionSort(nums, Less<string>());
  cout << "Ascedning sort:" << endl;
  printVec<string>(nums);
  
  cout << "How many doubles? ";
  vector<double> nums2 = getNums<double>();
  // descending sort.
  selectionSort(nums2, Greater<double>());
  cout << "Descedning sort:" << endl;
  printVec<double>(nums2);
  return 0;
}
