#include <iostream>
#include <vector>

using namespace std;

void printVec(vector<int> nums){
  for(unsigned i=0; i<nums.size(); ++i)
    cout << nums[i] << " ";  
  cout << endl; 
}

void mySwap(int &i, int &j){
  int tmp = i; i = j; j = tmp;
}

int compare(const int& i, const int& j){
  return i<j;
}

void selectionSort(vector<int>& array)
{
  for (size_t i = 0, n = array.size(); i < n - 1; ++i) {
    size_t pivot = i;
    for (size_t j = i+1; j < n; ++j) {
      if (!compare(array[pivot], array[j]))
        pivot = j;
    }
    if (pivot != i)
      mySwap(array[pivot], array[i]);
  }
}

int main(){
  // input.
  int count;
  vector<int> nums;
  cout << "How many numbers? ";
  cin >> count;
  nums.resize(count);
  for(int i=0; i<count; ++i)
    cin >> nums[i];

  cout << "Before sort:" << endl;
  printVec(nums);
  cout << endl;
  // sort.
  selectionSort(nums);
 
  // output.
  cout << "After sort:" << endl;
  printVec(nums);
  return 0;
}
