/****************************************************************************
  FileName     [ Matrix.h ]
  PackageName  [ HW1.2.P2 ]
  Synopsis     [ Class definitions for matrix in HW1.2 of DSnP class ]
  Author       [ Chung-Yang (Ric) Huang ]
  Copyright    [ Copyleft(c) 2010 LaDs(III), GIEE, NTU, Taiwan ]
****************************************************************************/

#ifndef MATRIX_H
#define MATRIX_H

#include <iostream>

using namespace std;

// Act as a "row" in class Matrix
// Feel free to add member functions for the TODO's,
// but do not change/add/delete data members
class Vector
{
public:
   Vector(unsigned size = 0):_size(size) {
      _data = new unsigned[_size];
   } // TODO
   ~Vector() {
      delete[] _data;
   } // TODO: need to release all the memory

   unsigned size() const { return _size; }

   // cerr an error message if i >= _size; return garbage
   unsigned& operator[] (unsigned i) { 
     if(i >= _size)
       cerr << "out of scope subscript detected for Vector[]; "
            << "i mod _size is returned instead." << endl;
     return _data[i%_size];
   } // TODO
   const unsigned& operator[] (unsigned i) const {
     if(i >= _size)
       cerr << "out of scope subscript detected for Vector[]; "
            << "i mod _size is returned instead." << endl;
     return _data[i%_size];
   } // TODO
   
   void resize(unsigned size){
     _size = size;
     delete[] _data;
     _data = new unsigned[_size];
   }

private:
   unsigned     _size;  // number of elements
   unsigned    *_data;  // dynamic array
};

// Feel free to add member functions for the TODO's,
// but do not change/add/delete data members
class Matrix
{
public:
   Matrix(unsigned numRows = 0, unsigned numColumns = 0):_nRows(numRows){
      init(numColumns);
   } // TODO
   Matrix(const Matrix& m):_nRows(m.rows()){
     init(m.columns());
     map(m);
   } // TODO: copy constructor
   ~Matrix() {
      delete[] _data;
   } // TODO: need to release all the memory

   unsigned rows() const { return _nRows; }
   unsigned columns() const { return (_nRows? _data[0].size(): 0); }

   // cerr an error message if i >= _nRows; return garbage
   Vector& operator[] (unsigned i) {
     if(i >= _nRows)
       cerr << "out of scope subscript detected for Matrix[]; "
            << "i mod _size is returned instead." << endl;
     return _data[i%_nRows];
   } // TODO
   const Vector& operator[] (unsigned i) const {
     if(i >= _nRows)
       cerr << "out of scope subscript detected for Matrix[]; "
            << "i mod _size is returned instead." << endl;
     return _data[i%_nRows];
   } // TODO

   // If matrix sizes do not match, cerr a message return a copy of (*this)
   Matrix operator + (const Matrix& m) const {
     if(m.rows() != _nRows || m.columns() != columns() ){
        cerr << "matrix sizes not match for matrix add operation; "
             << "LHS matrix is returned instead." << endl;
        return *this;
     }else{
        Matrix sum(_nRows, columns());
        for(unsigned i=0; i<_nRows; ++i)
           for(unsigned j=0; j<columns(); ++j)
              sum[i][j] = _data[i][j] + m[i][j];
        return sum;
     }
   } // TODO
   // If matrix sizes do not match, cerr a message return (*this)
   Matrix& operator += (const Matrix& m) { 
     if(m.rows() != _nRows || m.columns() != columns() ){
        cerr << "matrix sizes not match for matrix add operation; "
             << "LHS matrix is returned instead." << endl;
     }else{
        for(unsigned i=0; i<_nRows; ++i)
           for(unsigned j=0; j<columns(); ++j)
              _data[i][j] += m[i][j];
     }
     return *this;
   } // TODO

   // If matrix sizes do not match, cerr a message return a copy of (*this)
   Matrix operator * (const Matrix& m) const {
     if(m.rows()!=columns()){
        cerr << "matrix sizes not match for matrix add operation; "
             << "LHS matrix is returned instead." << endl;
        return *this;
     }else{
        Matrix product(_nRows, m.columns());
        for(unsigned i=0; i<product.rows(); ++i)
           for(unsigned j=0; j<product.columns(); ++j){
              product[i][j] = 0;
              for(unsigned k=0; k < columns(); ++k)
                 product[i][j] += _data[i][k] * m[k][j];
           }
        return product;
     }
   } // TODO
   // If matrix sizes do not match, cerr a message return (*this)
   Matrix& operator *= (const Matrix& m) {
     Matrix product = (*this) * m; 
     resize(product.rows(), product.columns());
     map(product);
     return *this;
   } // TODO

   friend ostream& operator << (ostream&, const Matrix&);

private:
   unsigned     _nRows;
   Vector      *_data;
   // auxilary functions
   unsigned maxElm() const {
      unsigned d = 0;
      for (unsigned i = 0; i < _nRows; ++i)
         for (unsigned j = 0, n = columns(); j < n; ++j)
            if (d < _data[i][j]) d = _data[i][j];
      return d;
   }
   void resize(unsigned numRows, unsigned numColumns){
     delete[] _data;
     _nRows = numRows;
     init(numColumns);
   }
   void init(unsigned numColumns){
     _data = new Vector[_nRows];
     for(unsigned i=0;i<_nRows; ++i)
       _data[i].resize(numColumns);
   }
   void map(const Matrix& m){
     for(unsigned i=0; i<_nRows; ++i) 
        for(unsigned j=0; j<columns(); ++j )
           _data[i][j] = m[i][j];
   }
};

#endif // MATRIX_H
