/****************************************************************************
  FileName     [ permute.cpp ]
  PackageName  [ HW1.2.P1 ]
  Synopsis     [ A permutation program for HW1.2 of DSnP class ]
  Author       [ Chung-Yang (Ric) Huang ]
  Copyright    [ Copyleft(c) 2010 LaDs(III), GIEE, NTU, Taiwan ]
****************************************************************************/
#include <iostream>
#include <sstream>

using namespace std;

const int MAX_DIGIT = 10;

//-----------------------------------------------------------
//   Define static functions
//-----------------------------------------------------------
static void readNum(int& num, const string& prompt)
{
   while (true) {
      cout << prompt;
      cin >> num;
      if (!cin.fail()) break;
      cin.clear();
      string str; cin >> str;
      cerr << "Error: \"" << str << "\" is not a number!!" << endl;
   }
}

//-----------------------------------------------------------
//   Define classes
//-----------------------------------------------------------
// Global static variables for permutations
class PermuteNum
{
public:
   PermuteNum() : _digit(0), _nPermutes(0) {
      for (int i = 0; i < MAX_DIGIT; i++)
         _nums[i] = 0;
   }

   void inputNum();
   void permute(int = 0);
   void summary() const;

private:
   int _digit;
   int _nums[MAX_DIGIT];
   int _orig[MAX_DIGIT];
   int _nPermutes;

   static bool readDigit(int&);
   void swap(int i, int j) {
      int tmp = _nums[i]; _nums[i] = _nums[j]; _nums[j] = tmp; }
};

//-----------------------------------------------------------
//   Define member functions 
//-----------------------------------------------------------
void
PermuteNum::inputNum()
{
   static stringstream st;
   st << "Please specify number of digits (<= " << MAX_DIGIT << ")? ";
   while (true) {
      readNum(_digit, st.str());
      if (_digit >= 1 && _digit <= MAX_DIGIT)
         break;
      cerr<< "Error: Illegal number of digits!!" << endl;
   }

   cout << "Please enter " << _digit << " single-digit numbers: ";
   for (int i = 0; i < _digit; i++)
      while (!readDigit(_nums[i]));

   // backup...
   for (int i = 0; i < _digit; i++)
      _orig[i] = _nums[i];
   // sort data...
   for (int i = 0; i < _digit; i++) {
      int min = i;
      for (int j = i+1; j < _digit; j++)
         if (_nums[min] > _nums[j]) min = j;
      swap(i, min);
   }
   cout << "=== The permutations of { ";
   for (int i = 0; i < _digit - 1; i++)
      cout << _orig[i] << ", ";
   cout << _orig[_digit - 1] << " } are ===" << endl;
}

void 
PermuteNum::permute(int iBegin)
{
   // base case
   if (iBegin == (_digit - 1)) {
      _nPermutes++;
      cout << _nPermutes << " ";
      for (int i = 0; i < _digit; i++)
         cout << _nums[i];
      cout << endl;
      cout << " \b";  // line 102; this is a dummy for break point
      return;
   }

   // Recursive case
   int idx = iBegin;
   while (true) {
      permute(iBegin+1);
      // find next different digit; break if not found
      while ((++idx < _digit) && (_nums[idx] == _nums[iBegin]));
      if (idx < _digit)
         swap(idx, iBegin);
      else break;
   }
}

void
PermuteNum::summary() const
{
   cout << "=== There are totally " << _nPermutes << " permutations of { ";
   for (int i = 0; i < _digit - 1; i++)
      cout << _orig[i] << ", ";
   cout << _orig[_digit - 1] << " } ===" << endl;
}

bool
PermuteNum::readDigit(int& d)
{
   char ch = cin.get();
   while (ch == ' ' || ch == '\t' || ch == '\b' || ch == '\n')
      ch = cin.get();
   if (ch < '0' || ch > '9') {
      cerr << "Error: Illegal character \'" << ch << "'!!" << endl;
      return false;
   }
   d = int(ch - '0');
   return true;
}

//-----------------------------------------------------------
//   Define main()
//-----------------------------------------------------------
int main()
{
   PermuteNum pn;

   pn.inputNum();
   pn.permute();

   pn.summary();
}
