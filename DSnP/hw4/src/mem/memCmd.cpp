/****************************************************************************
  FileName     [ memCmd.cpp ]
  PackageName  [ mem ]
  Synopsis     [ Define memory test commands ]
  Author       [ Chung-Yang (Ric) Huang ]
  Copyright    [ Copyleft(c) 2007-2010 LaDs(III), GIEE, NTU, Taiwan ]
****************************************************************************/
#include <iostream>
#include <iomanip>
#include "memCmd.h"
#include "memTest.h"
#include "cmdParser.h"
#include "util.h"

using namespace std;

extern MemTest mtest;  // defined in memTest.cpp

bool
initMemCmd()
{
   if (!(cmdMgr->regCmd("MTReset", 3, new MTResetCmd) &&
         cmdMgr->regCmd("MTNew", 3, new MTNewCmd) &&
         cmdMgr->regCmd("MTDelete", 3, new MTDeleteCmd) &&
         cmdMgr->regCmd("MTPrint", 3, new MTPrintCmd)
      )) {
      cerr << "Registering \"mem\" commands fails... exiting" << endl;
      return false;
   }
   return true;
}

// static helper function
// returns if a parameter matches a given pattern
bool isParam(const string& str, const string& param, const size_t& mandatory){
   if(str.size() > param.size() || str.size() < mandatory )
      return false;

   if( myStrNCmp( param, str, str.size() ) != 0 )
      return false;

   return true;
}

//----------------------------------------------------------------------
//    MTReset [(size_t blockSize)]
//----------------------------------------------------------------------
CmdExecStatus
MTResetCmd::exec(const string& option)
{
   // check option
   string token;
   if (!CmdExec::lexSingleOption(option, token))
      return CMD_EXEC_ERROR;
   if (token.size()) {
      int b;
      if (!myStr2Int(token, b) || b < int(sizeof(MemTestObj))) {
         cerr << "Illegal block size (" << token << ")!!" << endl;
         return CmdExec::errorOption(CMD_OPT_ILLEGAL, token);
      }
      #ifdef MEM_MGR_H
      mtest.reset(toSizeT(b));
      #else
      mtest.reset();
      #endif // MEM_MGR_H
   }
   else
      mtest.reset();
   return CMD_EXEC_DONE;
}

void
MTResetCmd::usage(ostream& os) const
{
   os << "Usage: MTReset [(size_t blockSize)]" << endl;
}

void
MTResetCmd::help() const
{
   cout << setw(15) << left << "MTReset: "
        << "(memory test) reset memory manager" << endl;
}


//----------------------------------------------------------------------
//    MTNew <(size_t numObjects)> [-Array (size_t arraySize)]
//----------------------------------------------------------------------
CmdExecStatus
MTNewCmd::exec(const string& option)
{
  // DONE

  vector<string> options;
  CmdExec::lexOptions(option, options);

  size_t n = options.size(), numObjects = 0, arraySize = 0;
  int tmp;

  // checking options
  for( size_t i = 0; i < n; ++i ){
    if (i >= 3) return CmdExec::errorOption(CMD_OPT_EXTRA, options[i]);

    if( isParam( options[i], "-Array", 2 ) ){ // -Array param
      ++i;  // reading the next param, which should be arraySize

      if( i >= n) // no options left, expecting arraySize
        return CmdExec::errorOption(CMD_OPT_MISSING, "");

      if ( !myStr2Int( options[i], tmp ) || tmp <= 0 )
        return CmdExec::errorOption(CMD_OPT_ILLEGAL, options[i]);

      arraySize = tmp;
    }
    else if( myStr2Int( options[i], tmp ) && tmp > 0 && numObjects == 0){
      numObjects = tmp;
    }
    else
      return CmdExec::errorOption(CMD_OPT_ILLEGAL, options[i]);
  }

  if(numObjects == 0) // "numObjects" is not set
    return CmdExec::errorOption(CMD_OPT_MISSING, "");

  // execute command.

  try {
    if(arraySize == 0)  // creating a single object
      mtest.newObjs(numObjects);
    else  // creating an object array
      mtest.newArrs(numObjects, arraySize);
  }
  catch(bad_alloc&){}

  return CMD_EXEC_DONE;
}

void
MTNewCmd::usage(ostream& os) const
{
   os << "Usage: MTNew <(size_t numObjects)> [-Array (size_t arraySize)]\n";
}

void
MTNewCmd::help() const
{
   cout << setw(15) << left << "MTNew: "
        << "(memory test) new objects" << endl;
}


//----------------------------------------------------------------------
//    MTDelete <-Index (size_t objId) | -Random (size_t numRandId)> [-Array]
//----------------------------------------------------------------------
CmdExecStatus
MTDeleteCmd::exec(const string& option)
{
  // TODO

  vector<string> options;
  CmdExec::lexOptions(option, options);

  size_t n = options.size(), id = 0, id_oid, num = 0; bool arr_flag = false;

  int tmp;

  // checking options
  for( size_t i = 0; i < n; ++i ){
    if (i >= 3) return CmdExec::errorOption(CMD_OPT_EXTRA, options[i]);

    if( isParam( options[i], "-Array", 2 ) ) // -Array param
      arr_flag = true;
    else if( isParam( options[i], "-Index", 2 ) ){
      ++i;  // reading the next param, which should be objId

      if( i >= n) // no options left, expecting arraySize
        return CmdExec::errorOption(CMD_OPT_MISSING, "");

      if ( !myStr2Int( options[i], tmp ) || tmp < 0 )
        return CmdExec::errorOption(CMD_OPT_ILLEGAL, options[i]);

      id = tmp;
      id_oid = i; // keep track of the option id for "id"
    }
    else if( isParam( options[i], "-Random", 2 ) ){
      ++i;  // reading the next param, which should be numRandId

      if( i >= n) // no options left, expecting arraySize
        return CmdExec::errorOption(CMD_OPT_MISSING, "");

      if ( !myStr2Int( options[i], tmp ) || tmp <= 0 )
        return CmdExec::errorOption(CMD_OPT_ILLEGAL, options[i]);

      num = tmp;
    }
    else
      return CmdExec::errorOption(CMD_OPT_ILLEGAL, options[i]);
  }

  if(n <= 1) // either -Index or -Random requires 2+ params
    return CmdExec::errorOption(CMD_OPT_MISSING, "");

  if(num != 0){ // random
    if(arr_flag)
      for(size_t i = 0; i < num; ++i){
         mtest.deleteArr( rnGen( mtest.getArrListSize() ) );
      }
    else
      for(size_t i = 0; i < num; ++i){
         mtest.deleteObj( rnGen( mtest.getObjListSize() ) );
      }
  }else{        // delete by index
    if(arr_flag){
      if(id >= mtest.getArrListSize())
        return CmdExec::errorOption(CMD_OPT_ILLEGAL, options[id_oid] );
      mtest.deleteArr(id);
    }
    else{
      if(id >= mtest.getObjListSize())
        return CmdExec::errorOption(CMD_OPT_ILLEGAL, options[id_oid] );
      mtest.deleteObj(id);
    }
  }

  return CMD_EXEC_DONE;
}

void
MTDeleteCmd::usage(ostream& os) const
{
   os << "Usage: MTDelete <-Index (size_t objId) | "
      << "-Random (size_t numRandId)> [-Array]" << endl;
}

void
MTDeleteCmd::help() const
{
   cout << setw(15) << left << "MTDelete: "
        << "(memory test) delete objects" << endl;
}


//----------------------------------------------------------------------
//    MTPrint
//----------------------------------------------------------------------
CmdExecStatus
MTPrintCmd::exec(const string& option)
{
   // check option
   if (option.size())
      return CmdExec::errorOption(CMD_OPT_EXTRA, option);
   mtest.print();

   return CMD_EXEC_DONE;
}

void
MTPrintCmd::usage(ostream& os) const
{
   os << "Usage: MTPrint" << endl;
}

void
MTPrintCmd::help() const
{
   cout << setw(15) << left << "MTPrint: "
        << "(memory test) print memory manager info" << endl;
}

