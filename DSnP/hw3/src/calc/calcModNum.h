/****************************************************************************
  FileName     [ calcModNum.h ]
  PackageName  [ calc ]
  Synopsis     [ Define class ModNum ]
  Author       [ Chung-Yang (Ric) Huang ]
  Copyright    [ Copyleft(c) 2007-2010 LaDs(III), GIEE, NTU, Taiwan ]
****************************************************************************/
#ifndef CALC_MOD_NUM_H
#define CALC_MOD_NUM_H

#include <iostream>
#include <vector>
#include <map>

using namespace std;

class ModNum;

typedef map<string, ModNum> CalcMap;

class ModNum
{
public:
   // Default constructor
   // ==> Make sure _num is within [0, _modulus)
   // CHANGED: Implement the default constructor;
   ModNum(int i = 0):_num(i%_modulus) {
      if(_num < 0) _num += _modulus;
   }
   // Get the ModNum from the _varMap based on the string "str".
   // If found, copy the value.
   // If not found, create a default one and insert to the _varMap.
   ModNum(const string& str) { _num = _varMap[str]._num; }

   // Operator overload
   // CHANGED: overload the following operators (+, +=, -, -=, *, *=, ==, !=, =)
   //
   ModNum operator + (const ModNum& i) const{
      return ModNum(_num + i._num);
   }

   ModNum& operator += (const ModNum& i){
      _num = (_num + i._num) % _modulus;
      return (*this);
   }

   ModNum operator - (const ModNum& i) const{
      return ModNum(_num - i._num);
   }

   ModNum operator - () const{
      return ModNum( _num * -1 );
   }

   ModNum& operator -= (const ModNum& i){
      _num = (_num - i._num) % _modulus;
      if(_num < 0) _num += _modulus;
      return (*this);
   }

   ModNum operator * (const ModNum& i) const{
      return ModNum(_num * i._num);
   }

   ModNum& operator *= (const ModNum& i){
      _num = (_num * i._num) % _modulus;
      return (*this);
   }

   bool operator == (const ModNum& i) const{
      return _num == i._num;
   }

   bool operator != (const ModNum& i) const{
      return _num != i._num;
   }

   ModNum& operator = (const ModNum& i){
      _num = i._num;
      return *this;
   }

   ModNum& operator = (const int& i){
      _num = i % _modulus;
      if(_num < 0) _num += _modulus;
      return *this;
   }

   // static methods
   static void setModulus(int m) { _modulus = m; }
   static int getModulus() { return _modulus; }
   //
   // [CHANGED] Set the variable 's' in the _varMap to value 'n',
   // no matter the variable 's' exists in _varMap or not
   static void setVarVal(const string& s, const ModNum& n);
   //
   // [CHANGED] Get the value of variable 's'.
   // If 's' can be found, store the value in 'n' and return true.
   // Otherwise ('s' not found), return false.
   static bool getVarVal(const string& s, ModNum& n);
   //
   // [CHANGED] If 's' is a valid variable name, return "getVarVal(s, n)";
   // else if 's' is a number, convert it to ModNum (e.g. by "myStr2Int") and
   // assign to 'n'
   static bool getStrVal(const string& s, ModNum& n);
   //
   // [CHANGED] Print out all the variables in _varMap, one variable per line,
   // in the following format ---
   // a = 9
   // b = 10
   // kkk = 18
   static void printVars();
   static void resetVapMap();

   // friend functions
   friend ostream& operator << (ostream&, const ModNum&);  // CHANGED

private:
   // Data members
   // DO NOT add/delete/modify data members
   int                _num;

   static int         _modulus;
   static CalcMap     _varMap;
};

#endif // CALC_MOD_NUM_H

