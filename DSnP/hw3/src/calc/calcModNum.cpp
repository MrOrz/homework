/****************************************************************************
  FileName     [ calcModNum.cpp ]
  PackageName  [ calc ]
  Synopsis     [ Define member functions for class ModNum ]
  Author       [ Chung-Yang (Ric) Huang ]
  Copyright    [ Copyleft(c) 2007-2010 LaDs(III), GIEE, NTU, Taiwan ]
****************************************************************************/
#include <iostream>
#include "util.h"
#include "calcModNum.h"

// CHANGED: Initialize the static data members of class ModNum
//       (Note: let default _modulus = 100000000)

int ModNum::_modulus = 100000000;
CalcMap ModNum::_varMap;

// CHANGED: Define the member functions of class ModNum

void ModNum::setVarVal(const string& s, const ModNum& n) {
   _varMap[s] = n;
}

bool ModNum::getVarVal(const string& s, ModNum& n) {
   if(_varMap.count(s) > 0 ){
      n = _varMap[s];
      return true;
   }
   else
      return false;
}

bool ModNum::getStrVal(const string& s, ModNum& n) {
   if(isValidVarName(s) ) return getVarVal(s,n);
   int num;
   if(!myStr2Int(s, num) ) return false;
   n = ModNum(num);
   return true;
}

void ModNum::printVars(){
   for( CalcMap::const_iterator it = _varMap.begin();
        it != _varMap.end(); ++it ){
      cout << it->first << " = " << it->second << endl;
   }
}

void ModNum::resetVapMap(){
   _varMap.clear();
}

ostream& operator << (ostream& os, const ModNum& i){
   return os << i._num;
}

