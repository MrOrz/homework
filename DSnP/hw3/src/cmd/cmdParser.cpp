/****************************************************************************
  FileName     [ cmdParser.cpp ]
  PackageName  [ cmd ]
  Synopsis     [ Define command parsing member functions for class CmdParser ]
  Author       [ Chung-Yang (Ric) Huang ]
  Copyright    [ Copyleft(c) 2007-2010 LaDs(III), GIEE, NTU, Taiwan ]
****************************************************************************/
#include <cassert>
#include <iostream>
#include <iomanip>
#include <cstdlib>
#include "util.h"
#include "cmdParser.h"

using namespace std;


//----------------------------------------------------------------------
//    Global cmd Manager
//----------------------------------------------------------------------
CmdParser* cmdMgr = new CmdParser("mcalc> ");


//----------------------------------------------------------------------
//    Member Function for class cmdParser
//----------------------------------------------------------------------
// return false if file cannot be opened
bool
CmdParser::openDofile(const string& dof)
{
   // CHANGED...
   ifstream* f = new ifstream(dof.c_str() );
   if(f->good()){ // dof is valid
      if(_dofile){ // a file is already opened
         if(_dofileStack.size() >= MAX_DOFILE_DEPTH ){ // max depth reached
            f->close(); delete f;
            return false;
         }

         _dofileStack.push(_dofile);
      }
      _dofile = f;
   }
   return f->good();
}

// Must make sure _dofile != 0
void
CmdParser::closeDofile()
{
   assert(_dofile != 0);

   // CHANGED...

   _dofile->close();
   delete _dofile;
   _dofile = NULL;

   if( _dofileStack.size() > 0 ){
      _dofile = _dofileStack.top();
      _dofileStack.pop();
   }
}

// Return false if registration fails
bool
CmdParser::regCmd(const string& cmd, unsigned nCmp, CmdExec* e)
{
   // Make sure cmd hasn't been registered and won't cause ambiguity
   string str = cmd;
   unsigned s = str.size();
   if (s < nCmp) return false;
   while (true) {
      if (getCmd(str)) return false;
      if (s == nCmp) break;
      str.resize(--s);
   }

   // Change the first nCmp characters to upper case
   // The strings stored in _cmdMap are all upper case
   //
   assert(str.size() == nCmp);  // str is now mandCmd
   string& mandCmd = str;
   for (unsigned i = 0; i < nCmp; ++i)
      mandCmd[i] = toupper(mandCmd[i]);
   string optCmd = cmd.substr(nCmp);
   assert(e != 0);
   e->setOptCmd(optCmd);

   // CHANGED: insert (mandCmd, e) to _cmdMap; return false if insertion fails.
   return _cmdMap.insert( CmdRegPair(mandCmd, e) ).second;
}

// Return false on "quit" or if exception happens
CmdExecStatus
CmdParser::execOneCmd()
{
   bool newCmd = false;
   if (_dofile != 0)
      newCmd = readCmd(*_dofile);
   else
      newCmd = readCmd(cin);

   // execute the command
   if (newCmd) {
      string option;
      CmdExec* e = parseCmd(option);
      if (e != 0)
         return e->exec(option);
   }
   return CMD_EXEC_NOP;
}

// For each CmdExec* in _cmdMap, call its "help()" to print out the help msg.
// Print an endl at the end.
void
CmdParser::printHelps() const
{
   // CHANGED...
   for(CmdMap::const_iterator it = _cmdMap.begin();
       it != _cmdMap.end(); ++it)
      it->second->help();
   cout << endl;
}

void
CmdParser::printHistory(int nPrint) const
{
   if (_historySize == 0) {
      cout << "Empty command history!!" << endl;
      return;
   }
   if ((nPrint < 0) || (nPrint > int(_historySize)))
      nPrint = _historySize;
   for (int i = _historySize - nPrint; i < _historySize; ++i)
      cout << "   " << i << ": " << _history[i] << endl;
}


//
// Parse the command from _history[_historySize - 1]
//
// 1. Read the command string from the leading part of str and retrive the
//    corresponding CmdExec* from _cmdMap
//    ==> If command not found, print to cerr the following message:
//        Illegal command!! "(string cmdName)"
//    ==> return it at the end.
// 2. Get the command options from the trailing part of str and store them
//    in "option"
//
CmdExec*
CmdParser::parseCmd(string& option)
{
   assert(_historySize > 0);
   string str = _history[_historySize - 1];

   string cmd;
   size_t pos = myStrGetTok(str, cmd); // get "command part" out of str
   if(pos != string::npos){   // there are options in str
      option = str.substr(pos);  // set option
   }

   CmdExec* e = getCmd(cmd);  // convert cmd to CmdExec instance
   if(e==NULL)
      cerr << "Illegal command!! (" << cmd <<  ")\n";

   // CHANGED...
   return e;
}

// cmd is a copy of the original input
//
// return the corresponding CmdExec* if "cmd" matches any command in _cmdMap
// return 0 if not found.
//
// Please note:
// ------------
// 1. The mandatory part of the command string (stored in _cmdMap) must match
// 2. The optional part can be partially omitted.
//    ==> Checked by the CmdExec::checkOptCmd(const string&) function
// 3. All string comparison are "case-insensitive".
//
CmdExec*
CmdParser::getCmd(string cmd)
{
   // CHANGED...
   for(CmdMap::const_iterator it = _cmdMap.begin();
       it != _cmdMap.end(); ++it ){
      const string& m  = it->first;
      CmdExec& c = *(it->second);
      if( myStrNCmp( m, cmd.substr(0, m.size() ), m.size() ) == 0 && // mandatory part matched
          c.checkOptCmd( cmd.substr( m.size() ) ) ){ // optional part matched
         return &c;
      }
   }
   return NULL;
}


//----------------------------------------------------------------------
//    Member Function for class CmdExec
//----------------------------------------------------------------------
// Return false if error options found
// "optional" = true if the option is optional XD
// "optional": default = true
//
bool
CmdExec::lexSingleOption
(const string& option, string& token, bool optional) const
{
   size_t n = myStrGetTok(option, token);
   if (!optional) {
      if (token.size() == 0) {
         errorOption(CMD_OPT_MISSING, "");
         return false;
      }
   }
   if (n != string::npos) {
      errorOption(CMD_OPT_EXTRA, option.substr(n));
      return false;
   }
   return true;
}

// if nOpts is specified (!= 0), the number of tokens must be exactly = nOpts
// Otherwise, return false.
//
bool
CmdExec::lexOptions
(const string& option, vector<string>& tokens, size_t nOpts) const
{
   string token;
   size_t n = myStrGetTok(option, token);
   while (token.size()) {
      tokens.push_back(token);
      n = myStrGetTok(option, token, n);
   }
   if (nOpts != 0) {
      if (tokens.size() < nOpts) {
         errorOption(CMD_OPT_MISSING, "");
         return false;
      }
      if (tokens.size() > nOpts) {
         errorOption(CMD_OPT_EXTRA, tokens[nOpts]);
         return false;
      }
   }
   return true;
}

CmdExecStatus
CmdExec::errorOption(CmdOptionError err, const string& opt) const
{
   switch (err) {
      case CMD_OPT_MISSING:
         cerr << "Missing option";
         if (opt.size()) cerr << " after (" << opt << ")";
         cerr << "!!" << endl;
      break;
      case CMD_OPT_EXTRA:
         cerr << "Extra option!! (" << opt << ")" << endl;
      break;
      case CMD_OPT_ILLEGAL:
         cerr << "Illegal option!! (" << opt << ")" << endl;
      break;
      case CMD_OPT_FOPEN_FAIL:
         cerr << "Error: cannot open file \"" << opt << "\"!!" << endl;
      break;
      default:
         cerr << "Unknown option error type!! (" << err << ")" << endl;
      exit(-1);
   }
   return CMD_EXEC_ERROR;
}

// Check if "check" is a matched substring of "_optCmd"...
// if not, return false.
//
// Perform case-insensitive checks
//
bool
CmdExec::checkOptCmd(const string& check) const
{
   // CHANGED...
   // typed command = check; valid command = _optCmd.

   if(check.size() > _optCmd.size() ) // typed command longer than valid command
      return 0;                       // illegal
   else if(check.size() > 0 )         // typed command shorter than valid command
      return (myStrNCmp(_optCmd, check, check.size() ) == 0 );
      // check if the typed command is a substring of valid command
   else  // check.size() == 0
      return true;
}

