#include <cassert>
#include <cstring>
#include "cmdParser.h"
#include <iomanip>

using namespace std;

//----------------------------------------------------------------------
//    Extrenal funcitons
//----------------------------------------------------------------------
void mybeep();
char mygetc(istream&);
ParseChar checkChar(char, istream&);

//----------------------------------------------------------------------
//    Static Function in this file
//----------------------------------------------------------------------

inline static void backspace(const unsigned& count){
  if(count > 0)
    cout << setfill(char(BACK_SPACE_CHAR)) << setw(count) << char(BACK_SPACE_CHAR) << flush;
}
inline static void fill(char* &start, char* &end, const char& c){
  memset(start, c, unsigned(end - (start - 1) ) );
}

//----------------------------------------------------------------------
//    Member Function for class Parser
//----------------------------------------------------------------------
void
CmdParser::readCmd()
{
   if (_dofile.is_open()) {
      readCmdInt(_dofile);
      _dofile.close();
   }
   else
      readCmdInt(cin);
}

void
CmdParser::readCmdInt(istream& istr)
{
   _readBufPtr = _readBufEnd = _readBuf;
   *_readBufPtr = 0;

   printPrompt();

   while (1) {
      char ch = mygetc(istr);
      ParseChar pch = checkChar(ch, istr);
      if (pch == INPUT_END_KEY) break;
      switch (pch) {
         case LINE_BEGIN_KEY :
         case HOME_KEY       : moveBufPtr(_readBuf); break;
         case LINE_END_KEY   :
         case END_KEY        : moveBufPtr(_readBufEnd); break;
         case BACK_SPACE_KEY : moveBufPtr(_readBufPtr - 1);
                               deleteChar(); break; 
         case DELETE_KEY     : deleteChar(); break;
         case NEWLINE_KEY    : addHistory();
                               cout << char(NEWLINE_KEY);
                               printPrompt(); break;
         case ARROW_UP_KEY   : moveToHistory(_historyIdx - 1); break;
         case ARROW_DOWN_KEY : moveToHistory(_historyIdx + 1); break;
         case ARROW_RIGHT_KEY: moveBufPtr(_readBufPtr + 1); break;
         case ARROW_LEFT_KEY : moveBufPtr(_readBufPtr - 1); break;
         case PG_UP_KEY      : moveToHistory(_historyIdx - PG_OFFSET); break;
         case PG_DOWN_KEY    : moveToHistory(_historyIdx + PG_OFFSET); break;
         case TAB_KEY        : // not yet supported; fall through to UNDEFINE
         case INSERT_KEY     : // not yet supported; fall through to UNDEFINE
         case UNDEFINED_KEY:   mybeep(); break;
         default:  // printable character
            insertChar(char(pch)); break;
      }
   }
}


// This function moves _readBufPtr to the "ptr" pointer
// It is used by left/right arrowkeys, home/end, etc.
//
// 1. Make sure ptr is within [_readBuf, _readBufEnd].
//    If not, make a beep sound and return false. (DON'T MOVE)
// 2. Move the cursor to the left or right, depending on ptr
// 3. Update _readBufPtr accordingly. The content of the _readBuf[] will
//    not be changed
//
bool
CmdParser::moveBufPtr(char* const ptr)
{
   // out of range exception
   if(!(ptr >= _readBuf && ptr <= _readBufEnd)){
     mybeep();
     return false;
   }
   
   unsigned total = (_readBufEnd - _readBuf + 1) / sizeof(char),
            original_pos = (_readBufPtr - _readBuf) / sizeof(char) ,
            new_pos = (ptr - _readBuf + 1) / sizeof(char) ;
   // backspace until the line head, print the buf and backspace again
   backspace(original_pos);
   cout << _readBuf ;
   backspace(total - new_pos);

   // update _readBufPtr to new position   
   _readBufPtr = ptr;
   // TODO...done.
   return true;
}


// 1. Delete the char at _readBufPtr
// 2. mybeep() and return false if at _readBufEnd
// 3. Move the remaining string left for one character
// 4. The cursor should stay at the same position
// 5. Remember to update _readBufEnd accordingly.
//
// For example,
//
// cmd> This is the command
//              ^                (^ is the cursor position)
//
// After calling deleteChar()---
//
// cmd> This is he command
//              ^
//
bool
CmdParser::deleteChar()
{
   if(_readBufPtr == _readBufEnd){
     mybeep();
     return false;
   }
   
   // move the charactor left
   memmove(_readBufPtr, _readBufPtr+1, unsigned(_readBufEnd - _readBufPtr) / sizeof(char));
   // covers the last character with space
   *(_readBufEnd-1) = ' ';
  
   // refresh command. the tailing space will cover the last character
   moveBufPtr(_readBufPtr);
   
   // officially remove last character
   *(_readBufEnd-1) = '\0';
   --_readBufEnd;
   // TODO...done.
   return true;
}

// 1. Insert character 'ch' at _readBufPtr
// 2. Move the remaining string right for one character
// 3. The cursor should move right for one position afterwards
//
// For example,
//
// cmd> This is the command
//              ^                (^ is the cursor position)
//
// After calling insertChar('k') ---
//
// cmd> This is kthe command
//               ^
//
void
CmdParser::insertChar(char ch)
{
  // move chars after ch.
  memmove(_readBufPtr+1, _readBufPtr, unsigned(_readBufEnd - (_readBufPtr - 1)));
  *_readBufPtr = ch; 
  ++_readBufEnd;
  
  // change pointer and print command
  moveBufPtr(_readBufPtr+1); 
   // TODO...done.
}

// 1. Delete the line that is currently shown on the screen
// 2. Reset _readBufPtr and _readBufEnd to _readBuf
// 3. Make sure *_readBufEnd = 0
//
// For example,
//
// cmd> This is the command
//              ^                (^ is the cursor position)
//
// After calling deleteLine() ---
//
// cmd>
//      ^
//
void
CmdParser::deleteLine()
{
  // fill buffer with whitespaces, then change ptr pos (also prints command)
  fill(_readBuf, _readBufEnd, ' ');
  moveBufPtr(_readBuf);
  
  // clear buffer, reset pointers
  fill(_readBuf, _readBufEnd, '\0');
  _readBufPtr = _readBufEnd = _readBuf;
   // TODO...done.
}


// This functions moves _historyIdx to index and display _history[index]
// on the screen. The _historySize should NOT be changed.
//
// Need to consider:
// If moving up... (i.e. index < _historyIdx)
// 1. If already at top (i.e. _historyIdx == 0), beep and do nothing.
// 2. If at bottom, temporarily record _readBuf to history.
//    (Do not remove spaces, and do not change _historySize.)
// 3. If index < 0, let index = 0.
//
// If moving down... (i.e. index > _historyIdx)
// 1. If already at bottom, beep and do nothing
// 2. If index >= _historySize, let index = _historySize.
//
// [Note] index should not = _historyIdx
//
void
CmdParser::moveToHistory(int index)
{
  if(index < _historyIdx){ // moving up
    if(_historyIdx == 0){  // do nothing when at top
      mybeep(); return;
    }
    if(_historyIdx == _historySize) // just start browsing history:
      _history[_historySize] = _readBuf; // temp record buf to history
    if(index < 0) index = 0;
  }else{                   // moving down
    if(_historyIdx == _historySize){ // no newer history available
      mybeep(); return;
    }
    if(index >= _historySize) index = _historySize; // recover current command
  }
  
  _historyIdx = index;  // set _historyIdx and retrieve the history.
  retrieveHistory();
   // TODO...done.
}


// This function adds the string in _readBuf to the _history[_historySize]
//
// 1. Remove ' ' at the beginning and end of _readBuf
// 2. If not a null string,
//       add to _history[_historySize] and set _historyIdx to ++_historySize
// 3. Reset _readBufPtr and _readBufEnd to _readBuf
// 4. Make sure *_readBufEnd = 0 ==> _readBuf becomes null string
//
void
CmdParser::addHistory()
{
  char *s, *e;
  
  // if buffer is empty, return directly.
  if(_readBuf == _readBufEnd) return; 
  
  // eats up the whitespaces
  for(s = _readBuf;      *s == ' ' && s < _readBufEnd-1 ; ++s);
  for(e = _readBufEnd-1; *e == ' ' && e > s             ; --e);
  if(s == e && *e==' '){ // if only spaces are in the buffer
    deleteLine(); // clear the buffer
    return;
  } 
  
  *(e+1) = '\0';                // terminates the command at the end
  _history[_historySize++] = s; // only records the command without head whitespaces
  _historyIdx = _historySize;   // reset _historyIdx to the newest
  
  fill(_readBuf, _readBufEnd, '\0'); // clear out buffer
  _readBufEnd = _readBufPtr = _readBuf; // reset buffer position
   // TODO...done.
}


// 1. Replace current line with _history[_historyIdx] on the screen
// 2. Set _readBufPtr and _readBufEnd to end of line
//
// [Note] Do not change _historySize.
//
void
CmdParser::retrieveHistory()
{
   deleteLine();
   strcpy(_readBuf, _history[_historyIdx].c_str());
   cout << _readBuf;
   _readBufPtr = _readBufEnd = _readBuf + _history[_historyIdx].size();
}
