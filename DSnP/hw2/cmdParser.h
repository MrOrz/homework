#ifndef CMD_PARSER_H
#define CMD_PARSER_H

#include <iostream>
#include <fstream>
#include <string>

#include "charDef.h"

using namespace std;

//----------------------------------------------------------------------
//    Forward Declaration
//----------------------------------------------------------------------

//----------------------------------------------------------------------
//    Base class : CmdParser
//----------------------------------------------------------------------

#define READ_BUF_SIZE    65536
#define MAX_HISTORY      65536

class CmdParser
{
#define PG_OFFSET        10

public:
   CmdParser() : _readBufPtr(_readBuf), _readBufEnd(_readBuf),
                 _historyIdx(0), _historySize(0) {}
   virtual ~CmdParser() {}

   bool openDofile(const char* dof) {
        _dofile.open(dof); return _dofile.is_open(); }

   void readCmd();

private:
   // Private member functions
   void readCmdInt(istream&);
   void printPrompt() const { cout << "cmd> "; }
   bool moveBufPtr(char* const);
   bool deleteChar();
   void insertChar(char);
   void deleteLine();
   void moveToHistory(int index);
   void addHistory();
   void addHistoryStr(const char* str) { _history[_historySize] = str; }
   void retrieveHistory();

   // Data members
   ifstream  _dofile;
   char      _readBuf[READ_BUF_SIZE];// save the current line input
                                     // be consistent as shown on the screen
   char*     _readBufPtr;            // point to the cursor position
                                     // also be the insert and delete point
   char*     _readBufEnd;            // end of string position of _readBuf
                                     // make sure *_readBufEnd = 0
   string    _history[MAX_HISTORY];  // oldest:_history[0],latest:_history[n]
   int       _historyIdx;            // position to display history string
   int       _historySize;           // number of valid _history entries
                                     // new history is always inserted at
                                     //    _history[_historySize];
};


#endif // CMD_PARSER_H
