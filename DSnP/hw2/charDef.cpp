#include <iostream>
#include <iomanip>
#include <termio.h>
#include <stdlib.h>
#include <ctype.h>
#include <cassert>
#include "charDef.h"

using namespace std;

//----------------------------------------------------------------------
//    Global static funcitons
//----------------------------------------------------------------------
static struct termios stored_settings;

static void reset_keypress(void)
{
   tcsetattr(0,TCSANOW,&stored_settings);
}

static void set_keypress(void)
{
   struct termios new_settings;
   tcgetattr(0,&stored_settings);
   new_settings = stored_settings;
   new_settings.c_lflag &= (~ICANON);
   new_settings.c_lflag &= (~ECHO);
   new_settings.c_cc[VTIME] = 0;
   tcgetattr(0,&stored_settings);
   new_settings.c_cc[VMIN] = 1;
   tcsetattr(0,TCSANOW,&new_settings);
}

//----------------------------------------------------------------------
//    Global funcitons
//----------------------------------------------------------------------
char mygetc(istream& istr)
{
   char ch;
   set_keypress();
   istr.unsetf(ios_base::skipws);
   istr >> ch;
   istr.setf(ios_base::skipws);
   reset_keypress();
   #ifdef TEST_ASC
   cout << left << setw(6) << int(ch);
   #endif // TEST_ASC
   return ch;
}

void mybeep()
{
   cout << char(BEEP_CHAR);
}

#ifndef TA_KB_SETTING
// YOU NEED TO CUSTOMIZE THIS PART...
// YOU NEED TO CUSTOMIZE THIS PART...
//
// Modify "charDef.h" to work with the following code
//
// Make sure you DO NOT define TA_KB_SETTING in your Makefile
//
ParseChar
checkChar(char ch, istream& istr)
{
   if(istr.eof()) return INPUT_END_KEY;
   switch (ch) {
      // Simple keys: one code for one key press
      // -- The following should be platform-independent
      case LINE_BEGIN_KEY:  // Ctrl-a
      case LINE_END_KEY:    // Ctrl-e
      case INPUT_END_KEY:   // Ctrl-d
      case TAB_KEY:         // tab('\t') or Ctrl-i
      case NEWLINE_KEY:     // enter('\n') or ctrl-m
         return ParseChar(ch);
      case BACK_SPACE_KEY:
         return ParseChar(ch);
      // TODO...done.
      // -- The following simple/combo keys are platform-dependent
      //    You should test to check the returned codes of these key presses
      // -- You should either modify the "enum ParseChar" definitions in
      //    "charDef.h", or revise the control flow of the "case ESC" below
      //
      // -- You need to handle:
      //    { BACK_SPACE_KEY, ARROW_UP/DOWN/RIGHT/LEFT,
      //      HOME/END/PG_UP/PG_DOWN/INSERT/DELETE }
      //
      // Combo keys: multiple codes for one key press
      // -- Usually starts with ESC key, so we check the "case ESC"
      case ESC_KEY: {
         char combo = mygetc(istr);
         // Note: ARROW_KEY_INT == MOD_KEY_INT, so we only check MOD_KEY_INT
         if (combo == char(MOD_KEY_INT)) {
            char key = mygetc(istr);
            if ((key >= char(MOD_KEY_BEGIN)) && (key <= char(MOD_KEY_END))) {
               if (mygetc(istr) == MOD_KEY_DUMMY)
                  return ParseChar(int(key) + MOD_KEY_FLAG);
               else return UNDEFINED_KEY;
            }
            else if ((key >= char(ARROW_KEY_BEGIN)) &&
                     (key <= char(ARROW_KEY_END)))
               return ParseChar(int(key) + ARROW_KEY_FLAG);
            else return UNDEFINED_KEY;
         }
         else if(combo == char(HE_KEY_INT)){ // HOME & END
            char key = mygetc(istr);
            switch(key){
              case char(HOME_KEY) : return HOME_KEY;
              case char(END_KEY)  : return END_KEY;
              default: return UNDEFINED_KEY;
            }
         }
         else return UNDEFINED_KEY;
      }

      // For the remaining printable and undefined keys
      default:
         if (isprint(ch)) return ParseChar(ch);
         else return UNDEFINED_KEY;
   }

   return UNDEFINED_KEY;
}
#else // TA_KB_SETTING is defined
// DO NOT CHANGE THIS PART...
// DO NOT CHANGE THIS PART...
//
// This part will be used by TA to test your program.
//
// TA will use "make -DTA_KB_SETTING" to test your program
//
ParseChar
checkChar(char ch, istream& istr)
{
   if(istr.eof()) return INPUT_END_KEY;
   switch (ch) {
      // Simple keys: one code for one key press
      // -- The following should be platform-independent
      case LINE_BEGIN_KEY:  // Ctrl-a
      case LINE_END_KEY:    // Ctrl-e
      case INPUT_END_KEY:   // Ctrl-d
      case TAB_KEY:         // tab('\t') or Ctrl-i
      case NEWLINE_KEY:     // enter('\n') or ctrl-m
         return ParseChar(ch);

      // -- The following simple/combo keys are platform-dependent
      //    You should test to check the returned codes of these key presses
      // -- You should either modify the "enum ParseChar" definitions in
      //    "charDef.h", or revise the control flow of the "case ESC" below
      case BACK_SPACE_KEY:
         return ParseChar(ch);

      // Combo keys: multiple codes for one key press
      // -- Usually starts with ESC key, so we check the "case ESC"
      case ESC_KEY: {
         char combo = mygetc(istr);
         // Note: ARROW_KEY_INT == MOD_KEY_INT, so we only check MOD_KEY_INT
         if (combo == char(MOD_KEY_INT)) {
            char key = mygetc(istr);
            if ((key >= char(MOD_KEY_BEGIN)) && (key <= char(MOD_KEY_END))) {
               if (mygetc(istr) == MOD_KEY_DUMMY)
                  return ParseChar(int(key) + MOD_KEY_FLAG);
               else return UNDEFINED_KEY;
            }
            else if ((key >= char(ARROW_KEY_BEGIN)) &&
                     (key <= char(ARROW_KEY_END)))
               return ParseChar(int(key) + ARROW_KEY_FLAG);
            else return UNDEFINED_KEY;
         }
         else return UNDEFINED_KEY;
      }
      // For the remaining printable and undefined keys
      default:
         if (isprint(ch)) return ParseChar(ch);
         else return UNDEFINED_KEY;
   }

   return UNDEFINED_KEY;
}
#endif // TA_KB_SETTING

