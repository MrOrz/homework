#include <string.h>
#include <cstdlib>
#include "cmdParser.h"

using namespace std;

static void
usage()
{
   cout << "Usage: cmdReader [ -file < doFile > ]" << endl;
}

static void
myexit()
{
   usage();
   exit(-1);
}

int
main(int argc, char** argv)
{
   CmdParser cmd;
   ifstream dof;

   if (argc == 3) {  // -file <doFile>
      if (strcmp(argv[1], "-file") == 0) {
         if (!cmd.openDofile(argv[2])) {
            cerr << "Error: cannot open file \"" << argv[2] << "\"!!\n";
            myexit();
         }
      }
      else {
         cerr << "Error: unknown argument \"" << argv[1] << "\"!!\n";
         myexit();
      }
   }
   else if (argc != 1) {
      cerr << "Error: illegal number of argument (" << argc << ")!!\n";
      myexit();
   }

   while (1) { // press "Ctrl-c" to end
      cmd.readCmd();
      cout << endl;  // a blank line between each command
   }

   return 0;
}
