pms = zeros(1, 100);
ks = [1:100];

for k = ks
    [~,pms(k),~,~] = margin(tf([k], [1/5/50 1/5+1/50 1 0]) );
end

plot(ks, pms);
ylabel('Phase Margin');
xlabel('K');
print('prob2_k_pm.eps','-depsc2');