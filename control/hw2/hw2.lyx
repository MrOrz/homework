#LyX 1.6.7 created this file. For more info see http://www.lyx.org/
\lyxformat 345
\begin_document
\begin_header
\textclass article
\begin_preamble
\usepackage{fontspec}
\usepackage{titlesec}
\usepackage{indentfirst}
\parindent=24pt
\usepackage{xunicode}
\usepackage{cancel}

\setmainfont{cwTeX 明體}
\setmonofont{Consolas}
%\setsansfont{Times New Roman}
\setmathrm{cwTeX 明體}

\XeTeXlinebreaklocale "zh"
\XeTeXlinebreakskip = 0pt plus 1pt
\end_preamble
\use_default_options true
\language chinese-traditional
\inputencoding utf8-plain
\font_roman default
\font_sans default
\font_typewriter default
\font_default_family default
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100

\graphics default
\paperfontsize default
\spacing other 1.2
\use_hyperref false
\pdf_bookmarks false
\pdf_bookmarksnumbered false
\pdf_bookmarksopen false
\pdf_bookmarksopenlevel 1
\pdf_breaklinks false
\pdf_pdfborder false
\pdf_colorlinks false
\pdf_backref false
\pdf_pdfusetitle true
\papersize a4paper
\use_geometry true
\use_amsmath 1
\use_esint 1
\cite_engine basic
\use_bibtopic false
\paperorientation portrait
\leftmargin 1.7cm
\topmargin 1.7cm
\rightmargin 1.7cm
\bottommargin 1.7cm
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\defskip medskip
\quotes_language english
\papercolumns 1
\papersides 2
\paperpagestyle default
\tracking_changes false
\output_changes false
\author "" 
\author "" 
\end_header

\begin_body

\begin_layout Title

\size large
自動控制實驗
\size default

\begin_inset Newline newline
\end_inset


\begin_inset VSpace smallskip
\end_inset


\size huge
Homework #2
\end_layout

\begin_layout Author
梁翔勝 B97901125
\end_layout

\begin_layout Section*
(1) 
\end_layout

\begin_layout Subsection*
(a)
\end_layout

\begin_layout Standard
Loop gain 
\begin_inset Formula $(20+0.2s)\left(\frac{360}{s^{2}}\right)0.03183=\frac{229.18+2.29s}{s^{2}}$
\end_inset

。由下面 Matlab 指令：
\end_layout

\begin_layout Standard
\begin_inset listings
lstparams "basicstyle={\footnotesize\ttfamily}"
inline false
status open

\begin_layout Plain Layout

>> sys = tf([2.29 229.18], [1 0 0])
\end_layout

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout

Transfer function:
\end_layout

\begin_layout Plain Layout

2.29 s + 229.2
\end_layout

\begin_layout Plain Layout

--------------
\end_layout

\begin_layout Plain Layout

     s^2
\end_layout

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout

>> [Gm,Pm,Wg,Wp] = margin(sys) 
\end_layout

\begin_layout Plain Layout

Gm =
\end_layout

\begin_layout Plain Layout

     0
\end_layout

\begin_layout Plain Layout

Pm =
\end_layout

\begin_layout Plain Layout

    8.6507
\end_layout

\begin_layout Plain Layout

Wg =
\end_layout

\begin_layout Plain Layout

     0
\end_layout

\begin_layout Plain Layout

Wp =
\end_layout

\begin_layout Plain Layout

   15.2260
\end_layout

\end_inset


\end_layout

\begin_layout Standard
可得 Gain margin = 0, Phase margin = 8.65 deg, 交越頻率 
\begin_inset Formula $15.226$
\end_inset

 rad/sec。margin 指令不指定輸出時可得下面 Bode plot。
\end_layout

\begin_layout Standard
\begin_inset Graphics
	filename 1a.eps
	width 50col%
	groupId fig

\end_inset


\end_layout

\begin_layout Standard
系統的整體 transfer function 則為 
\begin_inset Formula $\frac{(20+0.2s)\left(\frac{360}{s^{2}}\right)}{1+0.03183(20+0.2s)\left(\frac{360}{s^{2}}\right)}$
\end_inset

，輸入 matlab 得：
\end_layout

\begin_layout Standard
\begin_inset listings
lstparams "basicstyle={\small\ttfamily}"
inline false
status open

\begin_layout Plain Layout

sys = tf([0.2 20], [1 0 0]) * 360 / (1 + 0.03183 * tf([0.2 20], 1) * tf(360,
 [1 0 0]))   
\end_layout

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout

Transfer function:      
\end_layout

\begin_layout Plain Layout

     72 s^3 + 7200 s^2 
\end_layout

\begin_layout Plain Layout

--------------------------- 
\end_layout

\begin_layout Plain Layout

s^4 + 2.292 s^3 + 229.2 s^2
\end_layout

\end_inset


\end_layout

\begin_layout Standard
step(sys) 可以得到下面的步級響應：
\end_layout

\begin_layout Standard
\begin_inset Graphics
	filename 1a-step.eps
	width 50col%
	groupId fig

\end_inset


\end_layout

\begin_layout Standard
而 lsim(sys, [1:30000],[1:30000]); 可得下面 ramp 響應：
\end_layout

\begin_layout Standard
\begin_inset Graphics
	filename 1a-ramp.eps
	width 50col%
	groupId fig

\end_inset


\end_layout

\begin_layout Standard
由上圖，輸出沒有進入穩態的跡象，故 
\begin_inset Formula $e_{ss}$
\end_inset

 不存在；而對於 step response 的 
\begin_inset Formula $e_{ss}$
\end_inset

，可由下面指令取得：
\end_layout

\begin_layout Standard
\begin_inset listings
lstparams "basicstyle={\small\ttfamily}"
inline false
status open

\begin_layout Plain Layout

>> [y, t] = lsim(sys, ones(1,10001), [0:0.01:100]);
\end_layout

\begin_layout Plain Layout

>> y(end) - 1
\end_layout

\begin_layout Plain Layout

ans =
\end_layout

\begin_layout Plain Layout

   30.4169
\end_layout

\end_inset


\end_layout

\begin_layout Subsection*
(b)
\end_layout

\begin_layout Standard
將 tf 產生的 sys 用 zpk 函式轉換後，可用 zpkdata 取得極點資訊，如下。
\end_layout

\begin_layout Standard
\begin_inset listings
lstparams "basicstyle={\small\ttfamily}"
inline false
status open

\begin_layout Plain Layout

  >> [zero pole k] = zpkdata(zpk(sys))
\end_layout

\begin_layout Plain Layout

zero = 
\end_layout

\begin_layout Plain Layout

    [3x1 double]
\end_layout

\begin_layout Plain Layout

pole = 
\end_layout

\begin_layout Plain Layout

    [4x1 double]
\end_layout

\begin_layout Plain Layout

k =
\end_layout

\begin_layout Plain Layout

    72
\end_layout

\begin_layout Plain Layout

>> pole{1}
\end_layout

\begin_layout Plain Layout

ans =
\end_layout

\begin_layout Plain Layout

        0                   0             -1.1459 +15.0951i   -1.1459 -15.0951i
\end_layout

\end_inset


\end_layout

\begin_layout Standard
亦即 0 重根、
\begin_inset Formula $-1.1459\pm j15.09$
\end_inset

。極點均在左半平面，除 
\begin_inset Formula $\omega=0$
\end_inset

 （DC）之外皆穩定。
\end_layout

\begin_layout Subsection*
(c)
\end_layout

\begin_layout Standard
使用下面指令可以直接取得時域規格。
\end_layout

\begin_layout Standard
\begin_inset listings
lstparams "basicstyle={\small\ttfamily}"
inline false
status open

\begin_layout Plain Layout

>> [y, t] = lsim(sys, ones(1, 10001), [0:0.01:100]); 
\end_layout

\begin_layout Plain Layout

>> stepinfo(y, t)
\end_layout

\begin_layout Plain Layout

ans = 
\end_layout

\begin_layout Plain Layout

        RiseTime: 0.0702
\end_layout

\begin_layout Plain Layout

    SettlingTime: 3.3503
\end_layout

\begin_layout Plain Layout

     SettlingMin: 11.7244
\end_layout

\begin_layout Plain Layout

     SettlingMax: 56.4437
\end_layout

\begin_layout Plain Layout

       Overshoot: 79.6603
\end_layout

\begin_layout Plain Layout

      Undershoot: 0
\end_layout

\begin_layout Plain Layout

            Peak: 56.4437
\end_layout

\begin_layout Plain Layout

        PeakTime: 0.2000
\end_layout

\end_inset


\end_layout

\begin_layout Section*
(2) 
\end_layout

\begin_layout Standard
未加控制器前：
\end_layout

\begin_layout Standard
\begin_inset listings
lstparams "basicstyle={\small\ttfamily}"
inline false
status open

\begin_layout Plain Layout

>> KG = tf(6, [1 1 0])   
\end_layout

\begin_layout Plain Layout

Transfer function:
\end_layout

\begin_layout Plain Layout

   6
\end_layout

\begin_layout Plain Layout

------- 
\end_layout

\begin_layout Plain Layout

s^2 + s   
\end_layout

\begin_layout Plain Layout

>> rlocus(KG)
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Graphics
	filename 2a-root.eps
	width 50col%
	groupId fig

\end_inset


\end_layout

\begin_layout Standard
\begin_inset listings
lstparams "basicstyle={\footnotesize\ttfamily}"
inline false
status open

\begin_layout Plain Layout

>> sys = KG / (1+KG)   
\end_layout

\begin_layout Plain Layout

Transfer function:
\end_layout

\begin_layout Plain Layout

       6 s^2 + 6 s
\end_layout

\begin_layout Plain Layout

------------------------- 
\end_layout

\begin_layout Plain Layout

s^4 + 2 s^3 + 7 s^2 + 6 s
\end_layout

\begin_layout Plain Layout

>> step(sys)
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Graphics
	filename 2a-step.eps
	width 50col%
	groupId fig

\end_inset


\end_layout

\begin_layout Standard
加上控制器後：
\end_layout

\begin_layout Standard
\begin_inset listings
lstparams "basicstyle={\footnotesize\ttfamily}"
inline false
status open

\begin_layout Plain Layout

>> GC = tf([1 2], [1 4])*tf([1 0.0133],[1,0.001])   
\end_layout

\begin_layout Plain Layout

Transfer function: 
\end_layout

\begin_layout Plain Layout

s^2 + 2.013 s + 0.0266 
\end_layout

\begin_layout Plain Layout

---------------------- 
\end_layout

\begin_layout Plain Layout

s^2 + 4.001 s + 0.004   
\end_layout

\begin_layout Plain Layout

>> rlocus(GC*KG)
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Graphics
	filename 2b-root.eps
	width 50col%
	groupId fig

\end_inset


\end_layout

\begin_layout Standard
\begin_inset listings
lstparams "basicstyle={\footnotesize\ttfamily}"
inline false
status open

\begin_layout Plain Layout

>> sys = GC*KG / (1+GC*KG)   
\end_layout

\begin_layout Plain Layout

Transfer function:   
\end_layout

\begin_layout Plain Layout

6 s^6 + 42.09 s^5 + 84.6 s^4 + 49.2 s^3                                   
                                                 + 0.6875 s^2 + 0.0006384
 s                                                   ---------------------------
---------------------- 
\end_layout

\begin_layout Plain Layout

s^8 + 10 s^7 + 39.02 s^6 + 82.15 s^5 + 100.7 s^4                          
                                     
\end_layout

\begin_layout Plain Layout

+ 49.23 s^3 + 0.6875 s^2 + 0.0006384 s 
\end_layout

\begin_layout Plain Layout

>> step(sys)
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Graphics
	filename 2b-step.eps
	width 50col%
	groupId fig

\end_inset


\end_layout

\begin_layout Section*
(3.)
\end_layout

\begin_layout Subsection*
(a)
\end_layout

\begin_layout Standard
使用下面指令將原系統 
\begin_inset Formula $G(s)$
\end_inset

 的 bode plot 繪出：
\end_layout

\begin_layout Standard
\begin_inset listings
lstparams "basicstyle={\small\ttfamily}"
inline false
status open

\begin_layout Plain Layout

>> G = tf(40, [1 5 4 0]); 
\end_layout

\begin_layout Plain Layout

>> bode(G)
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Graphics
	filename 3a.eps
	width 50col%
	groupId fig

\end_inset


\end_layout

\begin_layout Standard
此系統的 phase margin 為負，故在交越頻率附近，這個系統相當不穩定。用 zpkdata 來取得 
\begin_inset Formula $\frac{G(s)}{1+G(s)}$
\end_inset

 整個系統的 pole 為：0, -5.57, -4, -1, 
\begin_inset Formula $0.2854\pm2.6643$
\end_inset

，有兩個 pole 在 s-plane 的右半面；如此一來會在 time domain 造成不會 decay 的震盪。
\end_layout

\begin_layout Subsection*
(b)
\end_layout

\begin_layout Standard
加入 compensater 之後，Loop Gain 變為 
\begin_inset Formula $G_{c}(s)G(s)$
\end_inset

，使用 margin 指令列出其 bode plot 和 margin 相關資訊為
\end_layout

\begin_layout Standard
\begin_inset Graphics
	filename 3b.eps
	width 50col%
	groupId fig

\end_inset


\end_layout

\begin_layout Standard
其中 Gain margin 與 Phase margin 均符合題目所要求。
\end_layout

\begin_layout Subsection*
(c)
\end_layout

\begin_layout Standard
Bode 圖如上，而 root locus 如下。
\end_layout

\begin_layout Standard
\begin_inset Graphics
	filename 3c.eps
	width 50col%
	groupId fig

\end_inset


\end_layout

\begin_layout Section*
(4.)
\end_layout

\begin_layout Standard
\begin_inset listings
lstparams "basicstyle={\footnotesize\ttfamily}"
inline false
status open

\begin_layout Plain Layout

>> A = [-1 -1; 6.5 0]; B = [1 1; 1 0]; C = [1 0; 0 1]; D = 0;
\end_layout

\begin_layout Plain Layout

>> sys = ss(A,B,C,D); 
\end_layout

\begin_layout Plain Layout

>> step(sys);
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Graphics
	filename 4.eps
	width 50col%
	groupId fig

\end_inset


\end_layout

\end_body
\end_document
