function prob6
    global sample_interval;
    H_0     = tf([1], [1, 2*0, 1]); % zeta = 0
    H_half  = tf([1], [1, 2*0.5, 1]); % zeta = 0.5
    H_1     = tf([1], [1, 2*1, 1]); % zeta = 1
    H_5     = tf([1], [1, 2*5, 1]); % zeta = 5

    sample_interval = 0:0.01:50; % [0, 50] with interval = 0.01s
    
    myplot(0, H_0);
    myplot(0.5, H_half);
    myplot(1.0, H_1);
    myplot(5.0, H_5);
end

function myplot(zeta, H)
    global sample_interval;
    impulse(H, sample_interval);
    title(sprintf('\\zeta=%d', zeta));
    print(sprintf('prob6-zeta%d', zeta), '-depsc2');
end