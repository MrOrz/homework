function prob5
    Y0 = tf([3], [1 5 4 0]);
    Yprime0 = tf([3], [1 5 4]);
    Y1 = tf([3], [1 6 9 4]);
    Yprime1 = tf([3 0], [1 6 9 4]);
    
    subplot(211);
    impulse(Y0, 30); hold on;
    impulse(Yprime0, 30); hold off;
    title('\beta=0');
    legend('y', 'y''');
    
    subplot(212);
    impulse(Y1, 30); hold on;
    impulse(Yprime1, 30); hold off;
    title('\beta=1');
    legend('y', 'y''');
end