% this is a function to calculate instantaneous based on EMD method
% The code is prepared by Zhaohua Wu. For questions, please read the "Q&A" section or
% contact
%   zwu@fsu.edu

function omega = ifndq_mine(vimf, dt)
Nnormal=5;
rangetop=0.90;

vlength = max( size(vimf) );
vlength_1 = vlength -1;

abs_vimf = abs(vimf);

for jj=1:Nnormal,
    [spmax, ~, ~]=extrema(abs_vimf);
    dd=1:1:vlength;
    upper= spline(spmax(:,1),spmax(:,2),dd);
    abs_vimf = abs_vimf ./ upper;
end

nvimf = abs(abs_vimf);

dq = sqrt(ones(1, vlength) - nvimf.*nvimf );

devi = zeros(1, vlength_1);
for i=2:vlength_1,
    devi(i)=nvimf(i+1)-nvimf(i-1);
    if devi(i)>0 && nvimf(i)<1
        dq(i)=-dq(i);
    end
end

rangebot=-rangetop;
omgcos = zeros(1, vlength - 1);
for i=2:(vlength-1),
    if nvimf(i)>rangebot && nvimf(i) < rangetop
        omgcos(i)=abs(nvimf(i+1)-nvimf(i-1))*0.5/sqrt(1-nvimf(i)*nvimf(i));
    else
        omgcos(i)=-9999;
    end
end
omgcos(1)=-9999;
omgcos(vlength)=-9999;

jj=1;
ddd = zeros(1, vlength);
temp = zeros(1, vlength);
for i=1:vlength,
    if omgcos(i)>-1000
        ddd(jj)=i;
        temp(jj)=omgcos(i);
        jj=jj+1;
    end
end

omgcos=spline(ddd(1:jj-1),temp(1:jj-1),dd);

pi2=pi*2;
omega = omgcos/dt/pi2;
