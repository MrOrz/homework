6c6
< function omega = ifndq(vimf, dt)
---
> function omega = ifndq_mine(vimf, dt)
13,18c13
< for i=1:vlength,
<     abs_vimf(i)=vimf(i);
<     if abs_vimf(i) < 0
<         abs_vimf(i)=-vimf(i);
<     end
< end
---
> abs_vimf = abs(vimf);
21c16
<     [spmax, spmin, flag]=extrema(abs_vimf);
---
>     [spmax, ~, ~]=extrema(abs_vimf);
24,26c19
<     for i=1:vlength,
<         abs_vimf(i)=abs_vimf(i)/upper(i);
<     end
---
>     abs_vimf = abs_vimf ./ upper;
29,34c22
< for i=1:vlength,
<     nvimf(i)=abs_vimf(i);
<     if vimf(i) < 0;
<         nvimf(i)=-abs_vimf(i);
<     end
< end
---
> nvimf = abs(abs_vimf);
36,38c24
< for i=1:vlength,
<     dq(i)=sqrt(1-nvimf(i)*nvimf(i));
< end
---
> dq = sqrt(ones(1, vlength) - nvimf.*nvimf );
39a26
> devi = zeros(1, vlength_1);
47c34,35
< rangebot=-rangetop;     
---
> rangebot=-rangetop;
> omgcos = zeros(1, vlength - 1);
58a47,48
> ddd = zeros(1, vlength);
> temp = zeros(1, vlength);
66,67d55
< temp2=spline(ddd,temp,dd); 
< omgcos=temp2;
69,72c57
< 
< for i=1:vlength,
<     omega(i)=omgcos(i);
< end
---
> omgcos=spline(ddd(1:jj-1),temp(1:jj-1),dd);
75c60
< omega=omega/dt/pi2;
---
> omega = omgcos/dt/pi2;
