function rootLocus( num, den, kRange )
    Ks = linspace(kRange(1), kRange(2), 1000);
    numLength = max(size(num));
    denLength = max(size(den));
    vlength = max(numLength, denLength);
    
    % pad 'zero' before donimators and denominators
    D = zeros(1, vlength);
    D(end-denLength+1 : end) = den;
    N = zeros(1, vlength);
    N(end-numLength+1 : end) = num;
    
    % 1 + KN/D = 0 ==> D+KN = 0
    for k = Ks
        r = roots(D+k*N);
        plot(r, '.'); hold on;
    end
    hold off;
    
    title('Root Locus');
    xlabel('Real Axis');
    ylabel('Imaginary Axis');

%% reference
%{
    sys = tf(num, den);
    rlocus(sys);
%}
end
