function dy = odefun2(t,y)
dy = zeros(2,1);    % a column vector
dy(1) = y(2);  % y' = x
dy(2) = -5.*y(2)-4.*y(1)+3.*((sign(t)+1).*0.5); 
    % x' = -5x-4y+3exp(-beta t)u(t)
