close all;
[T,Y] = ode45(@odefun,[0 30],[0 0]);
subplot(2,1,1);
plot(T,Y(:,1),'b-'); hold on;  % y
plot(T,Y(:,2),'g-'); hold off; % y'
title('\beta = 1');
legend('y', 'y''');

subplot(2,1,2);
[T,Y] = ode45(@odefun2,[0 30],[0 0]); 
plot(T,Y(:,1),'b-'); hold on;  % y
plot(T,Y(:,2),'g-'); hold off; % y'
title('\beta = 0');
legend('y', 'y''');