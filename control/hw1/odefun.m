function dy = odefun(t,y)
dy = zeros(2,1);    % a column vector
dy(1) = y(2);
dy(2) = -5.*y(2)-4.*y(1)+3.*exp(-t).*((sign(t)+1).*0.5);
