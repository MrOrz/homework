#include <iostream>
#include "../src/symbols.h"
#include "../src/HuffmanTree.h"

using namespace std;

int main(){
  vector<DCSymbol> dc;
  dc.push_back(DCSymbol(1));
  dc.push_back(DCSymbol(1));
  dc.push_back(DCSymbol(3));
  dc.push_back(DCSymbol(4));
  dc.push_back(DCSymbol(4));
  dc.push_back(DCSymbol(5));
  dc.push_back(DCSymbol(5));
  dc.push_back(DCSymbol(5));
  dc.push_back(DCSymbol(5));
  dc.push_back(DCSymbol(6));
  dc.push_back(DCSymbol(6));
  dc.push_back(DCSymbol(6));
  dc.push_back(DCSymbol(7));
  
  HuffmanTree<DCSymbol, CompareDC> dctree;
  dctree.build(dc);
  
  cout << "1 : " << dctree.encode(DCSymbol(1)) << endl;
  cout << "3 : " << dctree.encode(DCSymbol(3)) << endl;
  cout << "4 : " << dctree.encode(DCSymbol(4)) << endl;
  cout << "5 : " << dctree.encode(DCSymbol(5)) << endl;
  cout << "6 : " << dctree.encode(DCSymbol(6)) << endl;
  cout << "7 : " << dctree.encode(DCSymbol(7)) << endl;

  cout << "////////////////////" << endl;
  
  vector<ACSymbol> ac;
  ac.push_back(ACSymbol( 0,0 ));
  ac.push_back(ACSymbol( 0,0 ));
  ac.push_back(ACSymbol( 0,0 ));
  ac.push_back(ACSymbol( 0,1 ));
  ac.push_back(ACSymbol( 3,1 ));
  ac.push_back(ACSymbol( 3,2 ));
  ac.push_back(ACSymbol( 3,2 ));
  ac.push_back(ACSymbol( 3,3 ));
  ac.push_back(ACSymbol( 3,3 ));
  ac.push_back(ACSymbol( 3,4 ));

  HuffmanTree<ACSymbol, CompareAC> actree;
  actree.build(ac);

  cout << "(0,0) : " << actree.encode( ACSymbol(0,0) ) << endl;
  cout << "(0,1) : " << actree.encode( ACSymbol(0,1) ) << endl;
  cout << "(3,1) : " << actree.encode( ACSymbol(3,1) ) << endl;
  cout << "(3,2) : " << actree.encode( ACSymbol(3,2) ) << endl;
  cout << "(3,3) : " << actree.encode( ACSymbol(3,3) ) << endl;
  cout << "(3,4) : " << actree.encode( ACSymbol(3,4) ) << endl;
  
  cout << "////////////////////" << endl;
  
  // single element test
  vector<int> num;
  num.push_back(1);
  num.push_back(1);
  num.push_back(1);
  
  HuffmanTree<int, less<int> > inttree;
  inttree.build(num);
  
  cout << "1 : " << inttree.encode(1) << endl;
  return 0;
}
