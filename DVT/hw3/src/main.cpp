//#include <iostream>
#include <cstring>
#include <cstdio>
#include <cmath>
#include "rawImage.h"
//#include "symbols.h"
#include "HuffmanCodec.h"
#include "ArithmeticCodec.h"
#include "stopwatch.h"

using namespace std;

///////* helper functions *///////

//* printUsage() when input arguments are wrong
void printUsage();

//* printDiff(filename, a, b)
// prints diff image between a and b, then returns PSNR
double printDiff(string filename,
                 RAWImage<unsigned char>& a, RAWImage<unsigned char>& b);

//* JPEG quantization & inverse quantization *//
void quantize (double2& dct); // quantize given dct coefficients
void inverse_quantize (double2& dct); // invert quantization

//* RLC(dct coefficients, block size)
// RLC for entire image wigh block size N
// returns 2-D array of RLCSymbol, where one row represents RLC for one block.
vector<vector<RLCSymbol> > RLC(const double2& dct, const int N);
void printRLC(const vector<vector<RLCSymbol> >&, const string& filename);

//* RLD( array of RLC symbols, block size, # of block per row in pictire)
// RLD for blocks.
// for example:
//   rlc has 30 rows, N = 8, m = 6 --> decodes 6*5=30 blocks, each block is 8x8.
//   so the returned dct coefficients has the dimension (6*8)x(5*8).
double2 RLD(vector<vector<RLCSymbol> >& rlc, const int& N, const int& m);


/* =================================================================== */
//                              main script
/* =================================================================== */

int main(int argc, char* argv[]){

  // check arguments
  if(argc != 3){
    printUsage();
    return 0;
  }

  // set fullname, filetype and filename, where fullname = filename.filetype
  string fullname = argv[2];
  char* _filetype = strrchr(argv[2], '.');
  string filetype = (_filetype ? _filetype+1 : "");
  if(_filetype) *_filetype = '\0'; // terminates the string argv[2] at '.'
  string filename = argv[2];

  if(strcmp(argv[1], "encode") == 0){     //-------- encode
    RAWImage<unsigned char> pic(64,64),       // filename.raw,      INPUT
                            pic_idct(64,64),  // filename.idct.raw, OUTPUT
                            pic_rlc(64,64);   // filename.rlc.raw,  OUTPUT
    // open -> DCT
    pic.open(fullname);
    double2 dct = pic.DCT(8);

    // -> IDCT w/o quantization, output filename.idct.raw
    pic_idct.IDCT(dct, 8); pic_idct.write(filename + ".idct.raw");

    // -> quantize -> RLC
    quantize(dct);
    vector<vector<RLCSymbol> > rlc = RLC(dct, 8);
    printRLC(rlc, filename);

    Stopwatch sw; // profiling encoding

    // Huffman encoding
    printf("-- Huffman encode: \n");
    sw.start();
    HuffmanCodec huff;
    huff.encode(rlc, filename + ".huf");
    printf("... in %lf seconds\n", sw.read());

    // Arithmetic encoding
    printf("-- Arithmetic encode: \n");
    sw.start();
    ArithmeticCodec arith;
    arith.encode(rlc, filename);
    printf("... in %lf seconds\n", sw.read());

    // RLD -> inverse quantize -> IDCT -> output filename.rlc.raw
    double2 dct_restored = RLD(rlc, 8, 8);
    inverse_quantize(dct_restored);
    pic_rlc.IDCT(dct_restored,8);
    pic_rlc.write(filename + ".rlc.raw");

    // output diff of the two reconstructed images, and prints PSNR on screen
    printf("idct PSNR: %lf\nrlc PSNR: %lf\n",
      printDiff(filename + ".idct.diff.raw", pic, pic_idct),
      printDiff(filename + ".rlc.diff.raw", pic, pic_rlc)
    );
  }
  else if(strcmp(argv[1], "decode")==0 ){ //-------- decode
    if(filetype == "huf"){                //-------- decoding *.huf
      HuffmanCodec huff;
      vector<vector<RLCSymbol> > rlc = huff.decode(fullname);
      //printRLC(rlc, filename + ".huf");

      // RLD -> inverse quantize -> IDCT -> output filename.huf.raw
      double2 dct_restored = RLD(rlc, 8, 8);
      inverse_quantize(dct_restored);
      RAWImage<unsigned char> pic(64,64);
      pic.IDCT(dct_restored,8);
      pic.write(filename + ".huf.raw");

    }
    else if(filetype == "ari"){           //-------- decoding *.ari
      // generates filename.ari.raw
      ArithmeticCodec arith;
      vector<vector<RLCSymbol> > rlc = arith.decode(filename);
      //printRLC(rlc, filename + ".ari");

      // RLD -> inverse quantize -> IDCT -> output filename.huf.raw
      double2 dct_restored = RLD(rlc, 8, 8);
      inverse_quantize(dct_restored);
      RAWImage<unsigned char> pic(64,64);
      pic.IDCT(dct_restored,8);
      pic.write(filename + ".ari.raw");

    }
    else  // invalid fullname
      printUsage();
  }
  else  // invalud argv[1]
    printUsage();

  return 0;
}

/* =================================================================== */
//                           helper functions
/* =================================================================== */

// short hand math functions
#define abs(x)    ( (x)>0?(x):-(x) )
#define square(x) ((x)*(x))

double printDiff(string filename,
  RAWImage<unsigned char>& a, RAWImage<unsigned char>& b){

  long SE = 0;  // square error
  RAWImage<unsigned char> diff(a.width, a.height);

  for(int x=0; x<a.width; ++x)
    for(int y=0; y<a.height; ++y){
      diff[x][y] = new unsigned char( abs( *(a[x][y]) - *(b[x][y]) ) );
      SE += square( *diff[x][y] );
      *diff[x][y] = toByte( *diff[x][y] * 10 );
      // diff graph with intensity gain 10
    }
  diff.write(filename);
  return 10*log10(square(255.0) / (double(SE)/(a.width * a.height) ));
}

// 8x8 quantization matrix
const unsigned int Q[8][8] = {
  { 16, 11, 10, 16, 24, 40, 51, 61},
  { 12, 12, 14, 19, 26, 58, 60, 55},
  { 14, 13, 16, 24, 40, 57, 69, 56},
  { 14, 17, 22, 29, 51, 87, 80, 62},
  { 18, 22, 37, 56, 68,109,103, 77},
  { 24, 35, 55, 64, 81,104,113, 92},
  { 49, 64, 78, 87,103,121,120,101},
  { 72, 92, 95, 98,112,100,103, 99}
};

void quantize (double2& dct){
  int w = dct.size(), h = dct[0].size();

  for(int x=0; x<w; ++x)
    for(int y=0; y<h; ++y)
      dct[x][y] = round( dct[x][y] / Q[x%8][y%8] );
}

void inverse_quantize (double2& dct){
  int w = dct.size(), h = dct[0].size();

  for(int x=0; x<w; ++x)
    for(int y=0; y<h; ++y)
      dct[x][y] *= Q[x%8][y%8];
}

vector<vector<RLCSymbol> > RLC(const double2& dct, const int N){
  vector<vector<RLCSymbol> > rlc; // RLCSymbol array to return
  int w = dct.size(), h = dct[0].size();
  int last_DC = 0; // predictive coding for DC amplitude

  rlc.reserve( w/N * h/N ); // row of rlc = number of blocks

  // raster scanning through blocks
  for(int yoff=0; yoff < h; yoff += N)
  for(int xoff=0; xoff < w; xoff += N){
    vector<RLCSymbol> current_row;
      // current row of RLCSymbols = RLCSymbols for current block
    // DC part.
    current_row.push_back( RLCSymbol(0,dct[xoff][yoff] - last_DC) );
    last_DC = dct[xoff][yoff];

    // AC part.
    int run_length = 0; // current run length of 0.

    // zigzag scanning through pixels
    int xstep = 1;
    for(int sum=1;sum<=14; ++sum){  // sum = x + y
      int x = (sum < 8? 0:sum-7); // upper or lower triangle. y = sum-x.
      for(x = (sum%2==1?x:sum-x); // sum is odd => scans bottom-left to top-right
          0 <= x && x < 8 && 0 <= sum-x && sum-x < 8; // 0 <= x, y <= 8
          x += xstep){
        // zigzag body
        int current_amplitude = dct[xoff + x][yoff + sum-x];

        if(current_amplitude == 0)  // '0' is encountered
          ++ run_length;
        else{                   // non-zero amplitude is encountered
          current_row.push_back( RLCSymbol(run_length, current_amplitude) );
              // record run length and the amplitude
          run_length = 0;       // reset run length for the next run
        }
        // end of zigzag body (pixel)
      }

      xstep *= -1;
    }
    // end of zigzag scan

    rlc.push_back(current_row);
  }
  // end of raster scan
  return rlc;
}

double2 RLD(vector<vector<RLCSymbol> >& rlc, const int& N, const int& m){
  int pic_w = m*N, pic_h = (rlc.size()/m)*N; // pic width and height
  double2 rld(pic_w); // dct coefficients array to return
  for(int i=0;i<pic_w; ++i)
    rld[i].resize(pic_h, 0.0);  // set all dct coefficients to zero

  vector<vector<RLCSymbol> >::iterator current_row = rlc.begin();
    // current row of RLCSymbols = RLCSymbols for current block
  int last_DC = 0; // predictive coding for DC amplitude

  // raster scanning through blocks
  for(int yoff=0; yoff < pic_h; yoff += N)
  for(int xoff=0; xoff < pic_w; xoff += N){
    vector<RLCSymbol>::iterator current_symbol = current_row->begin();
    int remained_run_length = current_symbol->run;
    bool EOB = false;

    // zigzag scanning through pixels
    int xstep = -1;
    for(int sum=0;sum<=14 && !EOB; ++sum){  // sum = x + y
      int x = (sum < 8? 0:sum-7); // upper or lower triangle. y = sum-x.
      for(x = (sum%2==1?x:sum-x); // sum is odd => scans bottom-left to top-right
          0 <= x && x < 8 && 0 <= sum-x && sum-x < 8; // 0 <= x, y <= 8
          x += xstep){
        // zigzag body
        if(remained_run_length > 0 ){
          // reached end of block, or run length does not reach zero
          rld[xoff+x][yoff+sum-x] = 0;
          --remained_run_length;
        }
        else{ // remained run length reaches zero
          rld[xoff+x][yoff+sum-x] = current_symbol->amplitude;
          ++ current_symbol;  // proceed to the next symbol
          if(current_symbol == current_row->end() ){  // if EOB is reached
            EOB = true;
            break;  // exit zigzag scanning, leave the rest as 0.
          }
          remained_run_length = current_symbol->run;
            // updates the run length to the one of the next symbol
        }
        // end of zigzag body (pixel)
      }
      xstep *= -1;
    }
    // end of zigzag scan

    rld[xoff][yoff] += last_DC; // DC predictive decoding
    last_DC = rld[xoff][yoff];

    current_row ++; // proceed to the next block
  }
  // end of raster scan

  return rld;
}

void printUsage(){
  printf("usage: hw3 encode <filename>\n");
  printf("       hw3 decode <*.huf>\n");
  printf("       hw3 decode <*.ari>\n");
}

void printRLC(const vector<vector<RLCSymbol> >& rlc, const string& filename){
  // output .rlc file & prints bit information
  unsigned max_size = 0, // max amplitude size
           ac_count = 0; // # of AC symbols

  FILE* f_rlc = fopen( (filename + ".rlc").c_str(), "w");
  for(unsigned i=0; i< rlc.size(); ++i){
    fprintf(f_rlc, "%d,", rlc[i][0].amplitude); // DC amplitude
    if(rlc[i][0].size > max_size) max_size = rlc[i][0].size;
    for(unsigned j=1; j < rlc[i].size(); ++j ){  // AC run & amplitude
      fprintf(f_rlc, "(%d,%d),", rlc[i][j].run, rlc[i][j].amplitude);
      if(rlc[i][j].size > max_size) max_size = rlc[i][j].size;
    }
    fprintf(f_rlc, "EOB\n");
    ac_count += rlc[i].size(); // -1 for DC, +1 for EOB
  }
  fclose(f_rlc);
  printf("%d AC Nodes, maximum amplitude takes %d bits.\n", ac_count, max_size);
}

