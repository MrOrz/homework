
#include "symbols.h"

// ------------------- HuffmanCodec ---------------------
class HuffmanCodec {
  public:
    /// encode(data, filename)
    // data is a vector of blocks, a block contains a vector of RLCSymbols.
    //  filename is the full name of the target .huf file.
    void encode(const vector<vector<RLCSymbol> >& data, const string& filename);
    
    /// decode(filename)
    //  filename is the full name of a .huf file.
    vector<vector<RLCSymbol> > decode(const string& filename);
};


