/*
Usage: 
  huffman -encode <file.txt> <file.dict> <file.encode>
  huffman -decode <file.encode> <file.dict> <file.decode>
*/
#include<cstring>
#include "stopwatch.h"
#include "HuffmanTree.h"

int main(int argc, char *argv[])
{
    Stopwatch sw; 
    HuffmanTree ht;
    
    if(strcmp(argv[1], "-encode") == 0){
      sw.start();
      ht.txt2HuffmanTree(argv[2]);
      ht.encode(argv[2],argv[3],argv[4]);
    }
    else if(strcmp(argv[1], "-decode") == 0){
      sw.start();    
      ht.dict2HuffmanTree(argv[3]);
      ht.decode(argv[2],argv[4]);
    }
    
    printf("CPU Time = %lf sec\n", sw.read());

    return 0;
}

