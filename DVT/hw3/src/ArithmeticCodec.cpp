#include "CodecHelper.h"
#include "ArithmeticCodec.h"
#include "ac.h"
#include <string>
#include <iostream>
#include <fstream>
#include <map>

using namespace std;

static int* map2array(const map<unsigned, unsigned>& m){
  int* arr = new int[ m.size() ];
  for(map<unsigned, unsigned>::const_iterator it = m.begin();
      it != m.end(); ++it ) arr[it->first] = it->second;
  return arr;
}

//--- Arithmetic codec----
void ArithmeticCodec::encode(const vector<vector<RLCSymbol> >& data, const string& filename){

// Encoder workflow:
//  main.cpp 傳來 (run,size,amp)(run,size,amp)...
//  --> 分成 所有DC 和 各個block的AC
//  並且初始化 encoder。

  ac_encoder arith;    // the encoder that produces filename.ari
  ac_model   dc_mdl; // model for DC data
  ac_model   ac_mdl; // model for AC data

  ac_encoder_init(&arith, string(filename + ".ari").c_str() );
  ofstream dict_out(string(filename + ".ari.dict").c_str() );
  ofstream amp_out(string(filename + ".ari.amp").c_str() );

//  DC Encoding Workflow:
//      (size,amp)(size,amp)(size,amp)...
//      --> DCSymbol(size) DCSymbol(size) DCSymbol(size) ...
//    1.統計有幾種 DCSymbol，建立 DCSymbol -> symbol_id mapping，
//      以及 frequency array dc_freq[symbol_id] = 該 symbol 數量。
//      將 DCSymbol -> symbol_id mapping 與其 frequency 寫入字典。
//    2.用以上統計資訊建立 DC model。

  // extract DC data from data (2-d array of RLCed DCT coefficients)
  // and count symbols. Assigned id to symbol are [0 , symbol_count)
  vector<DCSymbol> dc_data;
  map<DCSymbol, unsigned, CompareDC> dc2id; // DCSymbol -> symbol_id map
  map<unsigned, unsigned> count; // temporarily save the count, symbol_id->count

  for(unsigned i=0; i<data.size(); ++i){ // for each block
    dc_data.push_back(DCSymbol(data[i][0].size ) );
    DCSymbol s = dc_data.back();   // the currently inserted symbol

    if(dc2id.count( s ) == 0 ){     // the symbol is not encountered before
      unsigned current_id = dc2id.size(); // an id for the new symbol
      dc2id[s] = current_id;      // assign the id to the symbol
      count[ current_id ] = 0;    // initialize the count for this id
    }
    ++count[ dc2id[ s ] ];
  }

  // dump count[] to dc_freq[].
  int* dc_freq = map2array(count);

  // initialize model
  ac_model_init(&dc_mdl, count.size(), dc_freq, 0);

  // print DC dictionary, format: "size@id:count"
  for(map<DCSymbol, unsigned, CompareDC>::iterator it = dc2id.begin();
      it != dc2id.end(); ++it ){
    // it->first is symbol, it->second is symbol_id
    dict_out << (it->first).size << "@"
             << it->second << ":" << count[ it->second ] << endl;
  }
  dict_out << "-1\n"; // end if DC dictionary

  cout << "DC dictionary size: " << dc2id.size() << " entries." << endl;

//  AC Encoding Workflow:
//  把所有 block 的 AC 串成一長串。記得保留 EOB。
//      (run,size,amp)(run,size,amp)(run,size,amp)...
//      --> ACSymbol(run,size) ACSymbol(run,size) ACSymbol(run,size) ...
//    1. 統計有幾種 ACSymbol, 建立 ACSymbol -> symbol_id mapping，
//       以及 frequency array freq_ac[symbol_id] = 該 symbol 數量。
//       將 ACSymbol -> symbol_id mapping 與其 frequency 寫入字典。
//    2. 用以上統計資訊建立 AC Symbol。

  // put AC data for all blocks together
  vector<ACSymbol> ac_data;
  map<ACSymbol, unsigned, CompareAC> ac2id; // ACSymbol -> symbol_id map
  count.clear();                            // reuse symbol_id->count mapping
  ac2id[EOB] = 0; count[ ac2id[EOB] ] = 0;  // put EOB into mapping first

  for(unsigned i=0; i<data.size(); ++i){ // for each block
    for(unsigned j=1; j<data[i].size(); ++j){ // for each AC RLC symbol in the block
      ac_data.push_back(ACSymbol(data[i][j].run, data[i][j].size ) );
      ACSymbol s = ac_data.back();   // the currently inserted symbol

      if(ac2id.count( s ) == 0 ){     // the symbol is not encountered before
        unsigned current_id = ac2id.size(); // an id for the new symbol
        ac2id[s] = current_id;      // assign the id to the symbol
        count[ current_id ] = 0;    // initialize the count for this id
      }
      ++count[ ac2id[ s ] ];
    }

    ac_data.push_back(EOB);
    ++count[ ac2id[ EOB ] ];
  }

  // dump count[] to ac_freq[]
  int* ac_freq = map2array(count);

  // initialize model
  ac_model_init(&ac_mdl, count.size(), ac_freq, 0);

  // print AC dictionary, format: "run,size@id:count"
  for(map<ACSymbol, unsigned, CompareAC>::iterator it = ac2id.begin();
      it != ac2id.end(); ++it ){
    // it->first is symbol, it->second is symbol_id
    dict_out << (it->first).run << "," << (it->first).size << "@"
             << it->second << ":" << count[ it->second ] << endl;
  }
  dict_out << "-1\n"; // end if AC dictionary
  cout << "AC dictionary size: " << ac2id.size() << " entries." << endl;

  dict_out.close();

//  VLC Encoding Workflow:
//  對於每個 block:
//  --> 使用 DC model encode 一個 DC Symbol (with its id)，接著將 amplitude 寫入 amp 檔案。
//  --> 使用 AC model encode 一個 AC Symbol (with its id)，接著將 amplitude 寫入 amp 檔案。
//  輸出三個檔案
//       一個 .ari 為 encoder 輸出的二進位檔，包含 run, size 資訊。
//       一個 .ari.amp 儲存 amplitude 資訊。
//       一個 .ari.dict 為 DCSymbol | ACSymbol --> symbol_id 的對應，以及其數量統計 (freq)

  // block-wise encode

  unsigned i=0,j=0, amp_bit_count = 0;
  for(; i<data.size(); ++i){ // for each block
    // DC VLC
    ac_encode_symbol( &arith, &dc_mdl , dc2id[ dc_data[i] ]); // encode DC

    if(data[i][0].size > 0)    // DC size might be zero. Only print when > 0.
      amp_out << binary(data[i][0]);// print amplitude's binary representation

    // update statistical count
    amp_bit_count += data[i][0].size;

    // AC VLC
    for(int j_start = j ; j-j_start < data[i].size() -1; ++j){
      // there are data[i].size()-1 AC values within block
      ac_encode_symbol(&arith, &ac_mdl, ac2id[ ac_data[j] ]);
      amp_out << binary(data[i][j-j_start+1]);

      // update statistical count
      amp_bit_count += data[i][j-j_start+1].size;
    }
    // EOB VLC
    ac_encode_symbol(&arith,  &ac_mdl,  ac2id[ EOB ]);
    ++j; // skip the EOB inside ac_data[]
  }

  ac_encoder_done(&arith);
  ac_model_done(&dc_mdl); ac_model_done(&ac_mdl);


  cout << "Encoded data (symbol + amplitude) size : "
       << amp_bit_count + (int) ac_encoder_bits(&arith)  <<  " bits." << endl;

  amp_out.close();
}

vector<vector<RLCSymbol> > ArithmeticCodec::decode(const string& filename){
// Decoder Workflow:
//  初始化 decoder，準備讀取 .ari。
  ac_decoder arith;
  ac_model dc_mdl, ac_mdl;

  ac_decoder_init(&arith, string(filename + ".ari").c_str() );
  ifstream dict_in(string(filename + ".ari.dict").c_str() );
  ifstream amp_in(string(filename + ".ari.amp").c_str() );

//  讀取 .ari.dict 來取得 symbol_id -> DCSymbol | ACSymbol 的對應與數量，
//  建立 freq_dc|ac[symbol_id] 來初始化 DC model 與 AC model。

  map<unsigned, DCSymbol, CompareAC> dc; // id -> DCSymbol map
  map<unsigned, unsigned> count;         // id -> count map
  while(1){
    int size, id, c; char ignore;

    dict_in >> size;
    if(size == -1) break; // end of DC dictionary
    dict_in >> ignore >> id >> ignore >> c;
    dc[id] = DCSymbol(size);
    count[id] = c;
  }
  int* freq_dc = map2array(count);
  ac_model_init(&dc_mdl, count.size(), freq_dc, 0);

  map<unsigned, ACSymbol, CompareAC> ac; // id -> ACSymbol map
  count.clear();                         // id -> count map

  while(1){
    int run, size, id, c; char ignore;

    dict_in >> run;
    if(run == -1) break; // end of DC dictionary
    dict_in >> ignore >> size >> ignore >> id >> ignore >> c;

    ac[id] = ACSymbol(run, size);
    count[id] = c;
  }
  int* freq_ac = map2array(count);
  ac_model_init(&ac_mdl, count.size(), freq_ac, 0);

  dict_in.close();

//  開始 decode ari：
//     1. decode 出 DC symbol id，查詢 mapping 得到 size，
//        再從 .amp 檔中讀出 size 個 bit，得到 DC amplitude
//     2. decode 出 AC symbol id，查詢 mapping 得到 run 與 size，
//        再從 .amp 檔中讀出對應 size 個 bit 得到 AC amplitude。
//     3. 重複 2；若讀到 EOB，則重複 1
  vector<vector<RLCSymbol> > rlc;

  while(1){
    vector<RLCSymbol> block; string amplitude; int id;

    // DC decode

    id = ac_decode_symbol(&arith, &dc_mdl);

    DCSymbol bufDC = dc[id];

    if(bufDC.size > 0){ // DC coefficient > 0, which is often the case.
      amp_in.width(bufDC.size);  // limit read size to read amplitude
      amp_in >> amplitude; // get amplitude's binary representation
    }

    block.push_back(RLCSymbol(0, bufDC.size, binary(amplitude) ));

    // AC decode
    while(1){

      id = ac_decode_symbol(&arith, &ac_mdl);
      ACSymbol bufAC = ac[id];
      if(bufAC.run==0 && bufAC.size==0) // EOB encountered, exit AC decode
        break;

      amp_in.width(bufAC.size);  // limit read size to read amplitude
      amp_in >> amplitude; // get amplitude's binary representation

      block.push_back(RLCSymbol(bufAC.run, bufAC.size, binary(amplitude) ));
    }

    rlc.push_back(block); // save current block and proceed to the next

    if(amp_in.peek() == EOF) break; // finish reading when no amplitude to read.
  }

  ac_decoder_done(&arith);
  ac_model_done(&dc_mdl); ac_model_done(&ac_mdl);

  return rlc;
}

