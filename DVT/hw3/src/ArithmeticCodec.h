#include "symbols.h"

// ------------------- ArithmeticCodec ---------------------
class ArithmeticCodec {
  public:
    /// encode(data, filename)
    // data is a vector of blocks, a block contains a vector of RLCSymbols.
    //  filename is the name (WITHOUT ".ari" POSTFIX) of output files
    void encode(const vector<vector<RLCSymbol> >& data, const string& filename);
    
    /// decode(filename)
    //  filename is the name (WITHOUT ".ari", ".dict" or ".amp" POSTFIX) of the files
    vector<vector<RLCSymbol> > decode(const string& filename);
};

