#include "HuffmanTree.h"
#include "HuffmanCodec.h"
#include "CodecHelper.h"
#include <string>
#include <iostream>
#include <fstream>

using namespace std;

// Encoder workflow: 
//      main.cpp 傳來的 (run,size,amp)(run,size,amp)...
//  --> 分成 所有DC 和 各個block的AC

void HuffmanCodec::encode( const vector<vector<RLCSymbol> >& data, 
                           const string& filename ){
                           
//  DC Encoding Workflow:
//      (size,amp)(size,amp)(size,amp)...
//  --> (size)(size)(size)(size)(size)... 傳入 HuffmanTree 建樹。(size) = DC

  // extract DC data from data (2-d array of RLCed DCT coefficients)
  vector<DCSymbol> dc_data;
  for(unsigned i=0; i<data.size(); ++i){ // for each block
    dc_data.push_back(DCSymbol(data[i][0].size ) );
  }
  
  // create the tree and fetch generated dictionary
  HuffmanTree<DCSymbol, CompareDC> dc_tree;
  map<DCSymbol, string, CompareDC> dc_dictionary = dc_tree.build(dc_data);
  
  cout << "DC dictionary size: " << dc_dictionary.size() << " entries." << endl;
  
//  AC Encoding Workflow:
//  把所有 block 的 AC 串成一長串。記得保留 EOB。
//      (run,amp)(run,amp)(run,amp)(run,amp)(run,amp)...
//  --> (run,size)(run,size)(run,size)... 傳入 HuffmanTree 建樹。(run,size) = AC

  // put AC data for all blocks together
  vector<ACSymbol> ac_data;
  for(unsigned i=0; i<data.size(); ++i){ // for each block
    for(unsigned j=1; j<data[i].size(); ++j) // for each AC RLC symbol in the block
      ac_data.push_back(ACSymbol(data[i][j].run,data[i][j].size ) );
      
    ac_data.push_back(EOB); 
  }
  
  // create the tree and fetch generated dictionary
  HuffmanTree<ACSymbol, CompareAC> ac_tree;
  map<ACSymbol, string, CompareAC> ac_dictionary = ac_tree.build(ac_data);
  
  cout << "AC dictionary size: " << ac_dictionary.size() << " entries." << endl;
  
//  VLC Encoding Workflow
//  --> 輸入 DC 值至 HuffmanTree.encode()，得到 codeword 來寫 「codeword-amplitude」。
//      亦可直接使用拿回來的 mapping。
//  --> 輸入 AC 值至 HuffmanTree.encode()，得到 codeword 來寫 「codeword-amplitude」。
//      亦可直接使用拿回來的 mapping。

//  輸出：一個 *.huf, 內容：
//       width of picture, height of picture, block size,
//       DC dict, AC dict, (DC VLC, AC VLC, EOB) * blocks
  ofstream fout (filename.c_str() );
  
  // DC dictionary
  for(map<DCSymbol, string, CompareDC>::iterator it = dc_dictionary.begin();
    it != dc_dictionary.end(); ++it)
    fout << (it->first).size << ":" << it->second << "\n";
    
  fout << "-1\n"; // end of DC dictionary
  
  // AC dictionary
  for(map<ACSymbol, string, CompareAC>::iterator it = ac_dictionary.begin();
    it != ac_dictionary.end(); ++it)
    fout << (it->first).run << "," << (it->first).size << ":" 
         << it->second << "\n";
         
  fout << "-1\n"; // end of AC dictionary
    
  // block-wise encode
  
  unsigned i=0,j=0, bit_count = 0;
  for(; i<data.size(); ++i){ // for each block
    // DC VLC
    fout << dc_dictionary[ dc_data[i] ]; // looking DCSymbol up in dictionary
                                         // and prints size.
    if(data[i][0].size > 0)    // DC size might be zero. Only print when > 0.
      fout << binary(data[i][0]);// print amplitude's binary representation
      
    // update statistical count
    bit_count += dc_dictionary[ dc_data[i] ].size() + data[i][0].size;
      
    // AC VLC
    for(int j_start = j ; j-j_start < data[i].size() -1; ++j){
      // there are data[i].size()-1 AC values within block
      fout << ac_dictionary[ ac_data[j] ]; // looking ACSymbol up in dictionary
      fout << binary(data[i][j-j_start+1]);
      
      // update statistical count
      bit_count += ac_dictionary[ ac_data[j] ].size() + data[i][j-j_start+1].size;
    }
    // EOB VLC
    fout << ac_dictionary[EOB]; 
    ++j; // skip the EOB inside ac_data[]
    
    // update statistical count
    bit_count += ac_dictionary[ EOB ].size();
  }
  
  fout.close();
  
  cout << "Encoded data size : " << bit_count << " bits." << endl;
}

// Decoder workflow:
//    吃一個 *.huf，依序進行：
vector<vector<RLCSymbol> > HuffmanCodec::decode(const string& filename){
//      由 width、height 與 block size 計算 block 總數目。
  ifstream fin(filename.c_str() );

//      DC dict 解碼：讀入 DC 的 DC->size mapping，傳入 HuffmanTree 建 DC 樹。
  map<DCSymbol, string, CompareDC> dc_dictionary;
  while(1){
    int size; string codeword;
    fin >> size; 
    if(size == -1) break; // end of dictionary
    fin.ignore(1,':');
    fin >> codeword;
    dc_dictionary[DCSymbol(size)] = codeword;
  }
  HuffmanTree<DCSymbol, CompareDC> dc_tree;
  dc_tree.build(dc_dictionary);
  
//      AC dict 解碼：讀入 block 數量個 AC 的 DC->size mapping，傳入 HuffmanTree 建 AC 樹。
  map<ACSymbol, string, CompareAC> ac_dictionary;
  while(1){
    int run, size; string codeword;
    fin >> run; 
    if(run == -1) break; // end of dictionary
    fin.ignore(1,',');
    fin >> size;
    fin.ignore(1,':');
    fin >> codeword;
    ac_dictionary[ACSymbol(run, size)] = codeword;
  }
  HuffmanTree<ACSymbol, CompareAC> ac_tree;
  ac_tree.build(ac_dictionary);

//      DC VLC 解碼：一個一個 bit 輸入 HuffmanTree.decode，
//        若解出 DC 則按照其解出的 size，自 huf 檔案讀入 amplitude，記下 (run=0, amplitude)。
//        開始解該 block 之 AC。
//      AC VLC 解碼：一個一個 bit 輸入 HuffmanTree.decode，
//        若解出 AC 則按照其解出的 size，自 huf 檔案讀入 amplitude，記下 (run, amplitude)。
//        直到解出 EOB

  fin.ignore(1,'\n'); // get rid of '\n'
  
  vector<vector<RLCSymbol> > rlc;
  while(1){
    vector<RLCSymbol> block; char buf; int size; string amplitude;
    
    // DC decode
    
    DCSymbol bufDC;
    do{
      fin.get(buf);
    }while (!dc_tree.decode(buf, bufDC)); // read until we reach a leaf

    if(fin.eof() ) // EOF is "pass-the-end", thus we capture EOF here.
      break;
    
    size = bufDC.size;
    if(size>0){ // DC coefficient > 0, which is often the case.
      fin.width(size);  // limit read size to read amplitude
      fin >> amplitude; // get amplitude's binary representation
      
    }
    
    block.push_back(RLCSymbol(0,size, binary(amplitude) ));
    
    // AC decode
    while(1){
      int run;
      
      ACSymbol bufAC;
      do{
        fin.get(buf); 
      } while(!ac_tree.decode(buf, bufAC)) ; // read until we reach a leaf
               
      run  = bufAC.run;
      size = bufAC.size;
      if(run==0 && size==0) // EOB encountered, exit AC decode
        break;
      fin.width(size);  // limit read size to read amplitude
      fin >> amplitude; // get amplitude's binary representation
      
      block.push_back(RLCSymbol(run,size, binary(amplitude) ));
    }    
    rlc.push_back(block); // save current block and proceed to the next
  }

//  輸出：一個 vector<vector<RLCSymbol> >，內含 (run,amp)(run,amp)(run,amp)(run,amp)……
//        一個 block 佔一列。
  return rlc;
}
