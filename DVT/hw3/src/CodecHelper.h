#ifndef __CODEC_HELPER__
#define __CODEC_HELPER__

#include "symbols.h"
#include "string"
//----- helper function ------

static ACSymbol EOB = ACSymbol(0,0);

/// string binary(RLCSymbol)
//  convert RLCSymbol to its binary representation as string
//
static string binary(const RLCSymbol& s){
  long amp = s.amplitude;// unsigned size = s.size;
  if(amp < 0) amp *= -1;
  string bits;
  do{
    char tmp = amp % 2 + '0';
    if(s.amplitude < 0) tmp = (tmp == '0'? '1':'0'); // 1's complement
    bits.push_back( tmp );
  }while(amp/=2);
  
  return string(bits.rbegin(), bits.rend() );
}

/// int binary(string)
//  binary representation to integer
//
static int binary(string str){
  bool negative = (str[0] == '0');
  if(negative){ // do 1's complement
    for(unsigned i=0;i<str.size(); ++i )
      str[i] = (str[i] == '0'?'1':'0');
  }
  int bits = 0;
  for(unsigned i = 0; i < str.size(); ++i )
    bits = bits * 2 + (str[i]-'0');
    
  if(negative)bits *= -1; // recover the sign
    
  return bits;
}

#endif
