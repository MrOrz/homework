// **************************************************************************
// File       [stopwatch.h]
// Author     [MrOrz]
// **************************************************************************

#ifndef _STOPWATCH_H_
#define _STOPWATCH_H_

#include <sys/time.h>
using namespace std;

/*
** Definitions are so short that it does not require .cpp file to define its 
** member functions.
*/

class Stopwatch{
  public:
  void start(){ gettimeofday(&_started_time, NULL); }
  
  // return the time elapsed from started (in sec)
  double read(){  
    struct timeval _ended_time;
    gettimeofday(&_ended_time, NULL);
    return _ended_time.tv_sec - _started_time.tv_sec 
           + (_ended_time.tv_usec - _started_time.tv_usec) * 0.000001;
  }
  private:
    struct timeval _started_time;
};

#endif

