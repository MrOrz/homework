
#ifndef _HUFFMAN_TREE_H_
#define _HUFFMAN_TREE_H_

#include <cstdio> 
#include <string>
#include <queue>                // priority_queue
#include <vector>
#include <map>

#include "HuffmanNode.h"

using namespace std;

template <class T, class Cmp >
class HuffmanTree {

public:
    HuffmanTree():_root(NULL){};        // constructor 

    // generate huffmanTree with the given vector of node data
    const map<T, string, Cmp>& build(const vector<T>&); 
    
    // encode a file. before calling encode, build(data) must be called!
    const string encode(const T&);  

    // read dictionary entry (data->codeword mapping) to generate huffmanTree
    void build(map<T, string, Cmp>);
    
    // decode a bit. before calling decode(), build(dict) must be called!
    const bool decode(const unsigned char&, T&);
  
  #ifdef __DEBUG_TREE__
    void printNodes();
    void printNodes(HuffmanNode<T>*, const string = "");
  #endif

private:
    void _createMap(const HuffmanNode<T>*,const string);
    map<T, string, Cmp> _huffmanMap;  // map T to huffman code-word
    // example _huffmanMap[DC(6)]="00";  Huffman code of DC(size=6) is "00"

    HuffmanNode<T> *_root;      // HuffmanTree root node pointer
    int _nodeCount;             // number of nodes in the HuffmanTree
};

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//                                Implementation
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

// a function class used in priority_queue
//
template <typename T>
class MyCompare {
  public:
  bool operator () (const HuffmanNode<T>& lhs, const HuffmanNode<T>& rhs) const{
    if(lhs.count < rhs.count) return false;
    else return true;
  }
};
  
// recursively traverse the Huffman Tree
//
template <class T, class Cmp >
void HuffmanTree<T,Cmp>::
  _createMap(const HuffmanNode<T>* n, const string code = ""){

  if(n->isLeaf()){
    if(code == "")  // if there is only one node, assign a codeword for it
      _huffmanMap[n->data] = "0";  
    else            // often the case
      _huffmanMap[n->data] = code;
  }else{
    _createMap(n->children[0], code + '0');
    _createMap(n->children[1], code + '1');
  }

}

// generate huffmanTree with the given vector of node data
// 
template <class T, class Cmp >
const map<T, string, Cmp>&
  HuffmanTree<T,Cmp>::build(const vector<T>& node_data){
  
  // Step 1: create and count the nodes from raw node_data.

  vector<HuffmanNode<T> > nodes;    // nodes[]
  map<T, unsigned, Cmp >     data2nodeId;  // maps node data to nodeId
    // note: nodes[] is a vector, which will re-allocate memory when enlarged.
    //       Thus we can only keep a mapping to node ID instead of a direct 
    //       reference to the node inside vector, or we get segmentation fault.

  for(unsigned i=0;i < node_data.size(); ++i){
    T data = node_data[i];
    if(data2nodeId.count(data) == 0 ){ // if the node has not been created before
      nodes.push_back(HuffmanNode<T>(data,1) ); // create the node in nodes[]
      data2nodeId[data] = nodes.size() -1;  // establish data -> nodeId mapping 
    }
    else
      ++(nodes[ data2nodeId[data] ].count);   // increase the node count
  }
  _nodeCount = nodes.size();
  
  if(_nodeCount > 1){ // have multiple nodes, which is often the case.
    
    // Step 2: put the nodes into the heap
    MyCompare<T> cmp;
    
    priority_queue<HuffmanNode<T>, vector<HuffmanNode<T> >, MyCompare<T> > 
      nodeHeap(nodes.begin(), nodes.end(), cmp);

    // Step 3: popping elements out of the heap to make a tree
    while(nodeHeap.size() > 1 ){
      HuffmanNode<T> combined;
      for(int i=0; i<2;++i){    // iterating in the 2 children
        combined.children[i] = new HuffmanNode<T>( nodeHeap.top() );
        /* "new" keyword moves the popped node from memory stack to memory heap,
           which will survive after build() terminates. */
        combined.count += nodeHeap.top().count;
        nodeHeap.pop();
      }
      nodeHeap.push(combined);
    }
    _root = new HuffmanNode<T>( nodeHeap.top() );
  }
  else{ // if there is only one node, just set it as root.
    _root = new HuffmanNode<T>(nodes[0]);
  }
  // create data-> code-word map.
  _createMap(_root);
  
  // return data->codeword map.
  return _huffmanMap;
}

// return encoded code word of given data
//
template <class T, class Cmp >
const string
  HuffmanTree<T,Cmp>::encode(const T& data){
  
  if(_huffmanMap.count(data) > 0 ) // if data is found
    return _huffmanMap[data];
  else                                             // if data is not found
    return "";
}

// read data->codeword mapping (i.e dictionary) to generate huffmanTree 
//
template <class T, class Cmp >
void HuffmanTree<T,Cmp>::build(map<T, string, Cmp> huffman_map){
  // put the map given into the _huffmanMap attribute
  _huffmanMap = huffman_map;
  _nodeCount = huffman_map.size();

  // start building trees
  _root = new HuffmanNode<T>();
  typename map<T, string, Cmp>::iterator entry;
    // see http://www.parashift.com/c++-faq-lite/templates.html#faq-35.18
    // for the additional "typename" keyword. This is to keep g++ happy.
  for( entry = huffman_map.begin();
       entry != huffman_map.end(); ++entry ){  // for each entry of the dictionary  
       
    T data = entry->first;         // the data
    string code = entry->second;   // corresponding code-word for data
    
    HuffmanNode<T>* currentNode = _root;
    for(unsigned j=0; j < code.size(); ++j){ // traveling to a leaf 
      int id = int(code[j] - '0');    // 0-> left, 1-> right
      
      if(currentNode->children[id] == NULL)
        currentNode->children[id] = new HuffmanNode<T>;
        // if the node does not exist, create one

      currentNode = currentNode->children[id];
    }
    currentNode->data = data;
  }
}

// traverse the huffman tree for ONE STEP by the given bit.
// If a leaf node is reached, the data of the leaf node is returned.
// Otherwise, null is returned.
//
template <class T, class Cmp >
const bool HuffmanTree<T,Cmp>::decode(const unsigned char& bit, T& data_found){

  static HuffmanNode<T>* currentNode = _root;
     // record where the function traverses to, thus uses static.
     
  // keep walking down the branch according to the bit 
  currentNode = currentNode->children[ int(bit - '0') ];
  if( currentNode->isLeaf() ){    // leaf is reached
    data_found = currentNode->data;
    currentNode = _root;          // restart walking from root
    return true;
  }
  else                            // leaf is not reached
    return false;
}

#endif

#ifdef __DEBUG_TREE__
template <class T, class Cmp >
void HuffmanTree<T,Cmp>::printNodes(){
  printNodes(_root);
}

template <class T, class Cmp >
void HuffmanTree<T,Cmp>::printNodes(HuffmanNode<T>* current, const string code){
  if(current -> isLeaf() )
    printf("%s\t%s\n", code.c_str() ,_huffmanMap[current->data].c_str() );
  else{
    for(int i=0; i<2; ++i){
      string passing = code;
      passing += (i+'0'); passing += "->";
      printNodes(current->children[i], passing);    
    }
  }
}

#endif

