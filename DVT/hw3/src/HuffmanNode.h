/*
  HuffmanNode.h

  Usage: Define the nodes used by HuffmanTree.
  Description: It is a class that represents a node data structure, and is only
    used within the HuffmanTree, thus it is good to keep it simple.
  
  Attributes: 
    T           ch            
    int         count
    HuffmanNode *children[]

  Methods:
         HuffmanNode()
         HuffmanNode(ch, count)   
    bool isLeaf()
*/
#ifndef _HUFFMAN_NODE_H_
#define _HUFFMAN_NODE_H_
template <class T>
class HuffmanNode {
    public:
        // empty constructor
        HuffmanNode():data(NULL),count(0)                       
          {children[0]=children[1]=NULL;}                 
          
        // constructor with init value
        HuffmanNode(T d,int count):data(d),count(count) 
          {children[0]=children[1]=NULL;}

        T    data;                      // data for HuffmanNode
        int  count;                     // frequency of the char

        HuffmanNode<T> *children[2];    // 0: left child, 1: right child.
                                        //    use array index for quick decoding

        bool isLeaf() const{            // check if this node is leaf node
          return (children[0] == NULL || children[1] == NULL);
        }              

};

#endif

