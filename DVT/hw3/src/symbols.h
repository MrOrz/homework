#ifndef __SYMBOLS__
#define __SYMBOLS__

#include <vector>

using namespace std;

//* double2
// alias for 2-D double array 
typedef vector<vector<double> > double2;

//* RLCSymbol
// RLC Symbol (run, amplitude)

class RLCSymbol{
  public:
    unsigned run;   // 0-run length
    unsigned size;  // size (binary length) of amplitude
    int amplitude;
    RLCSymbol(unsigned c, int v):run(c),amplitude(v){
      // calculate size of amplitude
      if(v < 0) v *= -1;
      for(size=0; v > 0; v/=2, ++size) continue;
    }
    RLCSymbol(unsigned r, unsigned s, int a):run(r),size(s),amplitude(a){}
};

//* DC Symbol
// DC Symbol (size)

class DCSymbol{
  public:
    unsigned size;
    DCSymbol(unsigned s = 0): size(s){};
};

//* AC Symbol
// AC Symbol (run, size)
class ACSymbol{
  public:
    unsigned run;
    unsigned size;
    ACSymbol(unsigned r = 0, unsigned s = 0):run(r), size(s){};
};

//* DC Compare function for map<>
class CompareDC {
  public:
  bool operator () (const DCSymbol& lhs, const DCSymbol& rhs) const{
    return lhs.size < rhs.size;
  }
};

//* AC Compare function for map<>
class CompareAC {
  public:
  bool operator () (const ACSymbol& lhs, const ACSymbol& rhs) const{
    if(lhs.run == rhs.run)
      return lhs.size < rhs.size;
    else
      return lhs.run < rhs.run;
  }
};
#endif
