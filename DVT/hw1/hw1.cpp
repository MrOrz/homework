#include<iostream>
#include<string>
#include<cstring>
#include<cstdio>
#include<cmath>
#include<vector>

#define abs(x) ( (x)>0?(x):-(x) )
#define square(x) (x)*(x)

using namespace std;

/* helper function */

inline unsigned char toByte(double value){
  if(value>255.0)   value = 255.0;
  else if(value<0)  value = 0.0;
  return (unsigned char)(value);
}


/* Color space classes as pixels */

class RGB{
  public:
    unsigned char R,G,B;
    RGB(unsigned char r, unsigned char g, unsigned char b):R(r),G(g),B(b){}
    void print(FILE* fout){
      fprintf(fout, "%c%c%c", R,G,B);
    }
    RGB* operator - (const RGB& p){   // return difference
      return new RGB(abs(R-p.R), abs(G-p.G), abs(B-p.B));
    }
};

class YUV{
  public:
    unsigned char Y,U,V;
    YUV(unsigned char y, unsigned char u, unsigned char v):Y(y),U(u), V(v){}
    YUV(const RGB &pixel){
      fromRGB(pixel);
    }
    void fromRGB(const RGB &pixel){
      double R=pixel.R, G=pixel.G, B=pixel.B;
      double y = 0.299 * R + 0.587 * G + 0.114 * B, // 0~255
             u = 0.492 * (B - y),                   // (-0.436~0.436) * 256
             v = 0.877 * (R - y);                   // (-0.615~0.615) * 256
      Y = toByte(y); U = toByte(u / 0.872 + 128); V = toByte(v / 1.230 + 128);
    }
    RGB* toRGB(){
      double y = Y, u = 0.872 * (U - 128), v = 1.230 * (V - 128);
      double r = 1.0 * y - 0.00003 * u + 1.140 * v,
             g = 1.0 * y - 0.394 * u - 0.58 * v,
             b = 1.0 * y + 2.0319 * u - 0.0004 * v;

      return new RGB(toByte(r), toByte(g), toByte(b) );
    }
};

class YIQ{
  public:
    unsigned char Y,I,Q;
    YIQ(unsigned char y, unsigned char i, unsigned char q):Y(y),I(i), Q(q){}
    YIQ(const RGB &pixel){
      fromRGB(pixel);
    }
    void fromRGB(const RGB &pixel){
      double R=pixel.R, G=pixel.G, B=pixel.B;
      double y = 0.299 * R + 0.587 * G + 0.114 * B, // 0~255
             i = 0.596 * R - 0.274 * G - 0.322 * B, // (-0.596 ~ 0.596) * 256
             q = 0.211 * R - 0.523 * G + 0.311 * B; // (-0.523 ~ 0.523) * 256
      Y = toByte(y); I = toByte(i / 1.192 + 128); Q = toByte(q / 1.046 + 128);
    }
    RGB* toRGB(){
      double y = Y, i = 1.192 * (I - 128), q = 1.046 * (Q - 128);
      double r = 1.0 * y + 0.956 * i + 0.621 * q,
             g = 1.0 * y - 0.272 * i - 0.649 * q,
             b = 1.0 * y - 1.106 * i + 1.703 * q;

      return new RGB(toByte(r), toByte(g), toByte(b) );
    }
};

//* the RAWImage class, prividing 
/*
  properties: width, height
  methods:    resize, open, write.
*/

template <class ColorSpace>
class RAWImage{
  public:
    int width, height;
  
  /// constructor, setting the width and height
    RAWImage(int w, int h):width(w),height(h){
      _resize();
    }

    void resize(int w, int h){
      width = w;
      height = h;
      _resize();
    }
    
    void open(const char filename[]){  
      FILE *fin = fopen(filename, "r");
      for(int j=0; j<height; ++j){
        for(int i=0; i<width; ++i){
          _pixelArr[i][j] = new ColorSpace(fgetc(fin),fgetc(fin),fgetc(fin));
        }
      }
      fclose(fin);
    }
    
    void write(const char filename[]){
      FILE *fout = fopen(filename, "w");
      for(int j=0; j<height; ++j){
        for(int i=0; i<width; ++i){
          _pixelArr[i][j]->print(fout);
        }
      }
      fclose(fout);
    }
    
    vector< ColorSpace* >& operator [] (int x){
      return _pixelArr[x];
    }
  private:
    vector < vector < ColorSpace* > > _pixelArr;  
    void _resize(){
      // resize pixelArr using width and height
      _pixelArr.resize(width);
      for(int i=0; i<width; ++i) _pixelArr[i].resize(height, NULL);
    }
};

int main(int argc, char* argv[]){

  RAWImage<RGB> pic(512,512);
  pic.open(argv[1]);

  string filename = strtok(argv[1], "."); // extracting first word before "."  
  FILE *Rout = fopen( (filename + ".R.raw").c_str(), "w"),
       *Gout = fopen( (filename + ".G.raw").c_str(), "w"),
       *Bout = fopen( (filename + ".B.raw").c_str(), "w"),
       *Yout = fopen( (filename + ".Y.raw").c_str(), "w"),
       *Uout = fopen( (filename + ".U.raw").c_str(), "w"),
       *Vout = fopen( (filename + ".V.raw").c_str(), "w"),
       *Iout = fopen( (filename + ".I.raw").c_str(), "w"),
       *Qout = fopen( (filename + ".Q.raw").c_str(), "w");
  
  RAWImage<RGB> pic_from_YUV(512,512), pic_from_YIQ(512,512), 
                pic_diff_YUV(512,512), pic_diff_YIQ(512,512); 
                // RGB -> YUV|YIQ -> RGB
                
  long SE_YUV=0, SE_YIQ=0; // Square error sum for YUV & YIQ

  
  for(int y=0; y<pic.height; ++y){
    for(int x=0; x<pic.width; ++x){  
      RGB pixel = *(pic[x][y]);   // RGB pixel
      fputc(pixel.R, Rout);       //   printing channels to different files
      fputc(pixel.G, Gout);
      fputc(pixel.B, Bout);
      
      YUV pixel2(pixel);          // YUV pixel from RGB pixel
      fputc(pixel2.Y, Yout);
      fputc(pixel2.U, Uout);
      fputc(pixel2.V, Vout);
      
      YIQ pixel3(pixel);          // YIQ pixel from RGB pixel
      fputc(pixel3.I, Iout);
      fputc(pixel3.Q, Qout);

      // convert YIQ and YUV back to RGB
      pic_from_YUV[x][y] = pixel2.toRGB();
      pic_from_YIQ[x][y] = pixel3.toRGB();
      
      // generate diff image
      pic_diff_YUV[x][y] = pixel - *(pic_from_YUV[x][y]);
      pic_diff_YIQ[x][y] = pixel - *(pic_from_YIQ[x][y]);
      
      // calculate sequare error (SE)
      SE_YUV += square( pic_diff_YUV[x][y]->R ) +
                square( pic_diff_YUV[x][y]->G ) + 
                square( pic_diff_YUV[x][y]->B );
  
      SE_YIQ += square( pic_diff_YIQ[x][y]->R ) +
                square( pic_diff_YIQ[x][y]->G ) + 
                square( pic_diff_YIQ[x][y]->B );

      // enchance the diff so that we could see in raw file directly
      pic_diff_YUV[x][y]->R *= 32; pic_diff_YUV[x][y]->G *= 32; pic_diff_YUV[x][y]->B *= 32;
      pic_diff_YIQ[x][y]->R *= 32; pic_diff_YIQ[x][y]->G *= 32; pic_diff_YIQ[x][y]->B *= 32;
    }
  }
  
  // print out RGB -> YUV|YIQ -> RGB
  pic_from_YUV.write( (filename + ".fromYUV.raw").c_str() );
  pic_from_YIQ.write( (filename + ".fromYIQ.raw").c_str() );
  // print out the difference of the image above and the origin RGB file
  pic_diff_YUV.write( (filename + ".diffYUV.raw").c_str() );
  pic_diff_YIQ.write( (filename + ".diffYIQ.raw").c_str() );
  
  // calculate MSE and print it out
  double MSE_YUV = SE_YUV / (512 * 512 * 3),
         MSE_YIQ = SE_YIQ / (512 * 512 * 3);
         
  printf("YUV PSNR: %lf\n", 10 * log10(square(255.0)/MSE_YUV));
  printf("YIQ PSNR: %lf\n", 10 * log10(square(255.0)/MSE_YIQ));
  
  return 0;
}
