------------------------------------------------------------------------
                                 README
------------------------------------------------------------------------

 hw1 (binary file compiled under ubuntu with g++ is included.)

--

 usage: ./hw1 FILE.raw

    A makefile is available with the cpp file. Simply type `make' on a 
  Linux machine should compile the cpp file into the binary file `hw1'. 
  The command `make clean' wipes out the binary file and the output data
  under the directory `testcase', leaving the original raw files intact.

--

 input:

  FILE.raw    the file name (relative path to the file is required, even
            if the raw file is put inside testcase/ directory.) of the 
            input raw file. The raw file MUST be 512x512 RGB interleaved 
            raw image. Except for the dot '.' before the word 'raw', there
            should not be any other dots within the file name or relative
            file path.
              Notice that this parameter is a must-have. If the file does
            not exist or no path is specified, hw1 will end up in 
            segmentation fault. 
--

 output:

    If the file with the file names listed below already exists, 
  it will be overwritten during hw1 execution.

  FILE.R.raw, FILE.G.raw, FILE.B.raw, FILE.Y.raw, FILE.U.raw, FILE.V.raw,
  FILE.I.raw, FILE.Q.raw
              These 8 raw files are the specified output of this homework.

  FILE.fromYUV.raw, FILE.fromYIQ.raw
              These 2 raw files are interleave RGB raw files, generated 
            by converting the original RGB raw file into YUV (or YIQ)
            and back to RGB again.

  FILE.diffYUV.raw, FILE.diffYIQ.raw
              These are the difference between FILE.fromYUV.raw 
            (FILE.fromYIQ.raw) and the original RGB raw file.

  [stdout] YUV PSNR, YIQ PSNR
              The two PSNR values will be printed on the screen.
