------------------------------------------------------------------------
                                 README
------------------------------------------------------------------------

 hw4 (binary file compiled under ubuntu with g++ is included inside bin/)

--

 usage:

 ./hw4 <FILE>
      Process the y-file and output y-files specified in this assignment.
      PSNR information and execution time can also be known.


    A makefile is available for the ease of compilation. To compile the
  program, just do the following:

    $ cd src
    $ make

    Then the executable 'hw4' will appear inside ../bin .

    `make clean' wipes out compiled *.o and the binary file.

    When the 2 test data (stefan.y & weather.y) are put inside bin directory,
  a shell script `doHW4' is available to generate all of the output files. It
  also uses GNU Octave to execute an m-file that prints PSNR graph.

--

 input:

  FILE      the file name of the input y-file.
            The y-file must be an 352x288 CIF grayscale video.

--

 output:

    If the file with the file names listed below already exists,
  it will be overwritten during hw3 execution.

  - filename_{W8|W16|step3}_dif.y filename_{W8|W16}_rec.y
    filename_{W8|W16}_res.y filename_{W8|W16}.y
      As specified in assignment.

  - filename_{W8|W16|step3}.psnr
      A file containing each frame's PSNR.

  - Running time of each motion compensator is written on screen.

