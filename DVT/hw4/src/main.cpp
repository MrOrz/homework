#include <cstring>
#include <cstdio>
#include <climits>
#include <cmath>
#include "rawImage.h"
#include "symbols.h"
#include "stopwatch.h"

#define WIDTH 352
#define HEIGHT 288

#define pixel int

using namespace std;

///////* helper functions *///////

//* printUsage() when input arguments are wrong
void printUsage();

//* PSNR(a, b)
// calculates the PSNR between image a and b.
double PSNR(RAWImage<pixel>& a, RAWImage<pixel>& b);

// 8x8 quantization matrix
const unsigned int Qi[8][8] = {
  {  8, 16, 19, 22, 26, 27, 29, 34},
  { 16, 16, 22, 24, 27, 29, 34, 37},
  { 19, 22, 26, 27, 29, 34, 34, 38},
  { 22, 22, 26, 27, 29, 34, 37, 40},
  { 22, 26, 27, 29, 32, 35, 40, 48},
  { 26, 27, 29, 32, 35, 40, 48, 58},
  { 26, 27, 29, 34, 38, 46, 56, 69},
  { 27, 29, 35, 38, 46, 56, 69, 83}
};

const unsigned int Qp[8][8] = {
  { 16, 16, 16, 16, 16, 16, 16, 16},
  { 16, 16, 16, 16, 16, 16, 16, 16},
  { 16, 16, 16, 16, 16, 16, 16, 16},
  { 16, 16, 16, 16, 16, 16, 16, 16},
  { 16, 16, 16, 16, 16, 16, 16, 16},
  { 16, 16, 16, 16, 16, 16, 16, 16},
  { 16, 16, 16, 16, 16, 16, 16, 16},
  { 16, 16, 16, 16, 16, 16, 16, 16}
};

//* quantization & inverse quantization *//
void quantize (double2& dct, const unsigned int (*)[8]);
void inverse_quantize (double2& dct, const unsigned int (*)[8]);


//* motion_compensate (RawImage& comp, RawImage& buf, RawImage& c, int s) *//
// use full search to create compensated image comp from buf and c.
// @comp : compensated output
// @buf  : buffer frame (reference frame)
// @c    : current frame
// @s    : search range width

void full_search(
  RAWImage<pixel>&,
  const RAWImage<pixel>&,
  const RAWImage<pixel>&,
  const int&
);

//* motion compensation using 3-step search *//
void three_step(
  RAWImage<pixel>&,
  const RAWImage<pixel>&,
  const RAWImage<pixel>&
);

//* motion compensation using hexagon-based search *//
void HEXBS(
  RAWImage<pixel>&,
  const RAWImage<pixel>&,
  const RAWImage<pixel>&
);

/* =================================================================== */
//                              main script
/* =================================================================== */

int main(int argc, char* argv[]){

  // check arguments
  if(argc != 2){
    printUsage();
    return 0;
  }

  // set fullname, filetype and filename, where fullname = filename.filetype
  string fullname = argv[1];
  char* _filetype = strrchr(argv[1], '.');
  string filetype = (_filetype ? _filetype+1 : "");
  if(_filetype) *_filetype = '\0'; // terminates the string argv[2] at '.'
  string filename = argv[1];

  RAWImage<pixel> pic(WIDTH,HEIGHT),         // filename.raw,       INPUT
                  pic_w8_dif(WIDTH,HEIGHT),  // filename_W8_dif.y,  OUTPUT
                  pic_w16_dif(WIDTH,HEIGHT), //   residue.
                  pic_w8_res(WIDTH,HEIGHT),  // filename_W8_res.y   OUTPUT
                  pic_w16_res(WIDTH,HEIGHT), //   reconstructed residue.
                  pic_w8_rec(WIDTH,HEIGHT),  // filename_W8_rec.y   OUTPUT
                  pic_w16_rec(WIDTH,HEIGHT), //   after MC
                  pic_w8(WIDTH,HEIGHT),      // filename_W8.y       OUTPUT
                  pic_w16(WIDTH,HEIGHT);     //   frame buffer

  RAWImage<pixel> pic_3step(WIDTH,HEIGHT),
                  pic_3step_rec(WIDTH,HEIGHT),
                  pic_3step_dif(WIDTH,HEIGHT);

  // psnr graph
  FILE* psnr_out_w8 = fopen( (filename + "_w8.psnr").c_str() , "w" );
  FILE* psnr_out_w16 = fopen( (filename + "_w16.psnr").c_str() , "w" );
  FILE* psnr_out_3step = fopen( (filename + "_3step.psnr").c_str() , "w" );

  // stopwatches
  Stopwatch sw_w8, sw_w16, sw_3step;

  // open raw image files for I/O

  pic.open(fullname, "rb");

  pic_w8.open(filename + "_w8.y", "w");
  pic_w16.open(filename + "_w16.y", "w");

  pic_w8_dif.open(filename + "_w8_dif.y", "w");
  pic_w16_dif.open(filename + "_w16_dif.y", "w");
  pic_3step_dif.open(filename + "_3step_dif.y", "w");

  pic_w8_res.open(filename + "_w8_res.y", "w");
  pic_w16_res.open(filename + "_w16_res.y", "w");

  pic_w8_rec.open(filename + "_w8_rec.y", "w");
  pic_w16_rec.open(filename + "_w16_rec.y", "w");
  pic_3step_rec.open(filename + "_3step_rec.y", "w");

  for(int i=0; pic.read(); ++i){
    bool predictive = i % 16; // whether the frame is P-frame or not

    pic_w8_dif = pic_w16_dif = pic; // copy input img to output img object.
    pic_3step_dif = pic;

    if(predictive) {
      // motion compensation
      sw_w8.start();
      full_search(pic_w8_rec, pic_w8, pic, 8);
      sw_w8.stop(); sw_w16.start();
      full_search(pic_w16_rec, pic_w16, pic, 16);
      sw_w16.stop(); sw_3step.start();
      three_step(pic_3step_rec, pic_3step, pic);
      sw_3step.stop();

      // predictive coding
      pic_w8_dif -= pic_w8_rec;
      pic_w16_dif -= pic_w16_rec;
      pic_3step_dif -= pic_3step_rec;
    }

    pic_w8_dif.write(); pic_w16_dif.write(); pic_3step_dif.write();

    // DCT -> quantize -> inverse quantize
    double2 dct_w8 = pic_w8_dif.DCT(8);
    double2 dct_w16 = pic_w16_dif.DCT(8);
    double2 dct_3step = pic_3step_dif.DCT(8);

    if(predictive) {  // P-frame (inverse) quantization
      quantize(dct_w8, Qp);
      quantize(dct_w16, Qp);
      quantize(dct_3step, Qp);
      inverse_quantize(dct_w8, Qp);
      inverse_quantize(dct_w16, Qp);
      inverse_quantize(dct_3step, Qp);
    } else {          // I-frame (inverse) quantization
      quantize(dct_w8, Qi);
      quantize(dct_w16, Qi);
      quantize(dct_3step, Qi);
      inverse_quantize(dct_w8, Qi);
      inverse_quantize(dct_w16, Qi);
      inverse_quantize(dct_3step, Qi);
    }

    // IDCT
    pic_w8_res.IDCT(dct_w8,8);        pic_w8_res.write();
    pic_w16_res.IDCT(dct_w16,8);      pic_w16_res.write();
    pic_3step_dif.IDCT(dct_3step, 8);

    pic_w8 = pic_w8_res;
    pic_w16 = pic_w16_res;
    pic_3step = pic_3step_dif;

    if(predictive){ // P-frame buffer reconstruction
      pic_w8 += pic_w8_rec;
      pic_w16 += pic_w16_rec;
      pic_3step += pic_3step_rec;
    }

    pic_w8.write(); pic_w16.write();

    if( !predictive ){
      // for i-frames,
      // load buffer image into motion compensation image.
      // (just for writing pic_w*_rec.y)
      pic_w8_rec = pic_w8;
      pic_w16_rec = pic_w16;
      pic_3step_rec = pic_3step;
    } // p-frames' _rec image has been calculated at the beginning of this loop

    pic_w8_rec.write(); pic_w16_rec.write(); pic_3step_rec.write();

    // calculates PSNR of buffer
    fprintf(psnr_out_w8, "%lf ", PSNR(pic, pic_w8));
    fprintf(psnr_out_w16, "%lf ", PSNR(pic, pic_w16));
    fprintf(psnr_out_3step, "%lf ", PSNR(pic, pic_3step));

    // run-time progress prompt
    if( !(i%10) )fprintf(stderr, "%d", i);
    else fprintf(stderr, ".");
  }

  fclose(psnr_out_w8);
  fclose(psnr_out_w16);
  fclose(psnr_out_3step);
  fprintf(stderr, "\n");
  printf("Running time: \n");
  printf("Full search (range = 8):\t%lf us\n", sw_w8.read() );
  printf("Full search (range = 16):\t%lf us\n", sw_w16.read() );
  printf("Three-step search:\t%lf us\n", sw_3step.read() );
  return 0;
}

/* =================================================================== */
//                           helper functions
/* =================================================================== */

// short hand math functions
#define abs(x)    ( (x)>0?(x):-(x) )
#define square(x) ((x)*(x))

double PSNR(RAWImage<pixel>& a, RAWImage<pixel>& b){

  long SE = 0;  // square error

  for(int x=0; x<a.width; ++x)
    for(int y=0; y<a.height; ++y)
      SE += square( *(a[x][y]) - *(b[x][y]) );

  return 10*log10(square(255.0) / (double(SE)/(a.width * a.height) ));
}

void quantize (double2& dct, const unsigned int (*Q)[8]){
  int w = dct.size(), h = dct[0].size();

  for(int x=0; x<w; ++x)
    for(int y=0; y<h; ++y)
      dct[x][y] = round( dct[x][y] / Q[x%8][y%8] );
}

void inverse_quantize (double2& dct, const unsigned int (*Q)[8]){
  int w = dct.size(), h = dct[0].size();

  for(int x=0; x<w; ++x)
    for(int y=0; y<h; ++y)
      dct[x][y] *= Q[x%8][y%8];
}

/// helper function for motion compensation, returns the pixel for a given x,y
// if x, y is out of range, the nearest border pixel is returned.
// (similar to H.263 unrestricted motion vector mode)
//
inline pixel& get_pixel(const RAWImage<pixel>& img, int x, int y){
  int w = img.width, h = img.height;
  if(x < 0) x = 0;
  else if(x >= w) x = w-1;

  if(y < 0) y = 0;
  else if(y >= h) y = h-1;

  return *img[x][y];
}

/// helper function for motion compensation, returns the SSD for a block
//  buffer frame will be extended if vec <i,j> points outside the image.
//
inline unsigned int SSD(
  const RAWImage<pixel>& current,
  const RAWImage<pixel>& buffer,
  const int& m, const int& n, // block position
  const int& i, const int& j, // candidate motion vector <i,j>
  const unsigned int& existed_min      // minimal SSD found before
  ){

  // calculate SSD
  unsigned int ssd = 0;
  for(int k = 0; k < 8; ++k)
  for(int l = 0; l < 8; ++l){ // each pixel within the block
    int x = 8*m+k, y = 8*n+l;
    int dif = *(current[x][y]) - get_pixel(buffer, x+i, y+j);
    ssd += square(dif);
    if(ssd > existed_min) // SSD already too big
      return ssd;
      // it can't be better than existed min position; abort calculation
  }
  // end of each pixel within block

  return ssd;
}

/// helper function for motion compensation,
//  given the buffer and the MV found previously, fill out the block of comp
//  ("patch" the block inside buffer onto a block in comp)
//
inline void patch( RAWImage<pixel>& comp, const RAWImage<pixel>& buffer,
  const int& m, const int& n, const int& mv_x, const int& mv_y ){

  for(int k = 0; k < 8; ++k)
  for(int l = 0; l < 8; ++l){ // each pixel within the block
    int x = 8*m+k, y = 8*n+l;
    delete comp[x][y];
    comp[x][y] = new pixel( get_pixel(buffer, x + mv_x, y + mv_y) );
  }
  // end of each pixel within the block

}

/// motion compensation w/ full search
//
void full_search(
  RAWImage<pixel>& comp,
  const RAWImage<pixel>& buffer,
  const RAWImage<pixel>& current,
  const int& search_range
){
  int w = comp.width, h = comp.height;

  for(int m = 0; m < w/8; ++m)
  for(int n = 0; n < h/8; ++n){ // each block
    int mv_x, mv_y; // MV for this block;
    unsigned int min_SSD = UINT_MAX; // minimal SSD

    for(int i = -search_range; i < search_range; ++i)
    for(int j = -search_range; j < search_range; ++j){ // each candidate position

      unsigned int ssd = SSD(current, buffer, m,n,i,j,min_SSD);

      if(ssd < min_SSD){ // a better position is found
        min_SSD = ssd;
        mv_x = i;
        mv_y = j;
      }
    }
    // end of each candidate position

    // build motion compensated image
    // using the motion vector <mv_x, mv_y> we found.
    patch(comp, buffer, m, n, mv_x, mv_y);
  }
  // end of each block

}

/// motion compensation w/ 3-step search
//
void three_step(
  RAWImage<pixel>& comp,
  const RAWImage<pixel>& buffer,
  const RAWImage<pixel>& current
){
  int w = comp.width, h = comp.height;

  for(int m = 0; m < w/8; ++m)
  for(int n = 0; n < h/8; ++n){ // each block
    int mv_x = 0, mv_y = 0; // MV for this block;
    int dx, dy;             // the candidate that has min SSD
    unsigned int min_SSD = UINT_MAX; // minimal SSD

    for(int range = 4; range > 0; range /= 2){ // range = 4,2,1
      for(int i = mv_x-range; i <= mv_x+range; i+=range)
      for(int j = mv_y-range; j <= mv_y+range; j+=range){ // each candidate position

        // calculate SSD
        unsigned int ssd = SSD(current, buffer, m,n,i,j,min_SSD);

        if(ssd < min_SSD){ // a better position is found
          min_SSD = ssd;
          dx = i;
          dy = j;
        }
      }
      // end of each candidate position

      mv_x = dx;
      mv_y = dy;
    }

    // build motion compensated image
    // using the motion vector <mv_x, mv_y> we found.
    patch(comp, buffer, m, n, mv_x, mv_y);

  }
  // end of each block

}


void printUsage(){
  printf("usage: hw4 <filename>\n");
}

