#ifndef __SYMBOLS__
#define __SYMBOLS__

#include <vector>

using namespace std;

//* double2
// alias for 2-D double array 
typedef vector<vector<double> > double2;

//* RLCSymbol
// RLC Symbol (run, amplitude)

class RLCSymbol{
  public:
    unsigned run;   // 0-run length
    unsigned size;  // size (binary length) of amplitude
    int amplitude;
    RLCSymbol(unsigned c, int v):run(c),amplitude(v){
      // calculate size of amplitude
      if(v < 0) v *= -1;
      for(size=0; v > 0; v/=2, ++size) continue;
    }
    RLCSymbol(unsigned r, unsigned s, int a):run(r),size(s),amplitude(a){}
};
#endif
