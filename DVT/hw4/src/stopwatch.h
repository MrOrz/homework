// **************************************************************************
// File       [stopwatch.h]
// Author     [MrOrz]
// **************************************************************************

#ifndef _STOPWATCH_H_
#define _STOPWATCH_H_

#include <sys/time.h>
using namespace std;

/*
** Definitions are so short that it does not require .cpp file to define its
** member functions.
*/

class Stopwatch{
  public:
  Stopwatch():_accumulated_us(0){}

  void start(){
    gettimeofday(&_started_time, NULL);
  }

  // return the time elapsed from started (in sec)
  void stop(){
    gettimeofday(&_ended_time, NULL);
    _accumulated_us += (_ended_time.tv_sec - _started_time.tv_sec) * 1000000
                     + _ended_time.tv_usec - _started_time.tv_usec;
  }
  double read(){
    return _accumulated_us * 0.000001;
  }
  private:
    struct timeval _started_time;
    struct timeval _ended_time;
    unsigned long long _accumulated_us;
};

#endif

