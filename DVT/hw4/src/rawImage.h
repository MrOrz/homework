#ifndef __RAWIMAGE__
#define __RAWIMAGE__

#include <string>
#include <vector>
#include <cstdio>
#include <cmath>

using namespace std;

//* RAWImage class
/*
  attributes: width, height
  methods:    resize, open, write, DCT, IDCT

  uses with ColorSpace set to unsigned char.
*/

template <class ColorSpace>
class RAWImage{
  public:
    //* public data member: width and height
    //
    int width, height;

    //* RAWImage(width, height)
    // constructor, setting the width and height
    //
    RAWImage(int, int);
    ~RAWImage();

    //* resize(width, height)
    // resize to new width and height
    //
    void resize(int,int);

    //* open(filename, "r|w")
    // Open raw image from file, using width and height setting as the public
    // data member 'width' and 'height'
    // 'r' indicates the image is for read from the file,
    // and 'w' indicates the file is going to be written.
    // Currently, only images of 1 byte/pixel (ColorSpace = unsigned char)
    // is supported.
    //
    void open(const string, const char*);
    void open(const char [], const char*);

    //* close(filename)
    // close opened file.
    //
    void close();

    //* read()
    // fill the image from the file.
    // returns false if EOF is reached.
    bool read();

    //* write()
    // write raw image to file.
    //
    void write();

    //* operator []
    // enables accessing pixels with []s.
    //
    // example:
    //   RAWImage<unsigned char> a(512,512);
    //   a.open("foo.raw");
    //   cout << a[0][3];   // prints the pixel
    //
    vector< ColorSpace* >& operator [] (int);
    const vector< ColorSpace* >& operator [] (int) const;

    //* operator =
    // copying frames.
    // notice that the pixels of the LHS of '=' are destructed (freed)
    // before assigned the new pixel value, thus the operation a = a
    // is prohibited.
    //
    RAWImage< ColorSpace >& operator = ( const RAWImage< ColorSpace >& );

    //* operator -=, operator +=
    // substract or add pixel-by-pixel
    //
    RAWImage< ColorSpace >& operator -= ( const RAWImage< ColorSpace >& );
    RAWImage< ColorSpace >& operator += ( const RAWImage< ColorSpace >& );

    //* DCT(block size)
    // perform IDCT, and returns a 2-D vector of DCT coefficients in double
    // when no block size is assigned, block size will be set to image width.
    //
    vector< vector< double > > DCT(int = -1);

    //* IDCT(coefficient array, block size)
    // perform IDCT, and stores the reconstructed pixels within itself.
    // The width and height must be set before performing IDCT().
    // when no block size is assigned, block size will be set to image width.
    //
    void IDCT( vector< vector< double > >, int=-1);

  private:
    vector < vector < ColorSpace* > > _pixel; // the pixels.
    void _resize(); // resize pixelArr using width and height

    inline double C(const int& u,const int& N) const{ // DCT & IDCT coefficient
      return u==0 ? 1.0/sqrt(N) : sqrt(2.0/N);
    }

    FILE *_file;

    static const double pi;              // pi
};

/* ===== helper functions ===== */

/* saturates the absolute value of pixel into 0~255 */
inline unsigned char toByte(double value){
  if(value < 0)value *= -1;
  if(value>255.0)   value = 255.0;
//  else if(value<0)  value = 0.0;
  return round(value);
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//                                Implementation
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------


/* ===== public methods (member functions) ===== */

/// constructor, setting the width and height
template <class ColorSpace>
RAWImage<ColorSpace>::RAWImage(int w, int h):width(w),height(h),_file(NULL){
  _resize();
}

template <class ColorSpace>
RAWImage<ColorSpace>::~RAWImage(){
  for(int i=0; i<width; ++i)
    for(int j=0; j<height; ++j){
      if(_pixel[i][j] != NULL)
        delete _pixel[i][j]; // free the new-ed Colorspace*
    }


}
template <class ColorSpace>
void RAWImage<ColorSpace>::resize(int w, int h){
  width = w;
  height = h;
  _resize();
}

template <class ColorSpace>
void RAWImage<ColorSpace>::open(const string filename, const char rw[]){
  open(filename.c_str(), rw);
}
template <class ColorSpace>
void RAWImage<ColorSpace>::open(const char filename[], const char rw[]){
  _file = fopen(filename, rw);
}


template <class ColorSpace>
bool RAWImage<ColorSpace>::read(){
  unsigned char* buf = new unsigned char[width*height];

  fread(buf, sizeof(unsigned char), width*height, _file);
  if(feof(_file))return false;

  int buf_index = 0;
  for(int j=0; j<height; ++j){
    for(int i=0; i<width; ++i){
      delete _pixel[i][j];
      _pixel[i][j] = new ColorSpace(buf[buf_index++]);
    }
  }
  return true;
}


template <class ColorSpace>
void RAWImage<ColorSpace>::write(){
  unsigned char* buf = new unsigned char[width*height]; // continuous memory allocation
                                                        // for fwrite
  int buf_index = 0;
  for(int j=0; j<height; ++j){
    for(int i=0; i<width; ++i){
      buf[buf_index++] = toByte( *(_pixel[i][j]) );
    }
  }
  fwrite(buf, sizeof(unsigned char), width*height, _file );

  delete[] buf;
}

template <class ColorSpace>
void RAWImage<ColorSpace>::close(){
  fclose(_file);
}

template <class ColorSpace>
vector< ColorSpace* >& RAWImage<ColorSpace>::operator [] (int x){
  return _pixel[x];
}

template <class ColorSpace>
const vector< ColorSpace* >& RAWImage<ColorSpace>::operator [] (int x) const{
  return _pixel[x];
}


template <class ColorSpace>
RAWImage< ColorSpace >& RAWImage<ColorSpace>::operator =
  (const RAWImage< ColorSpace >& x){
  for(int j=0; j<height; ++j){
    for(int i=0; i<width; ++i){
      delete _pixel[i][j];
      _pixel[i][j] = new ColorSpace(*x[i][j]);
    }
  }
  return *this;
}

template <class ColorSpace>
RAWImage< ColorSpace >& RAWImage<ColorSpace>::operator +=
  (const RAWImage< ColorSpace >& x){
  for(int j=0; j<height; ++j){
    for(int i=0; i<width; ++i){
      *_pixel[i][j] += *( x[i][j] ) ;
    }
  }
  return *this;
}

template <class ColorSpace>
RAWImage< ColorSpace >& RAWImage<ColorSpace>::operator -=
  (const RAWImage< ColorSpace >& x){
  for(int j=0; j<height; ++j){
    for(int i=0; i<width; ++i){
      *_pixel[i][j] -= *( x[i][j] ) ;
    }
  }
  return *this;
}


template <class ColorSpace>
vector< vector< double > > RAWImage<ColorSpace>::DCT(int N){
  // N = block width
  if(N==-1) N = width; // block size in default = width of the whole image
  vector< vector< double > > dct,   // the dct 2-d array of first 1-D DCT
                             dct2;  // the dct 2-d array to be returned

  dct.resize(width);              // init: resize to the size of the image
  dct2.resize(width);
  for(int i=0; i < width; ++i){
    // avoid overhead resize() call in the following calculation
    dct[i].resize(height, 0.0);
    dct2[i].resize(height, 0.0);
  }


  //----- 1st 1-D DCT -----
  for(int offx = 0; offx < width; offx+=N)    // block offset x,y
  for(int offy = 0; offy < height; offy+=N)
  for(int u=0; u<N; ++u){   // for each dct pixel in the block
    for(int v=0; v<N; ++v){
      double sum = 0.0;

      // sigma m=0->N
      for(int m=0;m<N;++m)  // 1-D DCT of horizontal
        sum += *(_pixel[m+offx][v+offy]) * cos(pi * u * (2*m + 1) / (2 * N));

      dct[u+offx][v+offy] = C(u, N) * sum;
    }
  }

  //----- 2nd 1-D DCT ------
  for(int offx = 0; offx < width; offx+=N)    // block offset x,y
  for(int offy = 0; offy < height; offy+=N)
  for(int u=0; u<N; ++u){ // for each dct pixel in the block
    for(int v=0; v<N; ++v){
      double sum = 0.0;

      // sigma m=0->N
      for(int m=0;m<N;++m)  // 1-D DCT of vertical
        sum += dct[u+offx][m+offy] * cos(pi * v * (2*m + 1) / (2 * N));

      dct2[u+offx][v+offy] = C(v, N) * sum;
    }
  }
  return dct2;
}

template <class ColorSpace>
void RAWImage<ColorSpace>::IDCT(
  vector< vector< double > > y,   // y = 2D DCT coefficients
  int N){                         // N = block width

  if(N==-1) N = width; // block size in default = width of the whole image
  vector< vector< double > > x;   // the idct 2-d array of first 1-D IDCT

  x.resize(width);              // init: resize to the size of the image
  for(int i=0; i < width; ++i){
    // avoid overhead resize() call in the following calculation
    x[i].resize(height, 0.0);
  }

  //----- 1st 1-D IDCT -----
  for(int offx = 0; offx < width; offx+=N)    // block offset x,y
  for(int offy = 0; offy < height; offy+=N)
  for(int u=0; u<N; ++u){   // for each dct pixel in the block
    for(int v=0; v<N; ++v){

      // sigma k = 0->N-1
      for(int k=0;k<N;++k)  // 1-D DCT of horizontal
        x[u+offx][v+offy] += C(k,N) * y[k+offx][v+offy] *
          cos(pi * k * (2*u + 1) / (2 * N));
    }
  }

  //----- 2nd 1-D IDCT ------
  for(int offx = 0; offx < width; offx+=N)    // block offset x,y
  for(int offy = 0; offy < height; offy+=N)
  for(int u=0; u<N; ++u){ // for each dct pixel in the block
    for(int v=0; v<N; ++v){
      double sum = 0.0;

      // sigma k = 0->N-1
      for(int k=0;k<N;++k)  // 1-D DCT of vertical
        sum += C(k, N) * x[u+offx][k+offy] * cos(pi * k * (2*v + 1) / (2 * N));

      delete _pixel[u+offx][v+offy];
//      _pixel[u+offx][v+offy] = new ColorSpace (toByte(sum));
      _pixel[u+offx][v+offy] = new ColorSpace (sum);
    }
  }
}


/* ===== private methods (member functions) ===== */
template <class ColorSpace>
void RAWImage<ColorSpace>::_resize(){ // resize pixel using width and height
  _pixel.resize(width);
  for(int i=0; i<width; ++i) _pixel[i].resize(height, NULL);
}

template <class ColorSpace>
const double RAWImage<ColorSpace>::pi = acos(-1.0);

#endif

