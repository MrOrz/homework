#include<iostream>

using namespace std;

int main(){

  int xoff = 0, yoff = 0;
  int xstep = 1;
  for(int sum=1;sum<=14; ++sum){  // sum = x + y
    int x = (sum < 8? 0:sum-7); // upper or lower triangle. y = sum-x.
    for(x = (sum%2==1?x:sum-x); // sum is odd => scans bottom-left to top-right
      0 <= x && x < 8 && 0 <= sum-x && sum-x < 8; // 0 <= x, y <= 8
      x += xstep)
      cout << "(" << xoff + x << "," << yoff + sum-x << ") "; // zigzag body

    cout << endl;
    xstep *= -1;
  }
  
  return 0;
}
