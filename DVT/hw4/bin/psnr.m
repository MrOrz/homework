files.a = "stefan";
files.b = "weather";


for [i, key] = files
  filename = strcat(i,"_w8");
  load(strcat(filename ,".psnr"));
  plot( eval(filename), strcat("r;", filename , ";") ); hold on;

  filename = strcat(i,"_w16");
  load(strcat(filename ,".psnr"));
  plot( eval(filename), strcat("g;", filename , ";") ); hold on;

  filename = strcat(i,"_3step");
  load(strcat(filename ,".psnr"));
  plot( eval(filename), strcat("b;", filename , ";") ); hold off;

  xlabel("frame #");
  ylabel("PSNR (dB)");
  print( strcat(i, "_psnr.eps"), "-color","-deps");

endfor

