------------------------------------------------------------------------
                                 README
------------------------------------------------------------------------

 hw2 (binary file compiled under ubuntu with g++ is included.)

--

 usage: ./hw2 FILE

    A makefile is available with the cpp file. Simply type `make' on a 
  Linux machine should compile the cpp file into the binary file `hw2'. 
  The command `make clean' wipes out the binary file and the output data
  under the current directory, leaving the original raw files intact.

--

 input:

  FILE       the file name of the input raw file. The raw file MUST be a
            64x64 grayscale raw image.
              Notice that this parameter is a must-have. If the file does
            not exist or no path is specified, hw1 will end up in 
            segmentation fault. 
--

 output:

    If the file with the file names listed below already exists, 
  it will be overwritten during hw2 execution.

  dct_FILE.raw, r3_FILE.raw, r4_FILE.raw, r5_FILE.raw, r6_FILE.raw
              These 5 raw files are the specified output of this homework.

  2Ddct_FILE.raw, blockdct_FILE.raw
              These 2 raw files are file showing the DCT coefficients

  r?diff_FILE.raw
              These are the difference between r?_FILE.raw and the original              raw file.

  [stdout] r? PSNR 
              The PSNR values between r?_FILE.raw and FILE.raw will be 
              printed on the screen.
  [stdout] 2D DCT, 1D DCT, Block DCT running time
  [stdout] bits required for r5
