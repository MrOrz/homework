#include<iostream>
#include<string>
#include<cstring>
#include<cstdio>
#include<cmath>
#include<vector>
#include<time.h>

#define abs(x)    ( (x)>0?(x):-(x) )
#define square(x) ((x)*(x))
#define C(u, N)   (u==0 ? 1.0/sqrt(N) : sqrt(2.0/N))
#define pi        (acos(-1.0))

using namespace std;

/* helper function */

/* saturates the value into 0~255 */
inline unsigned char toByte(double value){
  if(value>255.0)   value = 255.0;
  else if(value<0)  value = 0.0;
  return round(value);
}


//* the RAWImage class, prividing 
/*
  properties: width, height
  methods:    resize, open, write.
*/

template <class ColorSpace>
class RAWImage{
  public:
    int width, height;
  
  /// constructor, setting the width and height
    RAWImage(int w, int h):width(w),height(h){
      _resize();
    }

    ~RAWImage(){
      for(int i=0; i<width; ++i)
        for(int j=0; j<height; ++j){
          if(_pixelArr[i][j] != NULL)
            delete _pixelArr[i][j]; // free the new-ed Colorspace*    
        }
    }
    
    void resize(int w, int h){
      width = w;
      height = h;
      _resize();
    }
    void open(const string filename){
      open(filename.c_str());
    }
    void open(const char filename[]){  
      FILE *fin = fopen(filename, "r");
      for(int j=0; j<height; ++j){
        for(int i=0; i<width; ++i){
          _pixelArr[i][j] = new ColorSpace(fgetc(fin));
        }
      }
      fclose(fin);
    }
    void write(const string filename){
      write(filename.c_str());
    }
    void write(const char filename[]){
      FILE *fout = fopen(filename, "w");
      for(int j=0; j<height; ++j){
        for(int i=0; i<width; ++i){
          fputc(*(_pixelArr[i][j]), fout);
        }
      }
      fclose(fout);
    }
    
    vector< ColorSpace* >& operator [] (int x){
      return _pixelArr[x];
    }
    
    vector< vector< double > > twoDDCT(int N = -1){ // N = block width
      if(N==-1) N = width; // block size in default = width of the whole image
           
      vector< vector< double > > dct; // the dct 2-d array to be returned
      dct.resize(width);              // init: resize to the size of the image
      for(int i=0; i<height; ++i) dct[i].resize(height, 0.0); // to avoid overhead resize
      
      for(int u=0;u<N; ++u){  // for each dct pixel in the block
        for(int v=0; v<N; ++v){
          double sum = 0.0;
          // sigma sigma (x,y) = (0,0)->(N-1,N-1)
          for(int x=0; x<N; ++x)
            for(int y=0;y<N; ++y)
               sum += *(_pixelArr[x][y]) * cos(pi*(2*x+1)*u/(2*N)) * cos(pi*(2*y+1)*v/(2*N));
          // http://www.mathworks.com/help/toolbox/signal/dct.html          
          dct[u][v] = C(u, N) * C(v, N) * sum;
        }
      }
      return dct;
    }
    
    vector< vector< double > > DCT(int N=-1){ // N = block width
      if(N==-1) N = width; // block size in default = width of the whole image
      vector< vector< double > > dct,   // the dct 2-d array of first 1-D DCT
                                 dct2;  // the dct 2-d array to be returned

      dct.resize(width);              // init: resize to the size of the image
      dct2.resize(width);
      for(int i=0; i<height; ++i){
        // avoid overhead resize() call in the following calculation
        dct[i].resize(height, 0.0);
        dct2[i].resize(height, 0.0);
      } 

      
      //----- 1st 1-D DCT -----
      for(int offx = 0; offx < width; offx+=N)    // block offset x,y
      for(int offy = 0; offy < height; offy+=N)
      for(int u=0; u<N; ++u){   // for each dct pixel in the block
        for(int v=0; v<N; ++v){
          double sum = 0.0;
          
          // sigma m=0->N
          for(int m=0;m<N;++m)  // 1-D DCT of horizontal
            sum += *(_pixelArr[m+offx][v+offy]) * cos(pi * u * (2*m + 1) / (2 * N));
            
          dct[u+offx][v+offy] = C(u, N) * sum;
        }
      }
      
      //----- 2nd 1-D DCT ------      
      for(int offx = 0; offx < width; offx+=N)    // block offset x,y
      for(int offy = 0; offy < height; offy+=N)
      for(int u=0; u<N; ++u){ // for each dct pixel in the block
        for(int v=0; v<N; ++v){   
          double sum = 0.0;
          
          // sigma m=0->N
          for(int m=0;m<N;++m)  // 1-D DCT of vertical
            sum += dct[u+offx][m+offy] * cos(pi * v * (2*m + 1) / (2 * N));
            
          dct2[u+offx][v+offy] = C(v, N) * sum;
        }
      }
      return dct2;
    }
    
    void IDCT( vector< vector< double > > y,  // y = 2D DCT coefficients
               int N = -1 ){                  // N = block width
      
      if(N==-1) N = width; // block size in default = width of the whole image
      vector< vector< double > > x;   // the idct 2-d array of first 1-D IDCT

      x.resize(width);              // init: resize to the size of the image
      for(int i=0; i<height; ++i){
        // avoid overhead resize() call in the following calculation
        x[i].resize(height, 0.0);
      }

      //----- 1st 1-D IDCT -----
      for(int offx = 0; offx < width; offx+=N)    // block offset x,y
      for(int offy = 0; offy < height; offy+=N)
      for(int u=0; u<N; ++u){   // for each dct pixel in the block
        for(int v=0; v<N; ++v){
          
          // sigma k = 0->N-1
          for(int k=0;k<N;++k)  // 1-D DCT of horizontal
            x[u+offx][v+offy] += C(k,N) * y[k+offx][v+offy] *
              cos(pi * k * (2*u + 1) / (2 * N));
        }
      }
      
      //----- 2nd 1-D IDCT ------      
      for(int offx = 0; offx < width; offx+=N)    // block offset x,y
      for(int offy = 0; offy < height; offy+=N)
      for(int u=0; u<N; ++u){ // for each dct pixel in the block
        for(int v=0; v<N; ++v){   
          double sum = 0.0;
          
          // sigma k = 0->N-1
          for(int k=0;k<N;++k)  // 1-D DCT of vertical
            sum += C(k, N) * x[u+offx][k+offy] * cos(pi * k * (2*v + 1) / (2 * N));
            
          _pixelArr[u+offx][v+offy] = new unsigned char (toByte(sum));
        }
      }
    }
  private:
    vector < vector < ColorSpace* > > _pixelArr;  
    void _resize(){
      // resize pixelArr using width and height
      _pixelArr.resize(width);
      for(int i=0; i<width; ++i) _pixelArr[i].resize(height, NULL);
    }
};

/* helper function using RAWImage class */
/* prints diff image between a and b, then returns PSNR */
double printDiff(const char* filename, 
  RAWImage<unsigned char>& a, RAWImage<unsigned char>& b){

  long SE = 0;  // squeare error
  RAWImage<unsigned char> diff(a.width, a.height);

  for(int x=0; x<a.width; ++x)
    for(int y=0; y<a.height; ++y){
      diff[x][y] = new unsigned char( abs( *(a[x][y]) - *(b[x][y]) ) );
      SE += square( *diff[x][y] );
      *diff[x][y] = toByte( *diff[x][y] * 10 );
      // diff graph with intensity gain 10
    }
  diff.write(filename);
  return 10*log10(square(255.0) / (double(SE)/(a.width * a.height) ));
}

double printDiff(string filename, // string version.
  RAWImage<unsigned char>& a, RAWImage<unsigned char>& b){
  return printDiff(filename.c_str(), a, b);
}
/* =================================================================== */
//                              main script
/* =================================================================== */

int main(int argc, char* argv[]){

  RAWImage<unsigned char> pic(64,64);
  pic.open(argv[1]);

  string filename = argv[1]; // extracting first word before "."    
  printf("Input file: %s\n", filename.c_str());

  // ---------- problem 2 ----------
  {
    // profiling twoDDCT and DCT
    clock_t start_clk = clock();
    vector< vector< double > > dct2D = pic.twoDDCT();
    printf("2D DCT: %lf sec | ", (double)(clock() - start_clk)/CLOCKS_PER_SEC );
    
    start_clk = clock();
    vector< vector< double > > dct1D = pic.DCT();
    printf("1D DCT: %lf sec\n", (double)(clock() - start_clk)/CLOCKS_PER_SEC );
    
    // output the coefficients to file
    RAWImage<unsigned char> p22D(64, 64);
    RAWImage<unsigned char> p21D(64, 64);
    for(int y=0; y<pic.height; ++y){
      for(int x=0; x<pic.width; ++x){
        p22D[x][y] = new unsigned char( toByte(dct2D[x][y] + 128.0) );
        p21D[x][y] = new unsigned char( toByte(dct1D[x][y] + 128.0) );
      }
    }
    p22D.write("2Ddct_" + filename);
    p21D.write("dct_" + filename);
  }
  // ---------- problem 3 ----------
  {
    vector< vector< double > > dct = pic.DCT();
    RAWImage<unsigned char> idct(64,64);
    idct.IDCT(dct);
    idct.write("r3_" + filename);

    printf("r3 PSNR = %lf\n", 
           printDiff("r3diff_" + filename, pic, idct)) ;

  }
  // ---------- problem 4 & 5 ------------
  {
    vector< vector< double > > dct = pic.DCT();
    long dc_offset = 0, ac_offset = 0;
    long dc_threshold = 1<<15,  // 15 bits numeric data
         ac_threshold = 1<<7;   // 7 bits numeric data
    // calculate DC offset
    for( long dc = abs(dct[0][0]); // use abs() to get rid of sign (+/-)
         dc > dc_threshold;        // if there is still more space to shift
         dc >>= 1, ++dc_offset)     // do the shifting and update dc_offset
      continue;
    // truncate DC value with the calculated dc_offset.
    dct[0][0] = (long(dct[0][0]) / (1<<dc_offset)) * (1<<dc_offset);

    // calculate AC offset
    for(int x=0; x< pic.width; ++x)
      for(int y=(x==0?1:0); y<pic.height; ++y){
        long ac = abs(dct[x][y]);
        for( ac >>= ac_offset;   // first, shift ac with current ac_offset
             ac > ac_threshold; // if there is still more space to shift
             ac >>= 1, ++ac_offset)  // do the shifting and update ac_offset
          continue;
      }
    // truncate AC value with the calculated ac_offset.
    for(int x=0; x< pic.width; ++x)
      for(int y=(x==0?1:0); y<pic.height; ++y){
        dct[x][y] = (long(dct[x][y]) / (1<<ac_offset)) * (1<<ac_offset);
      }

    // inverse transform, and output the image
    RAWImage<unsigned char> idct(64,64);
    idct.IDCT(dct);
    idct.write("r4_" + filename );

    // print diff and PSNR
    printf("r4 PSNR = %lf\n", 
           printDiff("r4diff_" + filename ,pic, idct) );

    // --------------------------------------------------

    long saved_bits = 0;    
    // set AC values ranging in -7~7 to 0    
    for(int x=0; x< pic.width; ++x)
      for(int y=(x==0?1:0); y<pic.height; ++y){
        if(dct[x][y] < (7 << ac_offset) && dct[x][y] > -(7 << ac_offset) ){
          dct[x][y] = 0;
          saved_bits += 8; // for each coefficient set to 0, 8 bits is saved
        }
      }

    // inverse transform, and output the image
    idct.IDCT(dct);
    idct.write("r5_" + filename);

    // print diff and PSNR
    printf("r5 PSNR = %lf\n %ld bits required.\n", 
           printDiff( "r5diff_" + filename ,pic, idct),
           16 + 8 * (64*64-1)-saved_bits);
  }
  // ------------- problem 6 -----------------
  {
    clock_t start_clk = clock();
    vector< vector< double > > dct = pic.DCT(8); // block size indicated
    printf("Block DCT: %lf sec\n", (double)(clock() - start_clk)/CLOCKS_PER_SEC );

    RAWImage <unsigned char> p6(64,64), r6(64,64);

    for(int y=0; y<pic.height; ++y)
      for(int x=0; x<pic.width; ++x)
        p6[x][y] = new unsigned char( toByte(dct[x][y] + 128.0) );
    p6.write("blockdct_" + filename);
    
    r6.IDCT(dct, 8);
    r6.write("r6_" + filename);

    // print diff and PSNR
    printf("r6 PSNR = %lf\n", 
           printDiff( "r6diff_" + filename ,pic, r6));
  }
  return 0;
}
