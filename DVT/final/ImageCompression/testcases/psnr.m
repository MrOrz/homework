function psnr(orig, comp)
  img_orig = imread(orig);
  img_comp = imread(comp);
  
  [height, width, color] = size(img_orig);
  MSE_R = sum( sum((img_orig(:,:,1) - img_comp(:,:,1) ).^2 ) ) / (width * height);
  MSE_G = sum( sum((img_orig(:,:,2) - img_comp(:,:,2) ).^2 ) ) / (width * height);
  MSE_B = sum( sum((img_orig(:,:,3) - img_comp(:,:,3) ).^2 ) ) / (width * height);
  
  PSNR_R = 10*log10(255^2/MSE_R)
  PSNR_G = 10*log10(255^2/MSE_G)
  PSNR_B = 10*log10(255^2/MSE_B)
  
  %printf("R,G,B PSNR = %lf, %lf, %lf\n", PSNR_R, PSNR_G, PSNR_B);
    
end