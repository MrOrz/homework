#ifndef __WAVELET__
#define __WAVELET__

#include "bitmap.h"

class WaveletCodec {
  public:
    WaveletCodec(size_t width)
      :_w(width), _dc_count(width), _coeff(new double[width]){
      while(_dc_count % 2 == 0) _dc_count /= 2;
    }
    ~WaveletCodec(){
      delete[] _coeff;
    }

    // encode(encoded output, Pixels to be encoded)
    // put wavelet coefficients into encoded output.
    // both input and output array size are assumed to be _w.
    //
    void encode(double*, const Pixel*);

    // decode(buf, coefficients)
    // Pixels decoded from the coefficients will be put in buf.
    // notice that the coefficients will be altered by decode()
    //
    void decode(Pixel*, double*);

  private:
    size_t _w;
    size_t _dc_count;
    double* _coeff;   // buffer for encoding & decoding
};

#endif // __WAVELET__
