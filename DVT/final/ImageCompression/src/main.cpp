#include <cstdio>
#include <cstdlib>
//#include "tests.h"
#include "codec.h"

void print_usage(){
  #ifdef COMPILE_ENCODER
  printf("usage: Encoder.exe input.bmp output_bitstream_name\n");
  #else
  printf("usage: Decoder.exe input_bitstream_name output.bmp\n");
  #endif // COMPILE_ENCODER
  exit(0);
}

int main(int argc, char * argv[]){
#ifdef COMPILE_ENCODER
  if(argc != 3) print_usage();
  printf("%s: ",argv[0]);
  encode(argv[1], argv[2]);
#else
#ifdef COMPILE_DECODER
  if(argc != 3) print_usage();
  printf("%s: ",argv[0]);
  decode(argv[1], argv[2]);
#else // test builds
/*
  Pixel arr[1920] = {0};
  arr[35] = 1;
  arr[4] = 2;
  _circ_shift(arr, -5);
  printf("%d%d%d %d%d%d", arr[29], arr[30], arr[31], arr[1917], arr[1918], arr[1919]);
*/
  //printf("%u\n", toByte(1000) );
/*
  char* bin =  "testcases/image053.bmp";
  char* f =    "testcases/image053.n.output" ;
  char* bout = "testcases/image053.n.output.bmp";
  //encoder_test(bin, f);
  encode(bin, f);
  //printf("\n");
  //decoder_test(f, bout);
  decode(f, bout);
// */
  //diff_detection_test();
#endif // COMPILE_DECODER
#endif // COMPILE_ENCODER
  return 0;
}
