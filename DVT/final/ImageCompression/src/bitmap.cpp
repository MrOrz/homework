#include <cstdio>
#include <cmath>
#include "bitmap.h"



/* struct for reading one RGB pixel from bmp file */

class RGB; class RGBA;

class RGB { // 24 bits per pel
  public:
    Pixel b;
    Pixel g;
    Pixel r;
    RGB& operator = (const RGBA& p);
};

class RGBA{ // 32 bits per pel
  public:
    Pixel b;
    Pixel g;
    Pixel r;
    Pixel a;
    RGBA& operator = (const RGB& p);
};

RGB& RGB::operator = (const RGBA& p){
  b = p.b; g = p.g; r = p.r;
  return *(this);
}

RGBA& RGBA:: operator = (const RGB& p){
  b = p.b; g = p.g; r = p.r; a = 0;
  return *(this);
}


bool Bitmap::open(const string& filename, const char* flags){
  _f = fopen(filename.c_str() , flags);
  return _f != NULL; // return true on success
}

bool Bitmap::close(){
  return fclose(_f);
}

bool Bitmap::eof(){
  return feof(_f);
}

void Bitmap::readHeader(){
  if(!_f)return;
  fread(&_file_header , sizeof(BITMAPFILEHEADER), 1, _f);
  fread(&_info_header , sizeof(BITMAPINFOHEADER), 1, _f);
}
void Bitmap::writeHeader(){
  if(!_f)return;
  fwrite(&_file_header , sizeof(BITMAPFILEHEADER), 1, _f);
  fwrite(&_info_header , sizeof(BITMAPINFOHEADER), 1, _f);
}
void Bitmap::printHeader(){

  printf("BITMAPFILEHEADER:\n\n");

  printf("%c%c size=%d, offset=%d\n\n",
         char(_file_header.bfType%256), char(_file_header.bfType/256),  /* BM, little endian */
         int(_file_header.bfSize), int(_file_header.bfOffBits)
         );


  printf("BITMAPINFOHEADER:\n\n");

  printf("biSize: %d\n %dx%d, %d bpp, compression=%d\n",
         int(_info_header.biSize), int(_info_header.biWidth), int(_info_header.biHeight),
         int(_info_header.biBitCount), int(_info_header.biCompression)
         );
  printf("planes=%d biSizeImage=%d biXPelsPerMeter=%d biYPelsPerMeter=%d\n",
         int(_info_header.biPlanes),
         int(_info_header.biSizeImage),
         int(_info_header.biXPelsPerMeter),
         int(_info_header.biYPelsPerMeter) );
  printf("biClrUsed=%d, biClrImportant=%d\n\n",
         int(_info_header.biClrUsed), int(_info_header.biClrImportant));

  if(_f)
    printf("current file position: %ld\n\n", ftell(_f));
}

bool Bitmap::readRow(){
  if(!_f)return false;
  RGB pel; RGBA tmp;

  for(size_t i = 0; i < 1920; ++i){

    // read one pixel from the row
    if(_info_header.biBitCount == 24)   // 24 bit per pel
      fread(&pel, sizeof(RGB), 1, _f);
    else{                               // 32 bit per pel
      fread(&tmp, sizeof(RGBA), 1, _f); pel = tmp;
    }

    if(feof(_f)) return false;

    // RGB to YUV conversion
    y[i] = toByte( 0.299 * pel.r + 0.587 * pel.g + 0.114 * pel.b );
    u[i] = toByte ( 0.492 * (pel.b - y[i]) / 0.872 + 128 );
    v[i] = toByte ( 0.877 * (pel.r - y[i]) / 1.230 + 128 );

  }

  return true;
}
bool Bitmap::writeRow(){
  if(!_f)return false;
  RGB pel; RGBA tmp;

  for(size_t i = 0; i < 1920; ++i){
    double Y = y[i],
           U = 0.872 * ( int(u[i]) - 128),
           V = 1.230 * ( int(v[i]) - 128);

    // YUV to RGB conversion
    pel.r = toByte ( Y - 0.00003 * U + 1.140 * V );
    pel.g = toByte ( Y - 0.394 * U - 0.58 * V );
    pel.b = toByte ( Y + 2.0319 * U - 0.0004 * V );

    // write one pixel to the row
    if(_info_header.biBitCount == 24)   // 24 bit per pel
      fwrite(&pel, sizeof(RGB), 1, _f);
    else{                               // 32 bit per pel
      tmp = pel;
      fwrite(&tmp, sizeof(RGBA), 1, _f);
    }

  }

  return true;
}
