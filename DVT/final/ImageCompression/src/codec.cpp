# include <cstdio>
# include "codec.h"
# define ABSROUND(x) (x > 0 ? x+=0.5 : x-=0.5)
# define ABS(x) (x > 0? x : -(x))

Pixel shift_buf[1920]; // a buffer for row pixel round shifting

// helper function: circular shifts the row pixel
void circ_shift(Pixel* arr, int s){
  if(s==0) return;
  if(s < 0){  // left circular shift
    s = -s;
    memcpy(shift_buf, arr+s, 1920-s );
    memcpy(shift_buf+1920-s, arr, s);
    memcpy(arr, shift_buf, 1920);
  }
  else        // left circular shift
    circ_shift(arr, -1920 + s);
}

void encode(char* filename_in, char* filename_out) {

  fprintf(stderr, "%s -> %s\n", filename_in, filename_out);

  Bitmap bin(filename_in, "rb"), bout(filename_out, "wb");
  bin.readHeader(); bout.copyHeaderFrom(bin); bout.writeHeader();
  Bitstream bs(bout.getFileHandle()); // continues writing output file
  WaveletCodec wc(1920);
  EZWEncoder ezw_enc(1920);
  double* coeff = new double[1920];

  size_t row_count = 0,
         encoded_bitnum,  // recording # of bits used by U and V channels
         prev_shift_amount = 0; // predictive coding for shift amount
  while(bin.readRow()){
    encoded_bitnum = 0;

    // find max horizontal Y pixel difference difference
    int max_diff = ABS(bin.y[1919] - bin.y[0]), diff;
    size_t shift_amount = 1919;
    for(size_t i = 0; i < 1919; ++i){
      diff = ABS(bin.y[i] - bin.y[i+1]);
      if(diff > max_diff){
        max_diff = diff;
        shift_amount = i;
      }
    }
    circ_shift(bin.y, -shift_amount); // left shift the whole row
    circ_shift(bin.u, -shift_amount); // with shift_amount pixels
    circ_shift(bin.v, -shift_amount);

     // encode shift_amount into the bitstream
    if(prev_shift_amount == shift_amount){
      bs.put('1');
      encoded_bitnum += 1;
    }else{
      bs.put(shift_amount, 12); // max shift_amount is 1920
      encoded_bitnum += 12;     // requires 1+11 bits to store it
                                // (with MSB = 0)
    }
    prev_shift_amount = shift_amount;

    // encode U
    wc.encode(coeff, bin.u);
    for(size_t i = 0; i < 1920; ++i) ABSROUND(coeff[i]); // round!
    encoded_bitnum += ezw_enc.encode(&bs, coeff, 634);

    // encode V
    wc.encode(coeff, bin.v);
    for(size_t i = 0; i < 1920; ++i) ABSROUND(coeff[i]); // round!
    encoded_bitnum += ezw_enc.encode(&bs, coeff, 634);

    // encode Y
    wc.encode(coeff, bin.y);
    for(size_t i = 0; i < 1920; ++i) ABSROUND(coeff[i]); // round!
    ezw_enc.encode(&bs, coeff, 7680 - encoded_bitnum);

    if(row_count++ % 100 == 0) fprintf(stderr, ".");
  }
  fprintf(stderr, "\n");

  bs.terminate();
  bout.close(); bin.close();
  delete[] coeff;
}

void decode(char* filename_in, char* filename_out){
  fprintf(stderr, "%s -> %s\n", filename_in, filename_out);

  Bitmap bin(filename_in, "rb"), bout(filename_out, "wb");
  bin.readHeader(); bout.copyHeaderFrom(bin); bout.writeHeader();;
  Bitstream bs(bin.getFileHandle(), true); // continues reading ".output" file

  WaveletCodec wc(1920);
  EZWDecoder ezw_dec(1920);
  double* coeff = new double[1920];

  size_t decoded_bitnum, // recording # of bits used by U and V channels
         prev_shift_amount = 0; // predictive coding for shift amount
  for(size_t row_count = 0; row_count < 1080; ++row_count){
    decoded_bitnum = 0;

    // decode shift amount
    char use_prev; int shift_amount;
    bs.get(use_prev);
    decoded_bitnum += 1;
    if(use_prev == '0'){
      bs.get(shift_amount, 11);
      decoded_bitnum += 11;
    }
    else{
      shift_amount = prev_shift_amount;
    }
    prev_shift_amount = shift_amount;

    // decode U
    decoded_bitnum += ezw_dec.decode(&bs, coeff, 634); // 640-6
    wc.decode(bout.u, coeff);

    // decode V
    decoded_bitnum += ezw_dec.decode(&bs, coeff, 634);
    wc.decode(bout.v, coeff);

    // decode Y
    ezw_dec.decode(&bs, coeff, 7680 - decoded_bitnum); // 7680 - 12
    wc.decode(bout.y, coeff);

    circ_shift(bout.y, shift_amount); // right shift the whole row
    circ_shift(bout.u, shift_amount);
    circ_shift(bout.v, shift_amount);

    bout.writeRow();

    if(row_count % 100 == 0) fprintf(stderr, ".");
  }
  fprintf(stderr, "\n");

  bout.close(); bin.close();
  delete[] coeff;
}

