#ifndef __CODEC__
#define __CODEC__

#include "bitmap.h"
#include "wavelet.h"
#include "ezw.h"

void encode(char* fin, char* fout);
void decode(char* fin, char* fout);

#endif // __CODEC__
