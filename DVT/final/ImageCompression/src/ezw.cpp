#include <list>
#include <vector>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cassert>

#include "ezw.h"

# define ABS(x) (x > 0? x : -(x))

# define P    'p'   // positive significance
# define N    'n'   // negative significance
# define ZTR  't'   // zero tree root
# define IZ   'z'   // isolated zero
# define PRUNED 'x' // pruned zero tree root


// mark the node's siginicance via post-order traversal
// returns if the visited node is a zero-tree root
//
bool EZWEncoder::_markNodes(size_t node_id, const int& T ){

  // base case: (beyond the) leaf of the tree
  if(node_id >= _size) return true;

  // recursive case:
  bool left_ztr, right_ztr;
  left_ztr  = _markNodes(node_id * 2, T);        //  left subtree
  right_ztr = _markNodes(node_id * 2 + 1, T);    // right subtree

  if( ABS(_coeff[node_id]) >= T ){ // the node is sigificant
    //int tmp = ABS(_coeff[node_id]);

    if(_coeff[node_id] >= 0) _s_map[node_id] = P;
    else _s_map[node_id] = N;

    return false; // tells its parent that itself is not a zero tree root
  }
  else{ // the node is insignificant
    if(left_ztr && right_ztr){ // both left, right subtree are zero tree root
      _s_map[node_id] = ZTR;

      if(2*node_id < _size)
        _s_map[ 2*node_id ] = _s_map[ 2*node_id+1 ] = PRUNED;
        // children can be pruned, since their parent are set as ZTR

      return true; // tells its parent that itself is a ZTR
    }
    else{ // the node is a isolated zero
      _s_map[node_id] = IZ;
      return false;
    }
  }

}

size_t EZWEncoder::encode(Bitstream* bitstream, double* coeff, size_t encode_limit){
  _coeff = coeff;
  _encoded = bitstream;

  // set the encode bit limit, and zeros the count
  _encode_bit_limit = encode_limit;
  _encoded_bit_num = 0;

  //string encoded; // encoded oputput, including dominant & subordinate passes
  // significance symbol: isolated zero = 'z'; zero tree root = 't'
  // positive significance = 'p', negative significance = 'n'

  // find first threshold T (T_0)
  int max_coeff = 0;
  for(size_t i = 0; i<_size; ++i)
    if( ABS(_coeff[i]) > max_coeff ) max_coeff = _coeff[i];

  int T = 1, T_offset = 0;
  while( T*2 <= max_coeff ){
    T *= 2;
    ++T_offset;
  }

  // write T_0 to stream
  _encoded->put(T_offset, 3);
  _encoded_bit_num += 3;

  //T_0 = T; // passes out the initial threshold T_0
  vector<SubordinateEntry> sublist; // subordinate list

  for(; T >= 1; T/=2){ // multiple passes

    /* Dominant Pass: determine node signicance, and output node char
       First mark the tree nodes in post-order traversal.
            7
          3   6
         1 2 4 5
       Traversal result is saved in _s_map.
    */
    for(size_t i = _dc_count; i<2*_dc_count; ++i)
      _markNodes(i, T);  // first-order AC values are the tree roots

    // Then output the encoded character top-down (low frequency -> high frequency)
    //
    // Dominant Pass -- DC part:
    for(size_t i = 0; i < _dc_count; ++i){

      if( ABS(_coeff[i]) >= T ){
        // the DC is either positive significance or negative significance.

        if(_coeff[i]>0){
          if(!_putSignificance(P))return _encoded_bit_num;
        }
        else{
          if(!_putSignificance(N))return _encoded_bit_num;
        }
        // register it as a subordinate entry with uncertainty interval [T, 2T)
        sublist.push_back( SubordinateEntry(T, 2*T, _coeff[i]) );

        // set the significant coeff to 0, so that it does not prevent
        // the emergence of a new zero tree
        _coeff[i] = 0;
      }
      else{ // the DC is either a zero tree root or isolated zero
        char& child = _s_map[i + _dc_count];
        if(child == ZTR){
          // if the child is a ZTR and the DC is insignificant
          // the DC must be a ZTR too.
          if(!_putSignificance(ZTR))return _encoded_bit_num;
          child = PRUNED;     // don't traverse the zero trees in the following steps
        }else{
          // with a significance in its subtree, the DC is an isolated zero
          if(!_putSignificance(IZ))return _encoded_bit_num;
        }
      }
    }
    // Dominant Pass -- AC part: already marked by tree traversal before
    for(size_t i = _dc_count; i < _size; ++i){
      if(_s_map[i] != PRUNED)
        if(!_putSignificance(_s_map[i]))return _encoded_bit_num;
          // directly print out the significance

      if(_s_map[i] == P || _s_map[i] == N){
        // register it as a subordinate entry with uncertainty interval [T, 2T)
        sublist.push_back( SubordinateEntry(T, 2*T, _coeff[i]) );

        // set the significant coeff to 0, so that it does not prevent
        // the emergence of a new zero tree
        _coeff[i] = 0;
      }
    }

    // subordinary pass: output subordinary pass bits, and
    // halve the uncertianty interval of each entry in subordinate list
    //
    for(vector<SubordinateEntry>::iterator it = sublist.begin();
        it != sublist.end(); ++it){
      int m = (it->upper + it->lower) / 2;

      if( ABS(it->original) < m ){
        it->upper = m;          // halve the uncertainty interval
        if(!_putSubordinate('0'))return _encoded_bit_num;
      }
      else{
        it->lower = m;          // halve the uncertainty interval
        if(!_putSubordinate('1'))return _encoded_bit_num;
      }
    }

  }
  return _encoded_bit_num;
}

// puts a significance symbol to the bitstream.
// returns false on reading failure
//
bool EZWEncoder::_putSignificance(const char& c){
  if(_encoded_bit_num > _encode_bit_limit - 3)
    return false;
  //_encoded->put(c);

  switch(c){
    case ZTR:
      _encoded->put('0');
      _encoded_bit_num += 1;
      break;
    case IZ:
      _encoded->put("10");
      _encoded_bit_num += 2;
      break;
    case P:
      _encoded->put("110");
      _encoded_bit_num += 3;
      break;
    case N:
      _encoded->put("111");
      _encoded_bit_num += 3;
      break;
    default:
      fprintf(stderr, "Encoder: undefined significance symbol %c\n", c);
      exit(-1);
  }
  return true;
}

// puts a subordinate symbol to the bitstream.
// returns false on reading failure
//
bool EZWEncoder::_putSubordinate(const char& c){
  if(_encoded_bit_num > _encode_bit_limit - 3)
    return false;
  if(c!='0' && c!= '1'){
    fprintf(stderr, "Encoder: undefined subordinate symbol %c\n", c);
    exit(-1);
  }
  _encoded->put(c);
  ++_encoded_bit_num;
  return true;
}


size_t EZWDecoder::decode(Bitstream* encoded, double* decoded, size_t decode_lim){
  _encoded = encoded;

  // get T_0
  int T;
  _encoded->get(T, 3);
  T = 1 << T;

  // set the decode bit limit, and zeros the count
  _decode_bit_limit = decode_lim;
  _decoded_bit_num = 3; // 3 bits for T_0 already

  // cleanup decoded buffer
  for(size_t i = 0; i < _size; ++i) decoded[i] = 0.0;

  vector<SubordinateEntry> sublist; // subordinary list
  list<size_t> steps; // Queue of the node ids dominant pass should traverse

  // start decoding
  for(; T >= 1; T/=2){
    steps.clear(); //fprintf(stderr, "%d ", T);

    // Dominant Pass

    // Dominant Pass -- DC part
    for(size_t id = 0; id < _dc_count; ++id){
      char sig ;      // read a significance out of encoded msg
      if(!_getSignificance(sig))return _decoded_bit_num;

      if(sig != ZTR)
        steps.push_back(id + _dc_count); //  queueing the traversal of subtree root

      // process the current coeff
      if(sig == P){
        sublist.push_back( SubordinateEntry(T, T*2, id, 1) );
        decoded[id] = 3 * T / 2; // (T+2T)/2
      }else if(sig == N){
        sublist.push_back( SubordinateEntry(T, T*2, id, -1) );
        decoded[id] = - 3 * T / 2; // (T+2T)/2
      }

      // for ZTR and IZ, no additional steps are required
    }

    // Dominant Pass -- AC part
    while( !steps.empty() ){
      size_t id = steps.front();  // take the coeff id we are traversing
      steps.pop_front();

      char sig;    // read a significance out of encoded msg
      if(!_getSignificance(sig))return _decoded_bit_num;

      if(sig != ZTR && id*2 + 1 < _size){ // record next traversal steps
        steps.push_back(id*2); steps.push_back(id*2 + 1);
      }

      // process the current coeff
      if(sig == P){
        sublist.push_back( SubordinateEntry(T, T*2, id, 1) );
        decoded[id] = 3 * T / 2; // (T+2T)/2
      }else if(sig == N){
        sublist.push_back( SubordinateEntry(T, T*2, id, -1) );
        decoded[id] = - 3 * T / 2; // (T+2T)/2
      }

      // for ZTR and IZ, no additional steps are required
    }

    // Subordinary Pass: halve the uncertianty interval
    // and update coeff[] with the new interval
    //
    for(vector<SubordinateEntry>::iterator it = sublist.begin();
        it != sublist.end(); ++it ){

      int m = (it->upper + it->lower) / 2;
      char b;
      if(!_getSubordinate(b))return _decoded_bit_num;
      // halve their uncertianty interval
      if(b == '0')
        it->upper = m;
      else
        it->lower = m;

      // update decoded coeff
      decoded[it->id] = (it->upper + it->lower) / 2 * it->sign;
    }
  }

  return _decoded_bit_num;
}


// reads a significance symbol from the bitstream.
// returns false on reading failure
//
bool EZWDecoder::_getSignificance(char& c){ // TODO: cope with bit stream
  if(_decoded_bit_num > _decode_bit_limit - 3){
    //assert(_encoded_pos == _encoded.size());
    //assert(_decoded_bit_num > _decode_bit_limit - 3);
    return false;
  }

  // a static huffman decoding tree...
  _encoded->get(c);
  if(c=='0'){             // 0
    c = ZTR;
    _decoded_bit_num += 1;
    return true;

  }else if(c=='1'){
    _encoded->get(c);
    if(c == '0'){         // 10
      c = IZ;
      _decoded_bit_num += 2;
      return true;

    }else if(c == '1'){
      _encoded->get(c);
      if(c == '0'){       // 110
        c = P;
        _decoded_bit_num += 3;
        return true;
      }
      else if(c == '1'){  // 111
        c = N;
        _decoded_bit_num += 3;
        return true;
      }
    }
  }

  fprintf(stderr, "Decoder: undefined significance symbol %c\n", c);
  exit(-1);
}

// reads a subordinate symbol from the bitstream.
// returns false on reading failure
//
bool EZWDecoder::_getSubordinate(char &c){
  if(_decoded_bit_num > _decode_bit_limit - 3) {
    //assert(_encoded_pos == _encoded.size());
    //assert(_decoded_bit_num > _decode_bit_limit - 3);
    return false;
  }
  _encoded->get(c);

  if(c!='0' && c!= '1'){
    fprintf(stderr, "Decoder: undefined subordinate symbol %c\n", c);
    exit(-1);
  }

  _decoded_bit_num += 1;
  return true;
}
