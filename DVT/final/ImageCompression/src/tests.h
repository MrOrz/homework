// tests.h: collection of tests

#ifndef __TESTS__
#define __TESTS__

#include <cstdio>
#include "bitmap.h"
#include "wavelet.h"
#include "ezw.h"

# define ABS(x) (x > 0? x : -(x))
# define ABSROUND(x) (x > 0 ? x+=0.5 : x-=0.5 )

void diff_detection_test(){
  char* filename_in = "testcases/Buildings.n5.bmp";
  char* filename_out = "testcases/Buildings.edge.n5.bmp";

  fprintf(stderr, "%s -> %s\n", filename_in, filename_out);

  Bitmap bin(filename_in, "rb"), bout(filename_out, "wb");
  bin.readHeader(); bout.copyHeaderFrom(bin); bout.writeHeader();
  // Bitstream bs(bout.getFileHandle()); // continues writing output file
  WaveletCodec wc(1920);
  EZWEncoder ezw_enc(1920);
  //double* coeff = new double[1920];

  //size_t row_count = 0;
  while(bin.readRow()){
    memcpy(&(bout.y), &(bin.y), 1920);
    memcpy(&(bout.u), &(bin.u), 1920);
    memcpy(&(bout.v), &(bin.v), 1920);

    int max_y_diff = ABS(bin.y[0] - bin.y[1919]); size_t max_y_diff_id = 0;
    for(size_t i = 1; i<1920; ++i)
      if(bin.y[i] - bin.y[i-1] > max_y_diff){
        max_y_diff = bin.y[i] - bin.y[i-1];
        max_y_diff_id = i;
      }

    bout.y[max_y_diff_id] = 0;
    bout.writeRow();
  }
}

Pixel _shift_buf[1920];

// circular shift arr by s.
// s < 0: left circular shift; s > 0: right circular shift
//
void _circ_shift(Pixel* arr, int s){
  if(s==0) return;
  if(s < 0){  // left circular shift
    s = -s;
    memcpy(_shift_buf, arr+s, 1920-s );
    memcpy(_shift_buf+1920-s, arr, s);
    memcpy(arr, _shift_buf, 1920);
  }
  else        // left circular shift
    _circ_shift(arr, -1920 + s);
}

void encoder_test(char* filename_in, char* filename_out){

  fprintf(stderr, "%s -> %s\n", filename_in, filename_out);

  Bitmap bin(filename_in, "rb"), bout(filename_out, "wb");
  bin.readHeader(); bout.copyHeaderFrom(bin); bout.writeHeader();
  Bitstream bs(bout.getFileHandle()); // continues writing output file
  WaveletCodec wc(1920);
  EZWEncoder ezw_enc(1920);
  double* coeff = new double[1920];

  size_t row_count = 0, bitnum, encoded_bitnum;
  while(bin.readRow()){
    encoded_bitnum = 0;

    // find max horizontal Y pixel difference difference
    int max_y_diff = ABS(bin.y[1919] - bin.y[0]);
    size_t max_y_diff_id = 1919;
    for(size_t i = 0; i<1919; ++i)
      if(ABS(bin.y[i] - bin.y[i+1]) > max_y_diff){
        max_y_diff = ABS(bin.y[i] - bin.y[i+1]);
        max_y_diff_id = i;
      }
    _circ_shift(bin.y, -max_y_diff_id); // left shift the whole row
    _circ_shift(bin.u, -max_y_diff_id);
    _circ_shift(bin.v, -max_y_diff_id);

    bs.put(max_y_diff_id, 12); // encode shift amount
    //printf("%d ", max_y_diff_id);

    // encode U
    wc.encode(coeff, bin.u);
    for(size_t i = 0; i < 1920; ++i) ABSROUND(coeff[i]); // round!
    bitnum = ezw_enc.encode(&bs, coeff, 640-6);
    //printf("%d ", bitnum);
    encoded_bitnum += bitnum;

    // encode V
    wc.encode(coeff, bin.v);
    for(size_t i = 0; i < 1920; ++i) ABSROUND(coeff[i]); // round!
    bitnum = ezw_enc.encode(&bs, coeff, 640-6);
    //printf("%d ", bitnum);
    encoded_bitnum += bitnum;

    if(row_count>=46 && row_count <= 50){
      for(size_t i = 0; i < 1920; ++i) printf("%u ", bin.y[i]);
      printf("\n");
    }
    // encode Y
    wc.encode(coeff, bin.y);
    for(size_t i = 0; i < 1920; ++i) ABSROUND(coeff[i]); // round!
    if(row_count>=46 && row_count <= 50){
      for(size_t i = 0; i < 1920; ++i) printf("%lf ", coeff[i]);
      printf("\n");
    }

    bitnum = ezw_enc.encode(&bs, coeff, 7680-12-encoded_bitnum);
    //printf("%d\t", bitnum);


    if(row_count++ % 100 == 0) fprintf(stderr, ".");
  }
  fprintf(stderr, "\n");

  bs.terminate();
  bout.close(); bin.close();
  delete[] coeff;
}

void decoder_test(char* filename_in, char* filename_out){
  fprintf(stderr, "%s -> %s\n", filename_in, filename_out);

  Bitmap bin(filename_in, "rb"), bout(filename_out, "wb");
  bin.readHeader(); bout.copyHeaderFrom(bin); bout.writeHeader();;
  Bitstream bs(bin.getFileHandle(), true); // continues reading ".output" file

  WaveletCodec wc(1920);
  EZWDecoder ezw_dec(1920);
  double* coeff = new double[1920];

  size_t decoded_bitnum, bitnum;
  int shift_amount;
  for(size_t row_count = 0; row_count < 1080; ++row_count){
    decoded_bitnum = 0;

    bs.get(shift_amount, 12); // decode shift amount
    //printf("%d ", shift_amount);

    // decode U
    bitnum = ezw_dec.decode(&bs, coeff, 640-6);
    wc.decode(bout.u, coeff);
    //printf("%d\t", bitnum);
    decoded_bitnum += bitnum;

    // decode V
    bitnum = ezw_dec.decode(&bs, coeff, 640-6);
    wc.decode(bout.v, coeff);
    //printf("%d\t", bitnum);
    decoded_bitnum += bitnum;

    // decode Y
    bitnum = ezw_dec.decode(&bs, coeff, 7680-12-decoded_bitnum);
    if(row_count>=46 && row_count <= 50){
      for(size_t i = 0; i < 1920; ++i) printf("%lf ", coeff[i]);
      printf("\n");
    }
    wc.decode(bout.y, coeff);
    //printf("%d\n", bitnum);
    if(row_count>=46 && row_count <= 50){
      for(size_t i = 0; i < 1920; ++i) printf("%u ", bout.y[i]);
      printf("\n");
    }


    _circ_shift(bout.y, shift_amount); // right shift the whole row
    _circ_shift(bout.u, shift_amount);
    _circ_shift(bout.v, shift_amount);
    bout.writeRow();

    if(row_count % 100 == 0) fprintf(stderr, ".");
  }
  fprintf(stderr, "\n");

  bout.close(); bin.close();
  delete[] coeff;


}

void wavelet_test(){
  Pixel p[] = {10,13,25,26,29,21,7,15,10,13,25,26};
  size_t size = 12;
  double *c = new double[size];

  WaveletCodec wc(size);

  printf("original:\n");
  for( size_t i = 0; i<size; ++i )printf("%d ", p[i]);
  printf("\n\n");


  wc.encode(c, p);
  printf("encoded:\n");
  for( size_t i = 0; i<size; ++i )printf("%lf ", c[i]);
  printf("\n\n");

  wc.decode(p, c);
  printf("decoded:\n");
  for( size_t i = 0; i<size; ++i )printf("%d ", p[i]);
  printf("\n\n");
}

void show_testcase_info(){

  Bitmap bmp;

  char * files[] = {
    "testcases/7_Luma_CZP_50IRE.bmp",
    "testcases/Buildings.bmp",
    "testcases/image053.bmp",
    "testcases/image059.bmp",
    "testcases/image070.bmp",
    "testcases/image101.bmp",
    "testcases/QS_test_pattern.bmp",
    "testcases/Table.bmp"
  };

  for(size_t i = 0; i < 8; ++i){
    printf("====== %s ======\n\n", files[i]);

    bmp.open(files[i], "rb");
    bmp.readHeader(); bmp.printHeader();bmp.close();
  }

}
/*
void truncated_test(){
  Bitmap bmp_in("testcases/7_Luma_CZP_50IRE.bmp", "rb");
  Bitmap bmp_out("testcases/7_Luma_CZP_50IRE-compressed.bmp", "wb");

  bmp_in.readHeader();
  bmp_out.copyHeaderFrom(bmp_in); bmp_out.writeHeader();

  EZWEncoder ezw_enc(1920);
  EZWDecoder ezw_dec(1920);
  WaveletCodec wc(1920);

  double* coeff; string encoded;
  int T_0;

  size_t row_count = 0;

  while(bmp_in.readRow()){
    ++ row_count;

   // memcpy(&(bmp_out.y), &(bmp_in.y), 1920);
   // memcpy(&(bmp_out.u), &(bmp_in.u), 1920);
   // memcpy(&(bmp_out.v), &(bmp_in.v), 1920);

    // encode Y
    coeff = wc.encode(bmp_in.y);
    //for(size_t i = 0; i < 1920; ++i) printf("%lf\t", coeff[i]);
    for(size_t i = 0; i < 1920; ++i) ABSROUND(coeff[i]); // round!
    encoded = ezw_enc.encode(coeff, T_0, 1920*8*5/12);
    delete[] coeff;

    //fprintf(stderr, "%d ", encoded.size());
    //printf("\n\n");

    // decode Y
    // size_t TT = T_0; // mark old T, to detect how many passes is decoded
    coeff = ezw_dec.decode(encoded, T_0, 1920*8*5/12);
    //coeff = ezw_dec.decode(encoded, T_0); // near lossless
    //for(size_t i = 0; i < 1920; ++i) printf("%lf\t", coeff[i]);
    wc.decode(bmp_out.y, coeff);
    //for(size_t i = T_0; i < 1920; i += 128/(TT/T_0) ) if(ABS(bmp_out.y[i-1] - bmp_out.y[i]) <= T_0 )bmp_out.y[i] = 0; // mark deblock target
    delete[] coeff;

    // encode U
    coeff = wc.encode(bmp_in.u);
    for(size_t i = 0; i < 1920; ++i) ABSROUND(coeff[i]); // round!
    encoded = ezw_enc.encode(coeff, T_0, 1920*8/24);
    delete[] coeff;

    // decode U
    coeff = ezw_dec.decode(encoded, T_0, 1920*8/24);
    //coeff = ezw_dec.decode(encoded, T_0); // near lossless
    wc.decode(bmp_out.u, coeff);
    delete[] coeff;

    // encode V
    coeff = wc.encode(bmp_in.v);
    for(size_t i = 0; i < 1920; ++i) ABSROUND(coeff[i]); // round!
    encoded = ezw_enc.encode(coeff, T_0, 1920*8/24);
    delete[] coeff;

    // decode V
    coeff = ezw_dec.decode(encoded, T_0, 1920*8/24);
    //coeff = ezw_dec.decode(encoded, T_0); // near lossless
    wc.decode(bmp_out.v, coeff);
    //for(size_t i = 0; i < 1920; i += T_0) bmp_out.y[i] = 0; // mark T_0
    delete[] coeff;

    if( row_count%10 == 0)
      fprintf(stderr, ".");

    bmp_out.writeRow();
  }
  fprintf(stderr, "done.\n\n");
  bmp_in.close(); bmp_out.close();
}

void ezw_stats(){
# define P    'p'   // positive significance
# define N    'n'   // negative significance
# define ZTR  't'   // zero tree root
# define IZ   'z'   // isolated zero
# define PRUNED 'x' // pruned zero tree root

  char * files[] = {
    "testcases/7_Luma_CZP_50IRE.bmp",
    "testcases/Buildings.bmp",
    "testcases/image053.bmp",
    "testcases/image059.bmp",
    "testcases/image070.bmp",
    "testcases/image101.bmp",
    "testcases/QS_test_pattern.bmp",
    "testcases/Table.bmp"
  };
  Bitmap bmp;

  FILE * fout = fopen("ezw_stats.log", "w");

  int total[127] = {0};

  for(size_t i = 0; i < 8; ++i){

    fprintf(fout, "---- %s ----\n\n", files[i]);
    fprintf(stderr, "%s", files[i]);

    bmp.open(files[i], "rb");
    bmp.readHeader();

    EZWEncoder ezw_enc(1920);
    WaveletCodec wc(1920);

    int count_row = 0;
    while(bmp.readRow()){
      int count[127] = {0};
      int T_0;

      double *coeff = wc.encode(bmp.y);
      string encoded = ezw_enc.encode(coeff, T_0);
      int bitcount = 0;
      for(size_t j = 0; j < encoded.size() && bitcount < 1920*8/3 ; ++j){
        ++count[encoded[j]] ;
        ++bitcount;
        if(encoded[j] != '0' && encoded[j] != '1') ++bitcount;
      }
      fprintf(fout, "Y: T_0 = %d, P(%d) N(%d) IZ(%d) ZTR(%d)\n",
               T_0, count[P], count[N], count[IZ], count[ZTR]);
      total[P] += count[P]; total[N] += count[N];
      total[IZ] += count[IZ]; total[ZTR] += count[ZTR];
      count[P] = count[N] = count[IZ] = count[ZTR] = 0;
      delete[] coeff;

      coeff = wc.encode(bmp.u);
      encoded = ezw_enc.encode(coeff, T_0);
      bitcount = 0;
      for(size_t j = 0; j < encoded.size() && bitcount < 1920*8/12 ; ++j){
        ++count[encoded[j]] ;
        ++bitcount;
        if(encoded[j] != '0' && encoded[j] != '1') ++bitcount;
      }
      fprintf(fout, "U: T_0 = %d, P(%d) N(%d) IZ(%d) ZTR(%d)\n",
               T_0, count[P], count[N], count[IZ], count[ZTR]);
      total[P] += count[P]; total[N] += count[N];
      total[IZ] += count[IZ]; total[ZTR] += count[ZTR];
      count[P] = count[N] = count[IZ] = count[ZTR] = 0;
      delete[] coeff;

      coeff = wc.encode(bmp.v);
      encoded = ezw_enc.encode(coeff, T_0);
      bitcount = 0;
      for(size_t j = 0; j < encoded.size() && bitcount < 1920*8/12 ; ++j){
        ++count[encoded[j]] ;
        ++bitcount;
        if(encoded[j] != '0' && encoded[j] != '1') ++bitcount;
      }
      fprintf(fout, "V: T_0 = %d, P(%d) N(%d) IZ(%d) ZTR(%d)\n",
               T_0, count[P], count[N], count[IZ], count[ZTR]);
      total[P] += count[P]; total[N] += count[N];
      total[IZ] += count[IZ]; total[ZTR] += count[ZTR];
      count[P] = count[N] = count[IZ] = count[ZTR] = 0;
      delete[] coeff;

      if(count_row++ % 100 == 0)
        fprintf(stderr, ".");

    }

    fprintf(stderr, "done.\n");
    fprintf(fout, "\n");
    bmp.close();
  }

  int t = total[P] + total[N] + total[ZTR] + total[IZ];
  fprintf(fout, "total P:%d(%lf) N:%d(%lf) ZTR:%d(%lf) IZ:%d(%lf)\n",
          total[P], double(total[P])/t,
          total[N], double(total[N])/t,
          total[ZTR], double(total[ZTR])/t,
          total[IZ], double(total[IZ])/t
          );

  fclose(fout);
}

void ezw_test(){
  double c[] = {15, -13, 12, 3, 7, -4, -1, 6, 5, 2, -3, 1};
  //double c[] = {0,0,0,0,0,0,0,0,0,0,0,0};
  size_t size_of_c = 12;
  int T_0;

  EZWEncoder enc(size_of_c);

  printf("original: ");
  for(size_t i = 0; i < size_of_c; ++i) printf("%lf ", c[i]); printf("\n");

  printf("encoded: ");
  string encoded = enc.encode(c, T_0);
  printf("%s\n", encoded.c_str() );

  printf("\n------\n\n");

  EZWDecoder dec(size_of_c);
  double *d = dec.decode(encoded, T_0);

  printf("decoded: ");
  for(size_t i = 0; i < size_of_c; ++i) printf("%lf ", d[i]); printf("\n");
}
*/
#endif // __TESTS__
