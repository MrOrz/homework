#ifndef __EZW__
#define __EZW__

#include "bitstream.h"
//#include <string>
#define BITS_NUM_MAX 4096000000UL

using namespace std;

class EZWEncoder{
  public:
    EZWEncoder(size_t s):_size(s) {
      _dc_count = _size; // number of DC coefficients = max odd factor of size
      while(_dc_count % 2 == 0) _dc_count /= 2;

      _s_map = new char[_size];
    }
    ~EZWEncoder(){
      delete[] _s_map;
    }

    // encode(wavelet coefficients, target_bit_count)
    // puts encoded coefficients into Bitstream
    // returns # of bits encoded
    //
    size_t encode(Bitstream* , double* coeff, size_t = BITS_NUM_MAX);

  private:
    double* _coeff;  // share coeff array with _markNodes()
    size_t _size;    // number of pixels to encode
    char* _s_map;    // the significance map
    size_t _dc_count;// # of DC coeff (= the largest odd facter of _size)

    size_t _encode_bit_limit; // the max number of bits per encode
    size_t _encoded_bit_num;  // number of bits encoded.
                              // Resetted each time encode() is called.

    class SubordinateEntry {
      public:
        SubordinateEntry(int l, int u, double o):upper(u), lower(l), original(o){}
        double upper; // upper bound of uncertainty interval
        double lower; // lower bound of uncertainty interval
        double original; // original value of the coefficient
    };

    // mark the node's siginicance via post-order traversal
    // returns if the visited node is a zero-tree root
    //
    bool _markNodes(size_t node_id, const int& T );

    Bitstream* _encoded;

    // puts a symbol to the bit stream _encoded,
    // and increments
    // returns false on reading failure.
    //
    bool _putSignificance(const char&);
    bool _putSubordinate(const char&);
};

class EZWDecoder{
  public:
    EZWDecoder(size_t s):_size(s){
      _dc_count = _size; // number of DC coefficients = max odd factor of size
      while(_dc_count % 2 == 0) _dc_count /= 2;
    }

    // decode(Bitstream, target bit count)
    // returns # of bits decoded
    //
    size_t decode(Bitstream* ,double* coeff, size_t = BITS_NUM_MAX);
  private:
    size_t _size;      // number of pixels to decode.
    size_t _dc_count;  // # of DC coeff (= the largest odd facter of _size)

    //double* _decoded;  // decoded coefficient buffer
    size_t _decode_bit_limit; // the max number of bits per decode
    size_t _decoded_bit_num;  // number of bits decoded.
                              // Resetted each time decode() is called.

    class SubordinateEntry {
      public:
        SubordinateEntry(int l, int u, size_t i, char s)
          :upper(u), lower(l), id(i), sign(s){}
        int upper; // upper bound of uncertainty interval
        int lower; // lower bound of uncertainty interval
        size_t id; // the corresponding coefficent id of this entry
        char sign; // sign of the coefficient, 1 or -1
    };

    Bitstream* _encoded;
    //size_t _encoded_pos;

    // reads a symbol from the bit stream _encoded,
    // and increments
    // returns false on reading failure.
    //
    bool _getSignificance(char&);
    bool _getSubordinate(char&);
};

#endif // __EZW__
