#include <cstring> // memcpy
#include "wavelet.h"
//#include <cstdio>

void WaveletCodec::encode(double* tmp, const Pixel* img){
  size_t w = _w;

  for(size_t i=0; i<w; ++i) _coeff[i] = img[i];// copy from original image
                                              // also converts Pixel to double

  while(w % 2==0){

    for(size_t i = 0; i < w/2; ++i){
      tmp[i] = (_coeff[2*i] + _coeff[2*i+1]) / 2 ;     // low frequency part
      tmp[w/2+i] = (_coeff[2*i] - _coeff[2*i+1]) / 2; // high frequency part
    }

    memcpy(_coeff, tmp, sizeof(double) * w); // assign the output of this pass

    w /= 2; // only process low frequency part the next time
  }

  // write the final output to tmp[]
  memcpy(tmp, _coeff, _w * sizeof(double));
  //return tmp;
}

void WaveletCodec::decode(Pixel* buf, double* coeff){
  size_t width = _dc_count * 2;
  // width should be 2 * (odd number), indicating the first region
  // to be decoded.

  while(width <= _w){
    for(size_t i = 0; i < width/2; ++i){
      _coeff[2*i]    = (coeff[i] + coeff[width/2+i] ) ;
      _coeff[2*i+1]  = (coeff[i] - coeff[width/2+i] ) ;
    }
    memcpy(coeff, _coeff, sizeof(double) * width); // assign the output of this pass

    width *= 2;
  }
  for(size_t i = 0; i<_w; ++i) buf[i] = toByte(coeff[i]);
}
