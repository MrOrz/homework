#include "bitstream.h"
#include <cassert>

using namespace std;

bool Bitstream::get(char& c){
  if(_head == 0){
    fread(&_buf, 1, 1, _f);
    if(feof(_f))return false;
    _head = 1 << 7;
  }
  c = _buf & _head  ? '1':'0' ;
  _head >>= 1;
  return true;
}

bool Bitstream::get(int& n, const size_t width){
  n = 0;
  char c;
  for(size_t i = 0; i < width; ++i){
    if(!get(c)) return false;
    n = (n << 1) + int(c-'0');
  }

  return true;
}

void Bitstream::put(const char& c){
  assert(c == '1' || c == '0');

  if(_head == 0){
    _flush();
    _head = 1 << 7;
  }

  if(c == '1')
    _buf |= _head;
  _head >>= 1;
}

void Bitstream::put(const char* c){
  for(size_t i = 0; c[i]!='\0'; ++i) put(c[i]);
}

void Bitstream::put(const int& n, const size_t width){
  for(size_t head = 1 << (width-1); head > 0; head >>= 1){
    if(n & head)
      put('1');
    else
      put('0');
  }
}

void Bitstream::terminate(){
  while(_head > 0)
    put('0');
  _flush();
}

void Bitstream::_flush(){
  fwrite(&_buf, 1, 1, _f);
  _buf = 0;
}
