#ifndef __BITSTREAM__
#define __BITSTREAM__
#include <cstdio>

class Bitstream{
  public:
    Bitstream(FILE* fptr, bool input_stream = false)
    :_f(fptr), _buf(0), _head(1<<7){
      if(input_stream) fread(&_buf, 1, 1, _f); // fill the buffer first
    }
    void put(const char&);
    void put(const char*);
    void put(const int&, const size_t width);
    bool get(char&);
    bool get(int&, const size_t width);

    // terminate() is a must only for output Bitstreams;
    // input bitstreams must not call terminate()!
    //
    void terminate();

  private:
    FILE* _f;
    unsigned char _buf;
    size_t _head; // head of buffer;
    void _flush(); // write completed _buf to file.
};
#endif // __BITSTREAM__
