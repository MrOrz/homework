% calculates time consistance score
function timeconsist()
  FILES = {'7_Luma_CZP_50IRE', 'Buildings', 'image053', 'image059', 'image070', 'image101', 'QS_test_pattern', 'Table'};
  ENCODER = 'Encoder.exe';
  DECODER = 'Decoder.exe';
  
  for f = FILES
    fn = char(f);
    genImages(fn); % generate the shifted images
    max_diff = zeros(7,1); % max_diff_1 ~ max_diff_7 = 0
    
    for n = 0:7
      bn = bmpName(fn, n);
      
      if exist( strcat(bn, '.out'), 'file') == 0 % when xxx.bmp.out does not exist
        % xxx.bmp -> xxx.bmp.out
        system(sprintf( '%s %s %s.out', ENCODER, bn, bn ) );
      end
      if exist( strcat('c_', bn), 'file') == 0   % when c_xxx.bmp does not exist
        % xxx.bmp.out -> c_xxx.bmp
        system(sprintf( '%s %s.out c_%s', DECODER, bn, bn ) );
      end
      
      if n == 0
        iref = double( imread( sprintf('c_%s', bmpName(fn, 0) ) ) );
        % just read the compressed & uncompressed image
      else
        img = double( imread( sprintf('c_%s', bmpName(fn, n) ) ) );
        iref = circshift(iref, [0, 1, 0]);  % circle shift the reference image
        
        diffimg = abs( img-iref );          % do the substraction
        diffimg(:,1,:) = zeros(1080,1,3);   % zeros out the vertical column
        diffimg(:,1920,:) =  zeros(1080,1,3); % at column = 1 and 1920
        
        diff = max(max(max( diffimg )) ); % find the max of the 3-D matrix
        if max_diff(n) < diff
          max_diff(n) = diff;
        end
        iref = img; % ref of the next img == this image
      end
      
    end
    
    % show the max_diff and time consistency score
    max_diff = transpose(max_diff)
    avg_max_diff = mean(max_diff)
    score = 120 - 1.5 * avg_max_diff;
    
    fprintf(1, 'Time consistancy score for %s = %f\n', fn, score);
  end

end

% generate the circular-shifted images
% filename.n1.bmp, filename.n2.bmp, ...... filename.n7.bmp
function genImages(filename)
  img = imread( bmpName(filename, 0) );
  for i = 1:7
    nm = bmpName(filename, i);
    if exist(nm, 'file') ~= 0, continue, end % skip if the file exists
    img = circshift(img, [0, 1, 0]);
    imwrite(img, bmpName(filename, i), 'bmp');
  end
end

function bn = bmpName(fn ,i)
  if(i >= 1)
    bn = sprintf('%s.n%d.bmp', fn, i);
  else
    bn = strcat(fn, '.bmp');
  end
end