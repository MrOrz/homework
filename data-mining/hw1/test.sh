#!/bin/sh

echo "APRIORI =================\n"

echo "100 sup=30"
time bin/hw1 a test/100.txt output/100_30.txt 30 > out.log
diff -w -b out.log test/100_30.out
echo "\n100 sup=40"
time bin/hw1 a test/100.txt output/100_40.txt 40 > out.log
diff -w -b out.log test/100_40.out
echo "\n100 sup=50"
time bin/hw1 a test/100.txt output/100_50.txt 50 > out.log
diff -w -b out.log test/100_50.out

echo "\n1000 sup=300"
time bin/hw1 a test/1000.txt output/1000_300.txt 300 > out.log
diff -w -b out.log test/1000_300.out
echo "\n1000 sup=400"
time bin/hw1 a test/1000.txt output/1000_400.txt 400 > out.log
diff -w -b out.log test/1000_400.out
echo "\n1000 sup=500"
time bin/hw1 a test/1000.txt output/1000_500.txt 500 > out.log
diff -w -b out.log test/1000_500.out

echo "\n10000 sup=3000"
time bin/hw1 a test/10000.txt output/10000_3000.txt 3000 > out.log
diff -w -b out.log test/10000_3000.out
echo "\n10000 sup=4000"
time bin/hw1 a test/10000.txt output/10000_4000.txt 4000 > out.log
diff -w -b out.log test/10000_4000.out
echo "\n10000 sup=5000"
time bin/hw1 a test/10000.txt output/10000_5000.txt 5000 > out.log
diff -w -b out.log test/10000_5000.out


echo "\n\nFPGROWTH ================\n"

echo "100 sup=30"
time bin/hw1 f test/100.txt output/tmp.txt 30 > out.log
if ((`cat output/tmp.txt | wc -l`==`cat output/100_30.txt | wc -l`)); then echo "\nValidated."; fi
echo "\n100 sup=40"
time bin/hw1 f test/100.txt output/tmp.txt 40 > out.log
if ((`cat output/tmp.txt | wc -l`==`cat output/100_40.txt | wc -l`)); then echo "\nValidated."; fi
echo "\n100 sup=50"
time bin/hw1 f test/100.txt output/tmp.txt 50 > out.log
if ((`cat output/tmp.txt | wc -l`==`cat output/100_50.txt | wc -l`)); then echo "\nValidated."; fi

echo "\n1000 sup=300"
time bin/hw1 f test/1000.txt output/tmp.txt 300 > out.log
if ((`cat output/tmp.txt | wc -l`==`cat output/1000_300.txt | wc -l`)); then echo "\nValidated."; fi
echo "\n1000 sup=400"
time bin/hw1 f test/1000.txt output/tmp.txt 400 > out.log
if ((`cat output/tmp.txt | wc -l`==`cat output/1000_400.txt | wc -l`)); then echo "\nValidated."; fi
echo "\n1000 sup=500"
time bin/hw1 f test/1000.txt output/tmp.txt 500 > out.log
if ((`cat output/tmp.txt | wc -l`==`cat output/1000_500.txt | wc -l`)); then echo "\nValidated."; fi

echo "\n10000 sup=3000"
time bin/hw1 f test/10000.txt output/tmp.txt 3000 > out.log
if ((`cat output/tmp.txt | wc -l`==`cat output/10000_3000.txt | wc -l`)); then echo "\nValidated."; fi
echo "\n10000 sup=4000"
time bin/hw1 f test/10000.txt output/tmp.txt 4000 > out.log
if ((`cat output/tmp.txt | wc -l`==`cat output/10000_4000.txt | wc -l`)); then echo "\nValidated."; fi
echo "\n10000 sup=5000"
time bin/hw1 f test/10000.txt output/tmp.txt 5000 > out.log
if ((`cat output/tmp.txt | wc -l`==`cat output/10000_5000.txt | wc -l`)); then echo "\nValidated."; fi


echo "Clean-up..."
rm output/*_*.txt out.log output/tmp.txt