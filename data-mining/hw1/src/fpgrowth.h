#ifndef __FPTREE__
#define __FPTREE__

#include "miner.h"
#include "fpgrowth_utils.h"

using namespace std;
class FPGrowth : public Miner {
  public:
    FPGrowth(char ifile[], char ofile[], unsigned int minsup);
    void run();
  private:
    // Output the frequent item sets into output file,
    // From the single-path FP Tree.
    void _output(const Pattern&) const;
    // Generate combination of nodes in single-path tree and prints it.
    void _generate_pattern(const FPTree::Node& current_node,
        Pattern& pattern, const Pattern& alpha);
    // The recursive FP-growth algorithm.
    void _growth(const FPTree &tree, const Pattern& alpha);
    unsigned int _minsup; // minimal support
};

#endif