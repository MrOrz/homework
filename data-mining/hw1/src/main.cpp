#include <cstdio>
#include <cstdlib>
#include <cstring>

#include "apriori.h"
#include "fpgrowth.h"

using namespace std;

void print_usage();

int main(int argc, char* argv[]){
  if(argc != 5){
    print_usage();
    return 1;
  }
  Miner *miner;

  switch(argv[1][0]) {
  case 'a':
    miner = new Apriori(argv[2], argv[3], atoi(argv[4]));
    break;
  case 'f':
    miner = new FPGrowth(argv[2], argv[3], atoi(argv[4]));
    break;
  default:
    print_usage();
    return 1;
  }

  miner->run();
  return 0;
}

void print_usage(){
  printf("Usage:\n./hw1 <algorithm> <input> <output> <min-sup>\n");
  printf("\talgorithm: 'a' for Apriori, 'f' for FP-growth.\n");
}