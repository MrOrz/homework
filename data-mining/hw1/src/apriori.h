#ifndef __APRIORI__
#define __APRIORI__

#include "miner.h"
#include "apriori_utils.h"

class Apriori : public Miner {
  public:
    Apriori(char ifile[], char ofile[], unsigned int minsup);
    void run();
  private:
    // Output the frequent item sets into output file.
    void _output(const Candidate*) const;
    unsigned int _minsup; // minimal support
};

#endif