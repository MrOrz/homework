#ifndef __MINER__
#define __MINER__

#include "wordlut.h"
class Miner {
  public:
    virtual void run() = 0;
    virtual ~Miner(){
      fclose(_fin);
      fclose(_fout);
    }
  protected:
    FILE *_fin, *_fout;
    WordLUT _lookup;
};

#endif