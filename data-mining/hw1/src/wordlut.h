// Word Look-up table.
// Word --> integer ID and integer ID --> word mapping

#ifndef __WORD__
#define __WORD__

#include <string>
#include <vector>
#include <map>

typedef unsigned int ID;
const unsigned int ID_NIL = -1;

using namespace std;

class WordLUT{
  public:
    WordLUT(){};

    // Add word into the look up table, returns its ID.
    ID add(string word);

    // get the id for a word
    ID id_for(const string& word) const { return _ids.find(word)->second; }

    // get the word for an id.
    string word_for(const ID id) const { return _words[id]; }
  //private:
    map<string, ID> _ids;  // word --> id
    vector<string> _words; // id --> word
};

#endif