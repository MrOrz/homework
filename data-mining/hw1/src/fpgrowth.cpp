#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <string>
#include <algorithm>
#include <cassert>
#include "wordlut.h" // For ID, ID_NIL
#include "fpgrowth.h"

using namespace std;
FPGrowth::FPGrowth(char ifile[], char ofile[], unsigned int minsup):_minsup(minsup){
  _fin = fopen(ifile, "r");
  _fout = fopen(ofile, "w");
}

static void print_minsup(const unsigned int minsup){
  printf("min_support_count = %u\n", minsup);
}

/*
  Generate and output the pattern for each combination of the nodes
  in a single-path tree.
  For example, if the tree is a 4-item single-path tree as below, we could have
  2^4-1 combinations.

  A -> C -> E -> F
            0
       0 <
  0 <       1
       1 ......

  1    ......

  Where 0 denotes 'absence' and 1 denotes 'presense' of the item.

  The output and combination generation can be achieved by recursively visiting
  the tree, alternating between 'absent' and 'present' of a tree node, and perform
  printing at the leaf node.
*/
void FPGrowth::_generate_pattern(const FPTree::Node& current_node,
  Pattern& pattern, const Pattern& alpha){

  if(current_node.children_size()){
    // Backup the pattern support.
    unsigned int old_support = pattern.support;
    // It is not leaf node. Keep recursing.
    // First, try to be absent in the pattern.
    for(FPTree::Node::children_iterator it = current_node.begin();
        it != current_node.end(); ++it){
      _generate_pattern(it->second, pattern, alpha);
    }

    // Secondly, try to be present in the pattern.
    pattern.items.push_back(current_node.item);

    // The current node is now in play.
    // Update pattern and its support.
    // (The support should be the minimum of support within each node in the
    //  combination)
    if(current_node.support < pattern.support){
      pattern.support = current_node.support;
    }

    for(FPTree::Node::children_iterator it = current_node.begin();
        it != current_node.end(); ++it){
      _generate_pattern(it->second, pattern, alpha);
    }

    // Before return, we should take the current node item away.
    pattern.items.pop_back();
    pattern.support = old_support;
  }else{
    // It is the leaf node now.
    // Output the case that this leaf node is absent.
    Pattern postfixed = pattern;
    postfixed.items.insert(postfixed.items.end(), alpha.items.begin(), alpha.items.end());
    // First check if there is no nodes in pattern.
    // pattern.items = Ø is not a valid combination beta in the path P.
    if(pattern.items.size()){
      _output(postfixed);
    }

    // Try to be present in the pattern.
    vector<ID>::iterator insert_it = postfixed.items.begin() + pattern.items.size();
    postfixed.items.insert(insert_it, current_node.item);
    // The current node is now in play.
    // Update pattern and its support.
    // (The support should be the minimum of support within each node in the combination)
    if(current_node.support < postfixed.support){
      postfixed.support = current_node.support;
    }

    _output(postfixed);
  }
}

// Construct beta's conditional pattern base,
// given a_i, traverse itself upwards with the aid of header table
// and the node links.
static PatternBase construct_pattern_base(const HeaderTableEntry& a_i){
  PatternBase pb;

  FPTree::Node* a_i_nodes = a_i.head;
  while(a_i_nodes){
    FPTree::Node* current_node = a_i_nodes->parent; // The leaf of the branch.
    unsigned int support = a_i_nodes->support; // a_i_node's support = branch's support.
    vector<ID> items;

    // Traversing upwards upon root, adding items.
    while(current_node->item != ID_NIL /* Root has item = ID_NIL */){
      items.push_back(current_node->item);
      current_node = current_node->parent;
    }

    // Re-order the items from root to leaf.
    reverse(items.begin(), items.end());

    // Add the items to patternbase.
    pb.add_pattern(items, support);

    a_i_nodes = a_i_nodes->next_node;
  }

  return pb;
}

/*
Procedure FP-growth (Tree, alpha)
  if Tree contains a single path P
    for each combination (denoted as beta) of the nodes in the path P
      generate pattern beta U alpha with support = minimum support of nodes in beta;
  else
    for each a_i in the header of Tree
      generate pattern beta = a_i U alpha with support = a_i.support
      construct beta's conditional pattern base and then beta's conditional FP-tree Tree_beta ;
      if !Tree_beta.empty
        FP-growth (Tree_beta, beta)
*/
void FPGrowth::_growth(const FPTree &tree, const Pattern& alpha){
  // if Tree contains a single path P
  if(tree.contains_single_path()){
    // for each combination (denoted as beta) of the nodes in the path P
    //   generate pattern beta U alpha with support = minimum support of nodes in beta;
    Pattern p;
    // tree.debug(_lookup);
    _generate_pattern(tree.root.first_child(), p, alpha);
  }else{
    // for each a_i in the header of Tree
    for(HeaderTable::const_iterator it = tree.header_table.begin();
        it != tree.header_table.end(); ++it){

      // generate pattern beta = a_i U alpha with support = a_i.support
      const HeaderTableEntry& a_i = it->second;
      Pattern beta(vector<ID>(1, a_i.item), a_i.support);
      beta.items.insert(beta.items.end(), alpha.items.begin(), alpha.items.end());
      _output(beta);

      // construct beta's conditional pattern base
      // and then beta's conditional FP-tree Tree_beta
      PatternBase cond_pattern_base = construct_pattern_base(a_i);
      cond_pattern_base.filter(_minsup); // Generate Database D'

      FPTree tree_beta(cond_pattern_base);

      // tree_beta.debug(_lookup);

      // if !Tree_beta.empty
      if(!tree_beta.empty()){
        // FP-growth (Tree_beta, beta)
        _growth(tree_beta, beta);
      }
    }
  }
}

HeaderTable one_item_set;

// Looking up the support count when sorting an itemset.
static bool items_comp(const ID& i, const ID& j){
  unsigned int sup_i = one_item_set[i].support,
               sup_j = one_item_set[j].support;
  if(sup_i == sup_j){
    return i < j;
  }
  return sup_i > sup_j;
}

void FPGrowth::run(){
  print_minsup(_minsup);

  char buf[100];

  // 1st scan: Build header table (1-item set).
  while( fscanf(_fin, "%s", buf)!=EOF ){
    string bufstring(buf);
    if(bufstring == "!EOD") continue;

    ID word_id = _lookup.add(bufstring);
    if(one_item_set.count(word_id)){
      one_item_set[word_id].support += 1;
    }else{
      one_item_set[word_id] = HeaderTableEntry(word_id, 1);
    }
  }

  // Filter out items < _min_sup in one_item_set
  HeaderTable::iterator it = one_item_set.begin();
  while(it != one_item_set.end()){
    if( (it->second).support < _minsup){
      one_item_set.erase(it++);
      // Ref: http://stackoverflow.com/questions/263945/what-happens-if-you-call-erase-on-a-map-element-while-iterating-from-begin-to
    }else{
      ++it;
    }
  }

  // 2nd scan: Build FP-tree
  fseek(_fin, 0, SEEK_SET);
  vector<ID> items;
  FPTree tree(one_item_set);

  while( fscanf(_fin, "%s", buf)!=EOF ){
    string bufstring(buf);
    if(bufstring == "!EOD"){
      sort(items.begin(), items.end(), items_comp);
      tree.add_items(items);
      items.clear();
      continue;
    }
    ID word_id = _lookup.id_for(bufstring);
    if(one_item_set.count(word_id)){
      // The word is in frequent one-item set, add it to items.
      items.push_back(word_id);
    }
  }

  // tree.debug(_lookup);

  // Call FP-growth
  Pattern empty;
  _growth(tree, empty);
}

void FPGrowth::_output(const Pattern& pattern) const{
  unsigned int item_count = pattern.items.size();
  if(!item_count) return;

  for(int i=0; i<item_count; ++i){
    fprintf(_fout, "%s ", _lookup.word_for(pattern.items[i]).c_str());
  }
  fprintf(_fout, "-> %u\n", pattern.support);
}