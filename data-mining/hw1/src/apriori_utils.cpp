#include "apriori_utils.h"

void Candidate::add(IDSet s, unsigned int initval){
  if(_candidates.count(s) == 0){
    _candidates[s] = initval;
  }else{
    _candidates[s] = _candidates[s] + 1;
  }
}

set<ID> Candidate::filter(unsigned int minsup){
  set<ID> words;

  Candidate::iterator it = _candidates.begin();
  while(it != _candidates.end()){
    if(it->second < minsup){
      _candidates.erase(it++); // erase the infrequent item and increment the iterator.
      // Ref: http://stackoverflow.com/questions/263945/what-happens-if-you-call-erase-on-a-map-element-while-iterating-from-begin-to
    }
    else{
      // Populate the set that records all words in L_n.
      for(IDSet::iterator setit = it->first.begin(); setit != it->first.end(); ++setit)
        words.insert(*setit);

      ++it;
    }
  }

  return words;
}

bool Candidate::exists(const IDSet &s) const{
  return _candidates.count(s) > 0;
}

// void Large::add(IDSet s){
//   // IDKey k = _hash_func(s);
//   // if(_hash.count(k) == 0){
//   //   _hash[k] = vector<IDSet>();
//   // }
//   // for(int i=0; i<_hash[k].size(); ++i){
//   //   if( _hash[k][i] == s){
//   //     _hash[k][i]
//   //   }
//   // }

//   // _hash[k].push_back(s)
// }

// Check if a specific IDSet exists.
// bool Large::exists(const IDSet &s) const{
//   return _large.count(s) > 0;
//   // IDKey k = _hash_func(s);
//   // if(_hash.count(k) && _hash[k].count(s))
//   //   return true;
//   // else
//   //   return false;
// }

// XOR hash function.
// IDKey Large::_hash_func(IDSet s){
//   IDKey ret = 0;
//   for(IDSet::iterator it = s.begin(); it != s.end(); ++it){
//     ret ^= *it;
//   }
//   return ret;
// }