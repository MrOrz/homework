#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <string>
#include <cassert>
#include <algorithm>
#include "fpgrowth_utils.h"

// #define __DEBUG_TREE__
// #define __DEBUG_PATTERN__

using namespace std;

void FPTree::add_items(const vector<ID>& items, unsigned int support){
  Node *current_node = &root;

  #ifdef __DEBUG_TREE__
  printf("Adding items: ");
  #endif

  for(int i=0; i<items.size(); ++i){
    ID current_item = items[i];

    #ifdef __DEBUG_TREE__
    printf("%u ", current_item);
    #endif

    if( current_node->exist_child(current_item) ){
      current_node = current_node->get_child(current_item);
    } else {
      // Create new node and traverse to that node.
      current_node = current_node->add_child(current_item);

      // Maintain header table list
      assert(header_table.count(current_item));
      HeaderTableEntry& entry = header_table[current_item];
      current_node->next_node = entry.head;
      entry.head = current_node;
      entry.node_count += 1;
    }
    current_node->support += support;
  }

  #ifdef __DEBUG_TREE__
  printf("\t support = %u\n", support);
  #endif
}

// static bool header_table_comp(const HeaderTableEntry* i, const HeaderTableEntry* j){
//   if(i->support == j->support)
//     return i->item > j->item;
//   return i->support < j->support;
// }

// // Populate _sorted_header_table vector.
// void FPTree::finalize(){
//   if(!_sorted_header_table.size()){
//     for(HeaderTable::iterator it = header_table.begin();
//         it != header_table.end(); ++it){
//       _sorted_header_table.push_back(&(it->second));
//     }

//     sort(_sorted_header_table.begin(), _sorted_header_table.end(),
//          header_table_comp);
//   }
// }


void debug_traverse(const FPTree::Node& current_node, const int level,
                    const WordLUT& lookup){
  for (int i = 0; i < 2*level; ++i)
  {
    printf(" ");
  }
  if(current_node.item == ID_NIL){
    printf("Root \t@ %p\n", &current_node);
  }else{
    printf("%s(%u) = %u \t@ %p from %p\n",
      lookup.word_for(current_node.item).c_str(), current_node.item,
      current_node.support, &current_node, current_node.parent);
  }
  for(FPTree::Node::children_iterator it = current_node.begin();
      it != current_node.end();
      ++it){
    debug_traverse(it->second, level+1, lookup);
  }
}

void FPTree::debug(const WordLUT& lookup) const{
  printf(".........................\n");
  debug_traverse(root, 0, lookup);

  printf("\nHeader Table\n============\n");
  for(HeaderTable::const_iterator it = header_table.begin();
      it != header_table.end(); ++it){
    const HeaderTableEntry& entry = it->second;
    printf("%s(%u)\t-->", lookup.word_for(entry.item).c_str(), entry.item);
    Node* current_node = entry.head;
    while(current_node){
      printf(" %p", current_node);
      current_node = current_node->next_node;
    }
    printf("\n");
  }
  printf(".........................\n");
}

// Returns true if the tree contains only one single path.
bool FPTree::contains_single_path() const {
  for(HeaderTable::const_iterator it = header_table.begin();
      it != header_table.end(); ++it){
    if((it->second).node_count > 1){
      return false;
    }
  }
  return true;
}

// Add new pattern and updates the 1-item set.
void PatternBase::add_pattern(const vector<ID>& items, const unsigned int& sup){
  if(!items.size()) return;

  #ifdef __DEBUG_PATTERN__
  printf("Adding pattern:");
  #endif

  _patterns.push_back( Pattern(items, sup) );

  for(int i=0; i<items.size(); ++i){
    const ID& item = items[i];
    #ifdef __DEBUG_PATTERN__
    assert(item != ID_NIL);
    printf(" %u", item);
    #endif
    if(!_one_item_set.count(item)){
      // Create an 1-item if it is not existed before.
      HeaderTableEntry new_item_entry(item, sup);

      // TODO it is not inserted?
      _one_item_set.insert( pair<ID, HeaderTableEntry>(item, new_item_entry) );
    }else{
      // Increment the support.
      _one_item_set[item].support += sup;
    }
  }

  #ifdef __DEBUG_PATTERN__
  printf("\tsupport=%u\n", sup);
  #endif
}

// Eliminate the items in the patterns that are below minimal support.
// After filtering, PatternBase are equivalent to Database D'.
void PatternBase::filter(unsigned int min_sup){
  HeaderTable::iterator it = _one_item_set.begin();
  #ifdef __DEBUG_PATTERN__
    printf("Before filtering, _one_item_set.size() = %ld\n", _one_item_set.size());
  #endif

  while(it != _one_item_set.end()){
    if( (it->second).support < min_sup ){
      #ifdef __DEBUG_PATTERN__
      printf("  - Erasing 1-item [%u] (support: %u)\n", (it->second).item, (it->second).support);
      #endif
      _one_item_set.erase(it++);
      // Ref: http://stackoverflow.com/questions/263945/what-happens-if-you-call-erase-on-a-map-element-while-iterating-from-begin-to
    }else{
      ++it;
    }
  }
  // Update _patterns
  for(int i=0; i<_patterns.size(); ++i){
    vector<ID>& items = _patterns[i].items;
    #ifdef __DEBUG_PATTERN__
    printf("  ' Scanning pattern %d (size = %ld)\n", i, items.size());
    #endif

    vector<ID>::iterator it = items.begin();
    while(it < items.end()){
      if(!_one_item_set.count(*it)){
        // The item should not exist anymore.
        #ifdef __DEBUG_PATTERN__
        printf("  ! infrequent 1-item [%u] found @ %p\n", *it, &(*it));
        #endif
        it = items.erase(it);
      }else{
        ++it;
      }
    }
  }

  #ifdef __DEBUG_PATTERN__
    printf("After filtering, _one_item_set.size() = %ld\n", _one_item_set.size());
  #endif
}