#ifndef __FPGROWTH_UTILS__
#define __FPGROWTH_UTILS__
#include "wordlut.h" // for ID & ID_NIL
#include <vector>
#include <map>

using namespace std;

class Pattern{
  public:
    // support is -1 (Max unsigened int) because we often need to find
    // the minimum support, thus thus putting the largest support would be
    // sufficient.
    Pattern():support(-1){}
    Pattern(const vector<ID>& i, const unsigned int s)
      :items(i), support(s){};
    vector<ID> items;
    unsigned int support;
};

class FPTree;
class HeaderTableEntry;

typedef map<ID, HeaderTableEntry> HeaderTable;

class PatternBase {
  friend class FPTree;
  public:
    // Add new pattern and updates the 1-item set.
    void add_pattern(const vector<ID>& items, const unsigned int& support);

    // Eliminate the items in the patterns that are below minimal support.
    // After filtering, PatternBase are equivalent to Database D'.
    void filter(unsigned int min_sup);

    typedef vector<Pattern>::iterator iterator;
    iterator begin(){ return _patterns.begin(); }
    iterator end(){ return _patterns.end(); }

  private:
    vector<Pattern> _patterns;

    // _one_item_set uses HeaderTable class to avoid the overhead of
    // iterating & rebuild the header table when the pattern base is passed
    // to the FPTree constructor.
    HeaderTable _one_item_set;
};

class FPTree{
  public:
    // Create an empty FP Tree
    FPTree(const HeaderTable t):header_table(t){};

    // Create from a filtered pattern base (Database D')
    // by iterating the Database D' and calling add_items.
    FPTree(const PatternBase pb):header_table(pb._one_item_set){
      for(int i=0; i<pb._patterns.size(); ++i){
        add_items(pb._patterns[i].items, pb._patterns[i].support);
      }
    }

    // Create tree nodes and update header table.
    void add_items(const vector<ID>& items, unsigned int support = 1);

    bool empty() const{ return root.children_size() == 0; }

    // Returns true if the tree contains only one single path.
    bool contains_single_path() const;

    class Node{
      public:
        Node(const ID i = ID_NIL, Node* p = NULL, unsigned int sup = 0)
          :parent(p), next_node(NULL), support(sup), item(i){};

        bool exist_child(const ID& i) const { return _children.count(i) > 0; }

        unsigned int children_size() const { return _children.size(); }
        Node* add_child(const ID& i){
          _children[i] = Node(i, this); return &(_children[i]); }
        Node* get_child(const ID& i){ return &(_children[i]); }
        Node first_child() const { return (_children.begin())->second; }
        Node* parent;
        Node* next_node; // next-node
        unsigned int support;

        typedef map<ID, Node>::const_iterator children_iterator;
        children_iterator begin() const { return _children.begin(); }
        children_iterator end() const { return _children.end(); }
        ID item;
      private:
        map<ID, Node> _children; // Uses map to quickly find children.
    };
    Node root;
    HeaderTable header_table;

    void debug(const WordLUT&) const;
};

class HeaderTableEntry{
  public:
    HeaderTableEntry(ID i = ID_NIL, unsigned int sup=0)
      :item(i), node_count(0), support(sup), head(NULL){}
    ID item;
    unsigned int node_count;
    unsigned int support;
    FPTree::Node* head; // head of node-links
};

#endif