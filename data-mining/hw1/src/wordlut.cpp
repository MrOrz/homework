#include "wordlut.h"

ID WordLUT::add(string word){
  if(_ids.count(word) == 0){
    // Only process on new words.
    ID id = _ids[word] = _words.size(); // sequential new id
    _words.push_back(word);
    return id;
  }else{
    return id_for(word);
  }
}