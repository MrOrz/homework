#ifndef __APRIORI_UTILS__
#define __APRIORI_UTILS__

#include <vector>
#include <set>
#include <map>
#include "wordlut.h" // for the type ID

using namespace std;

typedef set<ID> IDSet; // set of IDs

// The candidate set, implemented as a map IDSet -> count (support)
//
class Candidate{
  public:
    Candidate(){};
    void add(IDSet s, unsigned int initval = 0);

    bool exists(const IDSet&) const;

    // Filter out the infrequent IDSets.
    // After filter() this candidate set C_n is equivalent to large set L_n.
    // Also, all word IDs exists in the L_n is returned with a set.
    set<ID> filter(unsigned int minsup);

    // Clear candidate sets.
    void clear(){ _candidates.clear(); }
    bool empty(){ return _candidates.empty(); }
    unsigned int size() const { return _candidates.size(); }

    typedef map<IDSet, unsigned int>::iterator iterator;
    typedef map<IDSet, unsigned int>::const_iterator const_iterator;
    iterator begin(){ return _candidates.begin(); }
    iterator end(){ return _candidates.end(); }
    const_iterator begin() const{ return _candidates.begin(); }
    const_iterator end() const{ return _candidates.end(); }

  private:
    map<IDSet, unsigned int> _candidates;
};

#endif