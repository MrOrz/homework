% adj_matrix, numnode = readnetwork(network_file_name)
% returned adj_matrix is a column vector of length = 10000
%

function [adj, num] = readnetwork(filename)

    num = 0;
    adj = cell(10000,1); % Pre-allocate 10000 nodes
    fin = fopen(filename, 'r');
    
    %% Reading edges from network file.
    edge = fscanf(fin, '%d', 2);
    while edge
        
        adjsize = length(adj);
        A = edge(1);
        B = edge(2);
        
        % Update number of nodes
        num = max([num, edge']);
        
        % connect A->B
        if adjsize < A
            adj{A} = [B];
        else
            adj{A} = [adj{A} B];
        end

        % connect B->A
        if adjsize < B
            adj{B} = [A];
        else
            adj{B} = [adj{B} A];
        end
        
        % Populate new edge
        edge = fscanf(fin, '%d', 2);
    end
    fclose(fin);
end
