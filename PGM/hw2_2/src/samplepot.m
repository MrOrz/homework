% Calculate the sum of sample edge potentials,
% the sum can be considered as a way to evaluate the sample.
function potsum = samplepot(nodes, adj, pot)
    potsum = 0;
    for i = 1:max(size(nodes))
        neighborvalues = nodes( adj{i} );
        potsum = potsum + sum( pot(nodes(i), neighborvalues) );
    end
 