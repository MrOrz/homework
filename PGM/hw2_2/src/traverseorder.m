% Generate traversal order of nodes

function order = traverseorder(adjmat, numnode)

    % The prior is the node with maximal connectivity.
    order = zeros(1,numnode);
    
    connectivities = cellfun(@length, adjmat);  % column vector
    connectivities = connectivities(1:numnode); % Get rid of excessive items
    [~, prior] = max(connectivities);
    order(1) = prior; % The prior is the first node to traverse.
    
    % The order of other nodes are determined using BFS from the prior
    % node.
    queued = zeros(1, numnode); % vector of flags whether the node is queued
    queued(prior) = 1;
    ptr = 2; % Pass-the-end position of the vector 'order'.
    
    for i = 1:numnode
        current = order(i);
        if current == 0
            % current node is not queued because these nodes are not
            % reachable (connectivity = 0).
            % 
            break;
        else
            % current node is queued
            neighbors = adjmat{current};

            % Pick up non-visited neighbors.
            neighbors = neighbors(queued(neighbors) == 0);
            queued(neighbors) = 1; % set the queued flag.

            % enqueue the neighbors
            order(ptr:ptr+length(neighbors)-1) = neighbors;
            ptr = ptr + length(neighbors);
        end
    end
    
    % Insert the non-reachable nodes into order array.
    ids = [1:numnode];
    order(ptr:numnode) = ids(connectivities == 0);
end