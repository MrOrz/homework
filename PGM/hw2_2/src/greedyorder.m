% Generate greedy traversal order, starting from the uncertain nodes with
% most neighbors with high confidence (certain nodes)

function order = greedyorder(adjlist, numnode, marg)
    RATIO_TRESHOLD = 3.0;
    
    probratio = max(marg(:, 1)./marg(:, 2), marg(:, 2)./marg(:, 1));
    ids = 1:numnode;
    uncertainids = ids(probratio <= RATIO_TRESHOLD);
    iscertain = probratio > RATIO_TRESHOLD;
    neighbors = adjlist(uncertainids);
    numcertainneighbors = cellfun(@(nid) sum(iscertain(nid))/length(nid), neighbors);
    
    sorted = sortrows([uncertainids' numcertainneighbors], -2);
    order = sorted(:, 1);
end