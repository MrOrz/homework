% Greedy sampling.
% Populate the nodes with 1 or 2, using the traversal order,
% adjacent matrix, and
% edge potentials.

function newnodes = binarygreedy(nodes, order, adj, pot)
    newnodes = nodes;
    
    %% Generate samples in newnodes.
    %
    for i = 1:length(order)
        current = order(i);
        
        % Fetch the node value of neighbors of node(i)
        neighborvalues = newnodes( adj{current} );

        % Product of potentials that the current node i is '1'.
        onepotprod = prod(pot(1, neighborvalues));

        % Product of potentials that the current node i is '2'.
        twopotprod = prod(pot(2, neighborvalues));

        % Deterministic approach that greedly maximizing the potential.
        if(onepotprod > twopotprod)
            newnodes(current) = 1;
        else
            newnodes(current) = 2;
        end
    end
    