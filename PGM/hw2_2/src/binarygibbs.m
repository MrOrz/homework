% Gibbs sampling.
% Populate the nodes with 1 or 2, using the traversal order,
% adjacent matrix, and
% edge potentials.

function newnodes = binarygibbs(nodes, order, adj, pot)
    newnodes = nodes;
    
    %% Generate samples in newnodes.
    %  Note i starts from 2, which means the first node in traversal order
    %  is not sampled (always fixed), acting as a prior node.
    %
    for i = 2:length(order)
        current = order(i);
        
        % Fetch the node value of neighbors of node(i)
        neighborvalues = newnodes( adj{current} );

        % Product of potentials that the current node i is '1'.
        onepotprod = prod(pot(1, neighborvalues));

        % Product of potentials that the current node i is '2'.
        twopotprod = prod(pot(2, neighborvalues));

        % Prob[ node(i) == 1 ]
        prob = onepotprod / (onepotprod + twopotprod);
        
        if isnan(prob)
            lala
        end
        % Draw the new node.
        % random('Binomial') outputs 0 and 1,
        % in which P[random=1] == prob.
        newnodes(current) = 2 - random('Binomial', 1, prob);
    end
    
    