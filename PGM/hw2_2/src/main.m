function main
    %% Read the network file and generate adjacent matrix
    [adjlist, numnode] = readnetwork('../test.network');

    order = traverseorder(adjlist, numnode);
    
    fprintf(1, 'Network loaded.\n');
    
    %% Randomly generate the init value of nodes
    % The values random variables 1~10000
    nodes = random('Discrete Uniform', 2,1,numnode);
    
    % Edge potentials.
    % Use 0.5 and 1 to prevent INF
    pot = [0.5 1; 1 0.5];
    
    %% Gibbs Sampling
    % This stage outputs bestnodes.mat and marg.mat as cached results.
    
    MARG_FILE = 'marg.mat'; NODE_FILE = 'bestnode.mat';
    
    if exist(MARG_FILE, 'file') == 2 && exist(NODE_FILE, 'file') == 2
        load(MARG_FILE); load(NODE_FILE);
    else
        % 1. Reach for equilibrium.
        fprintf(1, 'Reaching equilibrium\n');
        for i = 1:1000 % M = 1000
            nodes = binarygibbs(nodes, order, adjlist, pot);
            if mod(i, 10) == 0
                fprintf(1, '%d%%\n', i/10);
            else
                fprintf(2, '.');
            end
        end

        % 2. Sampling
        % count = 
        %   count(X1=1) count(X2=1) ... count(XN=1)
        %   count(X1=1) count(X2=2) ... count(XN=2)
        %
        count = zeros(2, numnode);
        bestnodes = [];
        bestpot = 0;

        fprintf(1, '\nSampling\n');
        for i = 1:1000 % N = 1000
            nodes = binarygibbs(nodes, order, adjlist, pot);

            % Select and increment the corresponding counters.
            idx = 0:2:(2*numnode-2); % [0,2,4, ..., 2*numnode-2]
            idx = idx + nodes;
            count(idx) = count(idx) + 1;

            % Update bestnodes & bestpot
            potsum = samplepot(nodes, adjlist, pot);
            if potsum > bestpot
                bestpot = potsum;
                bestnodes = nodes;
            end

            if mod(i, 10) == 0
                fprintf(1, '%d%%\n', i/10);
            else
                fprintf(2, '.');
            end
        end

        marg = count' ./ 1000;

        % Save cached variables
        save(MARG_FILE, 'marg');
        save(NODE_FILE, 'bestnodes');
    end
    
    %% Prediction
    %  Wipe out the values with a low probability ratio in bestnode,
    %  and poulate them with greedy results.
    
    fprintf(1, '\nPrediction\n');
    
    % Setup
    bestpot = samplepot(bestnodes, adjlist, pot);
    order = greedyorder(adjlist, numnode, marg);

    
    % Use the marginal probability to generate a sample, and compare it
    % with the original bestnodes.
    
    nodes = ones(numnode, 1);
    nodes(marg(:, 2) > 0.5) = 2;
    probpot = samplepot(nodes, adjlist, pot);
    if probpot > bestpot
        bestpot = probpot;
        bestnodes = nodes;
    end
    
    % Re-draw low-confidence nodes with greedy method.
    
    fprintf(1, 'Re-drawing %d nodes with greedy method.\n', length(order));
    fprintf(1, 'Baseline potential sum: %d\n', bestpot);
    
    nodes = bestnodes;
    for i = 1:10
        nodes = binarygreedy(nodes, order, adjlist, pot);
 
        % Update bestnodes & bestpot
        potsum = samplepot(nodes, adjlist, pot);
        if potsum > bestpot
            bestpot = potsum;
            bestnodes = nodes;
        end
    end
    
    fprintf(1, 'Predicted best sample potential sum: %d\n', bestpot);
    
    %% Output
    fout = fopen('../test.marg', 'w');
    fprintf(fout, '%lf %lf\n', marg');
    fclose(fout);
    
    fout = fopen('../test.pred', 'w');
    fprintf(fout, '%d\n', bestnodes);
    fclose(fout);
    
end
    