nNodes = 6 + 3*6; % S and problems
nStates = 2; % Good / bad, correct / incorrect

%% Connect the students.
%
%    2-3 6
%    |/|/|
%    1 4-5
%

% Edge(a1, b1)
%
% edges = a1  a2  ...
%         b1  b2  ...
edges = [ [1; 2] [2; 3] [1; 3] [3; 4] [4; 5] [5; 6] [4; 6] ];

%% Student S have problem 3S+4 (P1), 3S+5 (P3), 3S+6 (P2).
%
S = 1:6;

edges = [edges [ [S S S]; [3*S+4 3*S+5 3*S+6] ] ];

%% The problems are related with each other with their difficulties.
%  Connect 3S+4, 3S+5, 3S+6 together.
%
edges = [edges [3*S+4; 3*S+5] [3*S+4; 3*S+6] [3*S+5; 3*S+6]];

adjMat = connect(nNodes, edges);

%% Make edgeStruct
edgeStruct = UGM_makeEdgeStruct(adjMat,nStates);

%% Make edge potentials
maxState = max(edgeStruct.nStates);
nodePot = ones(nNodes, nStates);
edgePot = zeros(maxState,maxState,edgeStruct.nEdges);

for e = 1:edgeStruct.nEdges
    ends = edgeStruct.edgeEnds(e, :);
    pot = zeros(2,2);
    
    if min(ends) <= 6
        % Potentials between students
        pot = [2 1; 1 2];
    elseif ends(1) <= 6 && ends(2) > 6
        % Potentials between students and problems
        pot = [4 1; 1 4];
    else
        % Potentials between problems
        %  S is the student answering the problem.
        %  3S+4 < 3S+5 < 3S+6
        %   P1  <  P3  <  P2
        % 1=correct, 2=incorrect
        pot = [2 3; 1 2];
    end
    
    edgePot(:,:,e) = pot;
end

%% (a)
fprintf(1, '(a)');
[nodeBel,~,~] = UGM_Infer_Exact(nodePot,edgePot,edgeStruct);
nodeBel

%% (a) -2
fprintf(1, '(a)-2');
nodePot(1, :) = [2 1];
nodePot(6, :) = [1 2];
[nodeBel,~,~] = UGM_Infer_Exact(nodePot,edgePot,edgeStruct);
nodeBel

%% (b)

fprintf(1, '(b)');

% Reset nodePot
nodePot = ones(nNodes, nStates);

% Clampted vector
clamped = zeros(nNodes, 1);

% P1, P2 are 3S+4, 3S+6 respectively.
clamped([3*1+4, 3*1+6]) = [1 2];
clamped([3*2+4, 3*2+6]) = [1 2];
clamped([3*3+4, 3*3+6]) = [1 1];
clamped([3*4+4, 3*4+6]) = [1 2];
clamped([3*5+4, 3*5+6]) = [2 2];
clamped([3*6+4, 3*6+6]) = [1 2];

% Do conditional inference
[nodeBel,~,~] = UGM_Infer_Conditional(nodePot,edgePot,edgeStruct,clamped,@UGM_Infer_Exact);
nodeBel