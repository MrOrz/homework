% Output the adjacent matrix, given number of nodes and the undirected
% edges.
% One column in edges denotes the two ends of an edge.
%
function adjMat = connect(nNodes, edges)
    adjMat = zeros(nNodes, nNodes);
    for edge = edges
        adjMat(edge(2), edge(1)) = 1;
        adjMat(edge(1), edge(2)) = 1;
    end 
end