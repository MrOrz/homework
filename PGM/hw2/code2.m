%% Speficy the two graphs, G1 (G2's subgraph candidate) and G2.

G1nodeCount = 6;
G2nodeCount = 7;

G1adjMat = connect(G1nodeCount, [ [1;3] [1;4] [1;5] [2;3] [2;6] [3;6] [4;5] [4;6] ]);
G2adjMat = connect(G2nodeCount, [ [1;4] [1;5] [1;7] [2;4] [2;6] [2;7] [3;4] [3;5] [3;6] [4;5] [6;7] ]);


% The testcase that G1 is NOT a subgraph of G2.
% G1 is a star, and G2 is a chain.
%
% G1nodeCount = 4;
% G2nodeCount = 8;
% 
% G1adjMat = connect(G1nodeCount, [ [1;2] [1;3] [1;4] ]);
% G2adjMat = connect(G2nodeCount, [ [1;2] [2;3] [3;4] [4;5] [5;6] [6;7] [7;8]  ]);


%% Create graph
% Each node in G1 is considered as a node, each with G2nodeCount states,
% indicating the potential that the node in G1 is mappted into the
% corresponding node in G2.
% 
% The graph is a complete graph.

nodePot = ones(G1nodeCount, G2nodeCount);
adjMat = ones(G1nodeCount, G1nodeCount) - eye(G1nodeCount, G1nodeCount);
edgeStruct = UGM_makeEdgeStruct(adjMat, G2nodeCount);

% edgeStruct.edgeEnds

%% Specifying edge potentials

maxState = max(edgeStruct.nStates);
edgePot = zeros(maxState,maxState,edgeStruct.nEdges);

POSSIBLE = 10;
IMPOSSIBLE = 1;
TRAP = 5;

for e = 1:edgeStruct.nEdges
    ends = edgeStruct.edgeEnds(e, :);
    pot = ones(G2nodeCount, G2nodeCount) * TRAP;
    
    connectedInG1 = G1adjMat(ends(1), ends(2));
    
    if connectedInG1
       for end1 = 1:G2nodeCount - 1
           for end2 = end1+1:G2nodeCount
               connectedInG2 = G2adjMat(end1, end2);
               
               if connectedInG2    
                   pot(end1, end2) = POSSIBLE;
                   pot(end2, end1) = POSSIBLE;
               else
                   pot(end1, end2) = IMPOSSIBLE;
                   pot(end2, end1) = IMPOSSIBLE;
               end
           end
       end
        
    else
        pot = POSSIBLE * ones(G2nodeCount, G2nodeCount) - (POSSIBLE - TRAP) * eye(G2nodeCount, G2nodeCount);
    end
    
    edgePot(:,:,e) = pot;
end

% edgePot

%% Do the inference!
optimalDecoding = UGM_Decode_Exact(nodePot,edgePot,edgeStruct);
optimalDecoding
% [nodeBel,~,~] = UGM_Infer_Exact(nodePot,edgePot,edgeStruct);
% nodeBel