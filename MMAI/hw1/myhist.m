% calculates histogram of a specific region of image
% @param region: h * w * channels array
function h = myhist(region)
    edges = 0:16:256;                   % 16-bin for each channel
    hh = sum(histc(region, edges), 2); % sum(16 * w * channels, 2) = 16*1*channels
    
    % concatenate the three histograms of each channel and
    % returns a row vector with 16*3=48 elements
    h = [transpose(hh(1:end-1,1,1)) ...
         transpose(hh(1:end-1,1,2)) ...
         transpose(hh(1:end-1,1,3))];
end