% The main function to execute.
% Usage: main(video_id);
% @param video_id  a number of 1~8.
% After after indicating the number video_id, main() would find 
% 0X_frame directly and start processing the images right away.

function main(video_id)
    global n_pixels n_regions n_bins REGION_SIZE;
    dbstop if error

    % consts
    DIFFERENCE_TRHES = 6000000;  % RGB
    %DIFFERENCE_TRHES = 1000000;   % YIQ
    %DIFFERENCE_TRHES = 400000;   % HSV
    IMAGE_SIZE = [240, 320];
    REGION_SIZE = [60, 80];
    n_regions = prod( IMAGE_SIZE ./ REGION_SIZE );
    count_thres = 0.5 * n_regions;
    
    % jpg and cached mat files
    path = sprintf('videos.hw01/%02d_frame/', video_id);
    cached = size(dir(strcat(path, 'cache.mat')), 1) ~= 0;
    files = dir(strcat(path, '*.jpg'));
    
    % some directorys start from 0.jpg.
    if strcmp(files(1).name, '0.jpg') == 1
        i_init = 0;
    else
        i_init = 1;
    end
    
    if ~cached
        % last/this_hists are a list of histogram row-vectors
        first_im = imresize( ...
            imcrop( ...
                imread( ...
                    strcat(path, int2str(i_init) ,'.jpg')), ...
                    cropper(video_id) ...
                ), ...
            IMAGE_SIZE ...
        );
        %first_im = rgb2hsv(first_im./255) .* 255;
        %first_im = rgb2ntsc(first_im);
        

        % fill in the global variables (in fact they are now fixed)
        [height, width, channel] = size(first_im);
        n_pixels = width * height * channel;
        n_bins = 16 * channel;

        % load in first image
        last_hists = regionhist(first_im);
        
        % create cache
        cache = zeros(size(files), n_regions);
    else
        % if cached, load the cached variables
        load(strcat(path, 'cache.mat'));
    end
    
    % initialization
    x = zeros(size(files, 1)-1, 1);
    cuts = [];
    fprintf(1, 'Executing');
    edges = [0, DIFFERENCE_TRHES];
    
    for i = i_init + 1:size(files)-1
        % print progress
        if(mod(i,10) == 0)
            fprintf(2, '%d\n', i);
        else
            fprintf(2, '.');
        end
        
        if(cached)
            L2Dists = cache(i, :);
        else
            im = imresize( ...
                imcrop( ...
                    imread(strcat(path, int2str(i), '.jpg')), ...
                    cropper(video_id) ...
                ), ...
                IMAGE_SIZE ...
            );
        
            %im = rgb2hsv(im./255) .* 255;
            %im = rgb2ntsc(im);
            
            this_hists = regionhist( im );

            % L2Dists: list of histogram L2 distances of each region.
            % L2Dists is a column vector with size of (region_count)
            diffvec = last_hists - this_hists;
            L2Dists = sum(diffvec .* diffvec, 2);
            
            % populate cache
            cache(i, :) = L2Dists;
            
            % renew last_hists
            last_hists = this_hists;
        end
        
        % counting regions below and above the difference threshold.
        n_reg_below_diff_thres = transpose(histc(L2Dists, edges));
        n_reg_below_diff_thres = n_reg_below_diff_thres(1);        
        
        % record the ratio of regions above difference threhsold
        x(i) = n_regions - n_reg_below_diff_thres;
        %x(i) = mean(L2Dists);
        
        if(n_regions - n_reg_below_diff_thres >= count_thres)
            cuts = [cuts i]; % record the cutted frame number
        end
        
    end
    fprintf('Done.\n');
    stem(x);
    title(path);
    xlabel('frames');
    ylabel('# of regions has difference > difference threshold');
    
    if(~cached)
        % save the cache into a file
        save(strcat(path, 'cache.mat'), 'cache');
    end
    
    %  final output
    path
    cuts
    correct_cuts(video_id)
    [precision, recall, f1] = bench(cuts, correct_cuts(video_id));
    precision
    recall
    f1
    shot_length = abs(circshift(cuts,  [0,1]) - cuts);
    avg_shot_length = mean( shot_length(2:end) )
end

function hists = regionhist(img)
    % Calculates 80x60 region 48-bin histograms of a color image.
    % hists is a list of histogram row-vectors (region count * 192)
    % where region count = height*width/16.
    global n_regions n_bins REGION_SIZE;
    
    [height, width, ~] = size(img);
    hists = zeros( n_regions , n_bins);
    index = 1;
    for i = 1:REGION_SIZE(1):height-1
        for j = 1:REGION_SIZE(2):width-1
            hists(index ,:) = myhist( ...
                img(i:i+REGION_SIZE(1)-1, j:j+REGION_SIZE(2)-1, :) ...
            );
            index = index + 1;
        end
    end
end
