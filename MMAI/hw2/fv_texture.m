function ret = fv_texture(file_path)
    % resize to reduce calculation
    SCALE = 0.5;

    % splitting file path and get feature if one is saved before
    [pathstr, name, ~] = fileparts(file_path);
    cache_path = fullfile(pathstr, [name '.fv_texture.mat']);
    is_cached = size(dir(cache_path), 1) ~= 0;

    %is_cached = false;
    if(is_cached)
        % feature cached, retrieve vector from feature file
        ret = load(cache_path);
        ret = ret.ret;
    else
        im = imread(file_path);
        if(size(im, 3) > 1), im = rgb2gray(im); end;
        im = imresize(im, SCALE);
        
        nscale  = 4;      %Number of wavelet scales.
        norient = 6;      %Number of filter orientations.
        
        [EO, ~] = gaborconvolve(im, nscale, norient, 3, 2, 0.65);
        
        % ret: third dimension is [1st moment, 2nd moment]
        %      of the result image
        ret = zeros(nscale,norient,2);
        
        for s = 1:nscale
            for o = 1:norient
                result = abs(EO{s,o});
                
                ret(s,o,1) = mean(result(1:end));
                ret(s,o,2) = std(result(1:end));
            end
        end
        
        % convert to 1 dimensional feature vector
        ret = ret(1:end);
        save(cache_path, 'ret');
    end
end
