function im_query()

    %% load database and queries
    db = load_feature('dataset/', @(d) d.isdir && d.name(1) ~= '.' ...
              && ~ strcmp(d.name, '00queries00'));
    queries = load_feature('dataset/00queries00/', ...
                            @(d) d.isdir && d.name(1) ~= '.' );
    
    %% for each query
    for j = 1:numel(queries) 
        
        % query feature vector
        q_name = queries(j).name;
        [~, q_filename, ~] = fileparts(q_name);
        q_fv_color = queries(j).fv_color;
        q_fv_texture = queries(j).fv_texture;
        fprintf(1, 'Querying %s\n', q_filename);
        
        %% calculating distance and store into 'result'
        result = struct('name', {}, 'd_color', {}, 'd_texture', {});
        
        for i = 1:numel(db)
            entry = db(i);
            result(i).name = entry.name;
            result(i).d_color = fv_sim(q_fv_color, entry.fv_color);
            result(i).d_texture = fv_sim(q_fv_texture, entry.fv_texture);
        end

        %% sort using result.d_{color, texture}
        [~, c_index_of] = sort([result.d_color]);
        [~, t_index_of] = sort([result.d_texture]);
        
        show_first_ten(strcat('c_', q_filename), q_name, c_index_of, result);
        show_first_ten(strcat('t_', q_filename), q_name, t_index_of, result);
        
        %% mixed feature: summing two ranks
        rank_mixed = zeros(1,numel(db));
        for rank = 1:numel(rank_mixed) % for each rank
            % c_index_of(n) is the result id that ranked n in color feature
            % distance.
            % t_index_of(n) is the result id that ranked n in texture
            % feature distance.
            
            rank_mixed(c_index_of(rank)) = ...
                rank_mixed(c_index_of(rank)) + rank;
            rank_mixed(t_index_of(rank)) = ...
                rank_mixed(t_index_of(rank)) + rank; 
        end
        % Now rank_mixed(i) is the sum of the color and feature ranking
        % for the i-th result.
        
        [~, s_index_of] = sort(rank_mixed);
        show_first_ten(strcat('m_', q_filename), q_name, s_index_of, result);
    end
end

function db = load_feature(target_dir, dir_filter)
    % target_dir should include trailing slash.
    % dir_filter should return true on directories that we want to
    %            traverse.
    
    dd = dir(strcat(target_dir, '*'));
    db = struct('name', {}, 'fv_color', {}, 'fv_texture', {});
    idx = 1;
    fprintf(1,'Loading feature');
    for i = 1:numel(dd)
        d = dd(i);
        if ~( dir_filter(d) ) % applying dir filter
            continue;
        end
        path = strcat(target_dir, d.name);
        files = dir(strcat(path, '/*g')); % jpg / jpeg
        for j = 1:numel(files)
            f = files(j);
            db(idx).name = strcat(path, '/', f.name);
            db(idx).fv_color = fv_color(db(idx).name);
            db(idx).fv_texture = fv_texture(db(idx).name);
            idx = idx + 1;
            fprintf(2,'.');
        end
    end
    fprintf(1, 'done.\n');

end

function show_first_ten(query_title, q_name, sorted_index, result)
    
    figure(1);
    
    % showing query image
    subplot(4,3,1);
    imshow(imread(q_name));
    title(query_title,'Interpreter','none');
    
    % showing top-10 image
    for i = 1:10
       r = result(sorted_index(i));
       subplot(4, 3, i+2);
       
       imshow(imread(r.name));
       [~, name, ~] = fileparts(r.name);
       title(name,'Interpreter','none');
    end
    print(strcat('result/', query_title, '.png'), '-dpng');
    
end