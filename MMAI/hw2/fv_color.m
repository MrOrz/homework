function ret = fv_color(file_path)
    % Referring to:
    % Tools and Techniques for Color Image Retrieval

    % resizing const
    W = 128; H = 128;
    
    % splitting file path and get feature if one is saved before
    [pathstr, name, ~] = fileparts(file_path);
    cache_path = fullfile(pathstr, [name '.fv_color.mat']);
    is_cached = size(dir(cache_path), 1) ~= 0;
    
    %is_cached = false;
    if(is_cached)
        % feature cached, retrieve vector from feature file
        ret = load(cache_path);
        ret = ret.ret;
    else
        im = imresize( imread(file_path), [H, W] );
        if(size(im, 3) == 1) 
           % if only contains 1 channel,
           % extend G and B to be the same value as R.
           im(:,:,2) = im(:,:,1);
           im(:,:,3) = im(:,:,1);
        end
        im = rgb2hsv(im);
        
        %% HSV color space -- Q166 Quantizer
        % Applying Q166 quantizer (18 hues, 3 saturation, 3val, 4gray).
        % The 4 gray values are contributed by pixels with its saturation
        % falls into the lowest value bin (=0 after quantization),
        % thus we use 4th saturation bin to capture those low saturation
        % points.
        
        % quantized im
        q_im(:,:) = floor(im(:,:,1) .* 18);
        q_im(:,:,2) = floor(im(:,:,2) .* 4);
        q_im(:,:,3) = floor(im(:,:,3) .* 3);
            
        % Find grey pixels (pixels with low saturation).
        idx = (q_im(:,:,2) == 0);
            % an index logical indicating saturation == 0
        
        % constructing 3-dimentional indexing logical selecting hue (:,:,1)
        logical0s = false(H,W);
        %idx3_h = idx; idx3_h(:,:,2) = logical0s; idx3_h(:,:,3) = logical0s;
        
        % indexing logical selecting value (:,:,3)
        idx3_v = logical0s; idx3_v(:,:,2) = logical0s; idx3_v(:,:,3) = idx;
                
        % For the 'value' of grey pixels, use a 4-step quantizer instead.
        % However the quantized value is going to be passed into the median
        % filter, along with non-grey pixels, thus we should normalize the
        % value to 0~3 (with decimal points) so that it matches the range
        % of other non-grey pixels.
        q_im(idx3_v) = floor(im(idx3_v) .* 4) .* 3/4;
        
        %figure(1); imshow(q_im(:,:,1), [0,18]);
        
        %% Median Filtering
        % non-linear filter (2D median filter)
        % eliminate the effect of extreme values
        filt_im(:,:,1) = medfilt2(q_im(:,:,1));
        filt_im(:,:,2) = medfilt2(q_im(:,:,2));
        filt_im(:,:,3) = medfilt2(q_im(:,:,3));
        
        %figure(2); imshow(filt_im(:,:,1), [0,18]);
        
        %% Histogram calculation
        % Find the grey pixels after median filtering, 
        % since they should be seperated from other pixels when calculating
        % histograms
        idx = (filt_im(:,:,2) == 0);
            % an index logical indicating saturation == 0
                
        % Grey pixel indexing logical 'idx' changed; 
        % rebuild idx3_v.
        idx3_v(:,:,3) = idx;
        
        % Bulding indexing logicals of non-grey pixel's H, S, V values
        nidx = not(idx);
        nidx3_h = nidx; nidx3_h(:,:,2) = logical0s; nidx3_h(:,:,3) = logical0s;
        nidx3_s = logical0s; nidx3_s(:,:,2) = nidx; nidx3_s(:,:,3) = logical0s;
        nidx3_v = logical0s; nidx3_v(:,:,2) = logical0s; nidx3_v(:,:,3) = nidx;
        
        %figure(1); imshow(filt_im(:,:,1), [0,3]);
        %figure(2); imshow(filt_im(:,:,1), [0,3]);
        
        % 3-D histogram initialization
        zero18x3 = zeros(18,3); 
        hist = zero18x3; hist(:,:,2) = zero18x3; hist(:,:,3) = zero18x3;

        % non_grey is a 2-D matrix that hosts all non-grey pixels,
        % each pixel is a row vector of its H, S and V values
        non_grey = [filt_im(nidx3_h), filt_im(nidx3_s), filt_im(nidx3_v)];
        
        % Histogram calculation
        for pixel = transpose(non_grey)
            % notice that even after values are floor'ed,
            % there might be some values = upperbound, and should be
            % saturated to upperbound-1.
            h = min(pixel(1), 17) + 1;
            s = min(pixel(2)-1, 2) + 1; % s = 1~3
            v = min(floor(pixel(3)), 2) + 1;
                % decimal of pixel(3) are caused by pixels that are 
                % 'used to' be grey before.
                
            hist( h, s, v ) = hist( h, s, v ) + 1;
        end
        
        assert(sum(sum(sum(hist))) == size(non_grey,1), ...
               'HSV bins should contain all non-grey pixels');
        
        % Quantize the original image value to calculate gray bins.
        % Since filt_im, q_im are already quantized to 3 steps,
        % we can only use original im to do the 4-step qunatization.
        gray_bins = sum(histc(floor(im(idx3_v).*4), 0:4), 2);
        
        % histc puts values that equals the upper_limit to the last bin
        % and it should be in the bin of upper_limit-1~upper_limit.
        gray_bins = merge_last_bin(gray_bins);
           
        %% Feature Vector
        % Concatenate the bins to form a 166-dim feature vector.
        % Cache the feature vector
        ret = [hist(1:end), transpose(gray_bins)] ./ (W*H);
        assert(sum(ret) == 1, ...
               'Bins should cover all pixels');
        
         % caching
         save(cache_path, 'ret');
    end
end

function ret = merge_last_bin(h)
    % merge the last bin of the histogram into the second last bin of
    % histogram, and return 1~end-1 part of histogram.
    h(end-1) = h(end-1) + h(end);
    ret = h(1:end-1);
end