function ret = fv_sim(v1, v2, metric)
    if nargin == 2
        metric = 'L2';
    end
    
    switch metric
        %% Minkowski form metrics
        case 'L1'
            ret = sum(abs(v1-v2));
            
        case 'L2'
            d = v1 - v2;
            if(size(d, 2) == 1)
                % d is column vector
                ret = d' * d;
            else
                ret = d * d';
            end
    end
end