# search.py
# ---------
# Licensing Information: Please do not distribute or publish solutions to this
# project. You are free to use and extend these projects for educational
# purposes. The Pacman AI projects were developed at UC Berkeley, primarily by
# John DeNero (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# For more info, see http://inst.eecs.berkeley.edu/~cs188/sp09/pacman.html

"""
In search.py, you will implement generic search algorithms which are called
by Pacman agents (in searchAgents.py).
"""

from util import Stack, Queue, PriorityQueue, manhattanDistance

class SearchProblem:
  """
  This class outlines the structure of a search problem, but doesn't implement
  any of the methods (in object-oriented terminology: an abstract class).

  You do not need to change anything in this class, ever.
  """

  def getStartState(self):
     """
     Returns the start state for the search problem
     """
     util.raiseNotDefined()

  def isGoalState(self, state):
     """
       state: Search state

     Returns True if and only if the state is a valid goal state
     """
     util.raiseNotDefined()

  def getSuccessors(self, state):
     """
       state: Search state

     For a given state, this should return a list of triples,
     (successor, action, stepCost), where 'successor' is a
     successor to the current state, 'action' is the action
     required to get there, and 'stepCost' is the incremental
     cost of expanding to that successor
     """
     util.raiseNotDefined()

  def getCostOfActions(self, actions):
     """
      actions: A list of actions to take

     This method returns the total cost of a particular sequence of actions.  The sequence must
     be composed of legal moves
     """
     util.raiseNotDefined()


def tinyMazeSearch(problem):
  """
  Returns a sequence of moves that solves tinyMaze.  For any other
  maze, the sequence of moves will be incorrect, so only use this for tinyMaze
  """
  from game import Directions
  s = Directions.SOUTH
  w = Directions.WEST
  return  [s,s,w,s,w,w,s,w]

def depthFirstSearch(problem):
  """
  Search the deepest nodes in the search tree first [p 74].

  Your search algorithm needs to return a list of actions that reaches
  the goal.  Make sure to implement a graph search algorithm [Fig. 3.18].

  To get started, you might want to try some of these simple commands to
  understand the search problem that is being passed in:

  """

  # the state stack
  frontier = Stack()

  current = ( problem.getStartState(), )
  frontier.push(current)

  # parent map for all nodes that is (was) in frontier
  # (x,y) --> parent state
  parentOf = {current[0]: None}

  while True:
    if frontier.isEmpty():
      return #: Not found

    current = frontier.pop()
    if problem.isGoalState(current[0]):
      break

    successors = problem.getSuccessors(current[0])
    successors.reverse()

    for nextState in successors:
      #: expand only when not visited
      if(not parentOf.has_key(nextState[0])):
        parentOf[ nextState[0] ] = current
        frontier.push(nextState)


  #: Now we reach the goal.
  #: Trace the route back using parent relationship
  route = []
  while parentOf[ current[0] ] != None:
    route = [ current[1] ] + route
    current = parentOf[ current[0] ]

  return route


def breadthFirstSearch(problem):
  "Search the shallowest nodes in the search tree first. [p 74]"
  "*** YOUR CODE HERE ***"

  # the state stack
  frontier = Queue()

  current = ( problem.getStartState(), )
  frontier.push(current)

  # parent map for all nodes that is (was) in frontier
  # (x,y) --> parent state
  parentOf = {current[0]: None}

  while True:
    if frontier.isEmpty():
      return #: Not found

    current = frontier.pop()
    if problem.isGoalState(current[0]):
      break

    successors = problem.getSuccessors(current[0])

    for nextState in successors:
      #: expand only when not visited
      if(not parentOf.has_key(nextState[0])):
        parentOf[ nextState[0] ] = current
        frontier.push(nextState)


  #: Now we reach the goal.
  #: Trace the route back using parent relationship
  route = []
  while parentOf[ current[0] ] != None:
    route = [ current[1] ] + route
    current = parentOf[ current[0] ]

  return route

def uniformCostSearch(problem):
  "Search the node of least total cost first. "
  "*** YOUR CODE HERE ***"
  # the state stack
  frontier = PriorityQueue()

  #: current: (x,y), direc, cost, total_cost
  #:          0      1      2     3
  current = ( problem.getStartState(), None, 0, 0)
  frontier.push(current, 0)

  # parent map for all nodes that is (was) in frontier
  # (x,y) --> parent state
  parentOf = {current[0]: None}

  while True:
    if frontier.isEmpty():
      return #: Not found

    current = frontier.pop()
    if problem.isGoalState(current[0]):
      break

    successors = problem.getSuccessors(current[0])

    for nextState in successors:
      #: expand only when not visited
      if(not parentOf.has_key(nextState[0])):
        parentOf[ nextState[0] ] = current
        c = nextState[2]  #: action cost
        g = current[3]    #: total cost so far

        #: augument nextState with 'cost so far' = g + c
        frontier.push(nextState + (g+c,), g+c)

  #: Now we reach the goal.
  #: Trace the route back using parent relationship
  route = []
  while parentOf[ current[0] ] != None:
    route = [ current[1] ] + route
    current = parentOf[ current[0] ]

  return route


def nullHeuristic(state, problem=None):
  """
  A heuristic function estimates the cost from the current state to the nearest
  goal in the provided SearchProblem.  This heuristic is trivial.
  """
  return 0

def aStarSearch(problem, heuristic=nullHeuristic):
  "Search the node that has the lowest combined cost and heuristic first."
  "Search the node of least total cost first. "
  "*** YOUR CODE HERE ***"
  "*** YOUR CODE HERE ***"
  # the state stack
  frontier = PriorityQueue()

  #: current: (x,y), direc, cost, total_cost
  #:          0      1      2     3
  current = ( problem.getStartState(), None, 0, 0)
  frontier.push(current, heuristic(current[0], problem))

  # parent map for all nodes that is (was) in frontier
  # (x,y) --> parent state
  parentOf = {current[0]: None}

  while True:
    if frontier.isEmpty():
      return #: Not found

    current = frontier.pop()
    if problem.isGoalState(current[0]):
      break

    successors = problem.getSuccessors(current[0])

    for nextState in successors:
      #: expand only when not visited
      if(not parentOf.has_key(nextState[0])):
        parentOf[ nextState[0] ] = current
        c = nextState[2]  #: action cost
        g = current[3]    #: total cost so far
                          #: c+g = cost so far for next state
        h = heuristic(nextState[0], problem)  #: h(next state)

        #: augument nextState with 'cost so far' = g + c
        frontier.push(nextState + (g+c,), g+c+h)

  #: Now we reach the goal.
  #: Trace the route back using parent relationship
  route = []
  while parentOf[ current[0] ] != None:
    route = [ current[1] ] + route
    current = parentOf[ current[0] ]

  return route

# Abbreviations
bfs = breadthFirstSearch
dfs = depthFirstSearch
astar = aStarSearch
ucs = uniformCostSearch
