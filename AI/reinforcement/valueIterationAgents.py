# valueIterationAgents.py
# -----------------------
# Licensing Information: Please do not distribute or publish solutions to this
# project. You are free to use and extend these projects for educational
# purposes. The Pacman AI projects were developed at UC Berkeley, primarily by
# John DeNero (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# For more info, see http://inst.eecs.berkeley.edu/~cs188/sp09/pacman.html

import mdp, util

from learningAgents import ValueEstimationAgent

INF = float('inf')

class ValueIterationAgent(ValueEstimationAgent):
  """
      * Please read learningAgents.py before reading this.*

      A ValueIterationAgent takes a Markov decision process
      (see mdp.py) on initialization and runs value iteration
      for a given number of iterations using the supplied
      discount factor.
  """

  def expectedValue(self, state, action):
    """
      Return the expected reward after taking the specific action,
      using self.values.
    """
    #: list of (nextState s', probability P[s'|state, action]) pairs
    stateProbList = self.mdp.getTransitionStatesAndProbs(state, action)
    return reduce( lambda sum, pair: sum + pair[1] * self.values[pair[0]],
                   stateProbList, 0)

  def __init__(self, mdp, discount = 0.9, iterations = 100):
    """
      Your value iteration agent should take an mdp on
      construction, run the indicated number of iterations
      and then act according to the resulting policy.

      Some useful mdp methods you will use:
          mdp.getStates()
          mdp.getPossibleActions(state)
          mdp.getTransitionStatesAndProbs(state, action)
          mdp.getReward(state, action, nextState)
    """
    self.mdp = mdp
    self.discount = discount
    self.iterations = iterations
    self.values = util.Counter() # A Counter is a dict with default 0

    "*** YOUR CODE HERE ***"

    for iteration in range(0, iterations):
      #: Save a copy of values[] for this iteration
      currentValues = self.values.copy()

      for s in mdp.getStates():

        bestChoice = (-INF,) #: (expVal, action)
        possibleActions = mdp.getPossibleActions(s)

        if len(possibleActions) == 0: #: Do not let -INF pollute currentValues
          continue

        #: Find the best "action" that gives largest E[self.values]
        for a in possibleActions:
          expVal = self.expectedValue(s, a)
          if expVal > bestChoice[0]:
            bestChoice = (expVal, a)

        expReward = reduce( lambda sum, pair: sum + pair[1] * self.mdp.getReward(s, bestChoice[1], pair[0]),
                              self.mdp.getTransitionStatesAndProbs(s, bestChoice[1]), 0)

        currentValues[s] = expReward + self.discount * bestChoice[0]

      #: Update the values[] at the end of this iteration
      self.values = currentValues


  def getValue(self, state):
    """
      Return the value of the state (computed in __init__).
    """
    return self.values[state]


  def getQValue(self, state, action):
    """
      The q-value of the state action pair
      (after the indicated number of value iteration
      passes).  Note that value iteration does not
      necessarily create this quantity and you may have
      to derive it on the fly.
    """
    "*** YOUR CODE HERE ***"
    return self.expectedValue(state, action)

  def getPolicy(self, state):
    """
      The policy is the best action in the given state
      according to the values computed by value iteration.
      You may break ties any way you see fit.  Note that if
      there are no legal actions, which is the case at the
      terminal state, you should return None.
    """
    "*** YOUR CODE HERE ***"

    possibleActions = self.mdp.getPossibleActions(state)
    if len(possibleActions) == 0:
      return None

    #: (expected value, the action to take)
    bestChoice = (-INF, )
    for a in possibleActions:
      expVal = self.expectedValue(state, a)
      if expVal > bestChoice[0]:
        bestChoice = (expVal, a)

    return bestChoice[1]

  def getAction(self, state):
    "Returns the policy at the state (no exploration)."
    return self.getPolicy(state)

